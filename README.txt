English:
--------
- Copy of six ships (fully functional) of Ur-Quan Masters game (2 with automatic behavior).
- Entity component system.
- Data driven.
- Behavior Trees.
- Scripting with LUA.
- Made on own engine.

Espa�ol:
---------
- Reproducci�n de 6 naves completamente funcionales del juego Ur-Quan Master (2 de ellas con comportamiento autom�tico).
- Entity component system.
- Data driven.
- �rboles de comportamiento.
- Scripting mediante LUA.
- Realizado sobre motor propio.