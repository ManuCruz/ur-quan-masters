#include "../includeGame/autonomousFighterWeapon.h"
#include "../includeGame/renderable.h"
#include "../includeGame/movement.h"
#include "../includeGame/followEntityMovement.h"
#include "../includeGame/life.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//AUTONOMOUS FIGHTER WEAPON
C_AutonomousFighterWeapon::C_AutonomousFighterWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig){
  String soundName;
  m_margin = 25;
  m_time = 10;
  m_outputAngle = 135;
  m_life = 1;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("AutonomousFighterWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
      if (node->first_attribute("marginToAttack"))
        m_margin = String(node->first_attribute("marginToAttack")->value()).ToFloat();
      if (node->first_attribute("timeToAttack"))
        m_time = String(node->first_attribute("timeToAttack")->value()).ToFloat();
      if (node->first_attribute("outputAngle"))
        m_outputAngle = String(node->first_attribute("outputAngle")->value()).ToFloat();
      if (node->first_attribute("life"))
        m_life = (uint32)String(node->first_attribute("life")->value()).ToInt();
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_AutonomousFighterWeapon::Update(double elapsed){
  C_Weapon::Update(elapsed);
}

void C_AutonomousFighterWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_AutonomousFighterWeapon::Shooting(){
  C_Weapon::Shooting();
  if (m_canShot){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);

    //Entity 1
    Entity* ent1 = getOwner()->GetEntityManager()->addEntity();
    ent1->SetIdOwner(getOwner()->GetIdOwner());
    getOwner()->GetEntityManager()->addTarget(ent1);

    //Renderable Component
    RenderableParam *paramRenderable1 = new RenderableParam();
    paramRenderable1->spriteName = m_spriteName;

    getOwner()->GetEntityManager()->addComponent(ent1, Renderable, paramRenderable1);
    delete paramRenderable1;

    //Movement Component
    MovementParam *paramMovement1 = new MovementParam();
    paramMovement1->initialX = mesGetSprite->sprite->GetX();
    paramMovement1->initialY = mesGetSprite->sprite->GetY();
    paramMovement1->angle = mesGetSprite->sprite->GetAngle() + m_outputAngle;
    paramMovement1->parentRadius = mesGetSprite->sprite->GetRadius();
    paramMovement1->acceleration = m_speed;
    paramMovement1->maxAcc = m_speed;
    paramMovement1->angularSpeed = m_angularSpeed;

    getOwner()->GetEntityManager()->addComponent(ent1, Movement, paramMovement1);
    delete paramMovement1;

    //Follow Entity Movement component
    FollowEntityMovementParam *paramFollowEntityMovement1 = new FollowEntityMovementParam();
    paramFollowEntityMovement1->margin = m_margin;
    paramFollowEntityMovement1->time = m_time;
    getOwner()->GetEntityManager()->addComponent(ent1, FollowEntityAutonomousFighter, paramFollowEntityMovement1);
    delete paramFollowEntityMovement1;

    //Life component
    LifeParam *paramLife1 = new LifeParam();
    paramLife1->life = m_life;
    getOwner()->GetEntityManager()->addComponent(ent1, Life, paramLife1);
    delete paramLife1;

    //Entity 2
    Entity* ent2 = getOwner()->GetEntityManager()->addEntity();
    ent2->SetIdOwner(getOwner()->GetIdOwner());
    getOwner()->GetEntityManager()->addTarget(ent2);

    //Renderable Component
    RenderableParam *paramRenderable2 = new RenderableParam();
    paramRenderable2->spriteName = m_spriteName;

    getOwner()->GetEntityManager()->addComponent(ent2, Renderable, paramRenderable2);
    delete paramRenderable2;

    //Movement Component
    MovementParam *paramMovement2 = new MovementParam();
    paramMovement2->initialX = mesGetSprite->sprite->GetX();
    paramMovement2->initialY = mesGetSprite->sprite->GetY();
    paramMovement2->angle = mesGetSprite->sprite->GetAngle() - m_outputAngle;
    paramMovement2->parentRadius = mesGetSprite->sprite->GetRadius();
    paramMovement2->acceleration = m_speed;
    paramMovement2->maxAcc = m_speed;
    paramMovement2->angularSpeed = m_angularSpeed;

    getOwner()->GetEntityManager()->addComponent(ent2, Movement, paramMovement2);
    delete paramMovement2;

    //Follow Entity Movement component
    FollowEntityMovementParam *paramFollowEntityMovement2 = new FollowEntityMovementParam();
    paramFollowEntityMovement2->margin = m_margin;
    paramFollowEntityMovement2->time = m_time;
    getOwner()->GetEntityManager()->addComponent(ent2, FollowEntityAutonomousFighter, paramFollowEntityMovement2);
    delete paramFollowEntityMovement2;

    //Life component
    LifeParam *paramLife2 = new LifeParam();
    paramLife2->life = m_life;
    getOwner()->GetEntityManager()->addComponent(ent2, Life, paramLife2);
    delete paramLife2;

    delete mesGetSprite;
  }
}
