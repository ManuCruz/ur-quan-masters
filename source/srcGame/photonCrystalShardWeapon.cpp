#include "../includeGame/photonCrystalShardWeapon.h"
#include "../includeGame/renderable.h"
#include "../includeGame/movement.h"
#include "../includeGame/lineAutoMovement.h"
#include "../includeGame/doFriendlyDamage.h"
#include "../includeGame/destroyWhenDoDamage.h"
#include "../includeGame/destroyAfterDistance.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

// PHOTON CRYSTAL SHARD WEAPON
C_PhotonCrystalShardWeapon::C_PhotonCrystalShardWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig) {
  m_shooting = false;
  m_initShoot = false;
  String soundNameStart;
  String soundNameEnd;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("PhotonCrystalShardWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundNameStart"))
        soundNameStart = String(node->first_attribute("soundNameStart")->value());
      if (node->first_attribute("soundNameEnd"))
        soundNameEnd = String(node->first_attribute("soundNameEnd")->value());
      if (node->first_attribute("numOfFragment"))
        m_numOfFragment = String(node->first_attribute("numOfFragment")->value()).ToInt();
    }

    xml_node<>* nodeFragment = doc.first_node("game")->first_node("entityRepository")->first_node("FragmentPhotonCrystal");
    if (nodeFragment){
      if (nodeFragment->first_attribute("spriteShot"))
        m_spriteFragmentName = String(nodeFragment->first_attribute("spriteShot")->value());
      if (nodeFragment->first_attribute("damage"))
        m_damageFragment = String(nodeFragment->first_attribute("damage")->value()).ToFloat();
      if (nodeFragment->first_attribute("distance"))
        m_distanceFragment = String(nodeFragment->first_attribute("distance")->value()).ToFloat();
      if (nodeFragment->first_attribute("speed"))
        m_speedFragment = String(nodeFragment->first_attribute("speed")->value()).ToFloat();
    }

  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundNameStart.Split("_")[0] + "/" + soundNameStart + ".wav"));
  m_aSourceEnd = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundNameEnd.Split("_")[0] + "/" + soundNameEnd + ".wav"));
  m_aSource->SetLooping(false);
}

void C_PhotonCrystalShardWeapon::Update(double elapsed){
  if (!m_shooting && m_initShoot){ //si no est� disparando y el disparo se ha iniciado, detonarlo
    Detonate();
  }

  m_shooting = false;
  C_Weapon::Update(elapsed);
}

void C_PhotonCrystalShardWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_PhotonCrystalShardWeapon::Shooting(){
  m_shooting = true;

  if (m_bomb)
    if (m_bomb->GetIdOwner() == getOwner()->GetId())
      if (m_aSource)
        if (!m_aSource->IsPlaying())
          m_aSource->Play();

  M_GetBattery *mesBattery = new M_GetBattery();
  getOwner()->ReceiveMessage(mesBattery);
  if (mesBattery->amount >= m_energy){
    if (!m_initShoot){
      SpendEnergy();
      m_cooling = true;
      m_initShoot = true;

      //Entity
      m_bomb = getOwner()->GetEntityManager()->addEntity();
      m_bomb->SetIdOwner(getOwner()->GetIdOwner());

      //Renderable Component
      RenderableParam *paramRenderable = new RenderableParam();
      paramRenderable->spriteName = m_spriteName;

      getOwner()->GetEntityManager()->addComponent(m_bomb, Renderable, paramRenderable);
      delete paramRenderable;

      //Movement Component
      MovementParam *paramMovement = new MovementParam();

      M_GetSprite *mesGetSprite = new M_GetSprite();
      getOwner()->ReceiveMessage(mesGetSprite);
      paramMovement->initialX = mesGetSprite->sprite->GetX();
      paramMovement->initialY = mesGetSprite->sprite->GetY();
      paramMovement->angle = mesGetSprite->sprite->GetAngle();
      paramMovement->parentRadius = mesGetSprite->sprite->GetRadius();
      delete mesGetSprite;

      paramMovement->acceleration = m_speed;
      paramMovement->maxAcc = m_speed;
      paramMovement->angularSpeed = m_angularSpeed;

      getOwner()->GetEntityManager()->addComponent(m_bomb, Movement, paramMovement);
      delete paramMovement;

      //Line Auto Movement Component
      LineAutoMovementParam *paramLineAutoMovement = new LineAutoMovementParam();
      getOwner()->GetEntityManager()->addComponent(m_bomb, LineAutoMovement, paramLineAutoMovement);
      delete paramLineAutoMovement;

      //Do friendly damage component
      DoFriendlyDamageParam *paramDoFriendlyDamage = new DoFriendlyDamageParam();
      paramDoFriendlyDamage->damage = m_power;
      paramDoFriendlyDamage->owner = getOwner()->GetId();

      getOwner()->GetEntityManager()->addComponent(m_bomb, DoFriendlyDamage, paramDoFriendlyDamage);
      delete paramDoFriendlyDamage;

      //Destroy When Do Damage component
      getOwner()->GetEntityManager()->addComponent(m_bomb, DestroyWhenDoDamage, NULL);
    }
  }
  delete mesBattery;
}

void C_PhotonCrystalShardWeapon::Detonate(){
  m_initShoot = false;
  m_aSource->Stop();

  if (m_bomb){
    if (m_bomb->GetIdOwner() == getOwner()->GetId()){
      if (m_aSourceEnd)
        if (!m_aSourceEnd->IsPlaying())
          m_aSourceEnd->Play();

      M_GetSprite *mesGetSprite = new M_GetSprite();
      m_bomb->ReceiveMessage(mesGetSprite);

      //generate minibullet
      for (uint32 i = 0; i < m_numOfFragment; i++){
        //Entity
        Entity* ent = getOwner()->GetEntityManager()->addEntity();
        ent->SetIdOwner(getOwner()->GetIdOwner());

        //Renderable Component
        RenderableParam *paramRenderable = new RenderableParam();
        paramRenderable->spriteName = m_spriteFragmentName;

        getOwner()->GetEntityManager()->addComponent(ent, Renderable, paramRenderable);
        delete paramRenderable;

        //Movement Component
        MovementParam *paramMovement = new MovementParam();
        paramMovement->initialX = mesGetSprite->sprite->GetX() ;
        paramMovement->initialY = mesGetSprite->sprite->GetY();
        paramMovement->angle = i * 360 / m_numOfFragment;
        paramMovement->parentRadius = mesGetSprite->sprite->GetRadius();

        paramMovement->acceleration = m_speedFragment;
        paramMovement->maxAcc = m_speedFragment;

        getOwner()->GetEntityManager()->addComponent(ent, Movement, paramMovement);
        delete paramMovement;

        //Line Auto Movement Component
        LineAutoMovementParam *paramLineAutoMovement = new LineAutoMovementParam();
        getOwner()->GetEntityManager()->addComponent(ent, LineAutoMovement, paramLineAutoMovement);
        delete paramLineAutoMovement;

        //Destroy After Distance Component
        DestroyAfterDistanceParam *paramDestroyAfterDistance = new DestroyAfterDistanceParam();
        paramDestroyAfterDistance->speed = m_speedFragment;
        paramDestroyAfterDistance->distance = m_distanceFragment;

        getOwner()->GetEntityManager()->addComponent(ent, DestroyAfterDistance, paramDestroyAfterDistance);
        delete paramDestroyAfterDistance;

        //Do friendly damage component
        DoFriendlyDamageParam *paramDoFriendlyDamage = new DoFriendlyDamageParam();
        paramDoFriendlyDamage->damage = m_damageFragment;
        paramDoFriendlyDamage->owner = getOwner()->GetId();

        getOwner()->GetEntityManager()->addComponent(ent, DoFriendlyDamage, paramDoFriendlyDamage);
        delete paramDoFriendlyDamage;
      }

      delete mesGetSprite;

      //destroy bomb
      getOwner()->GetEntityManager()->deleteEntity(m_bomb);
    }
  }
}