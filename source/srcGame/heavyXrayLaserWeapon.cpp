#include "../includeGame/heavyXrayLaserWeapon.h"

#include "../include/sprite.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//HEAVY X-RAY LASER WEAPON
C_HeavyXrayLaserWeapon::C_HeavyXrayLaserWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_LaserWeapon(owner, param, fileConfig) {
  String soundName;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("HeavyXrayLaserWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_HeavyXrayLaserWeapon::Update(double elapsed){
  C_LaserWeapon::Update(elapsed);
}

void C_HeavyXrayLaserWeapon::ReceiveMessage(Message *msg) {
  C_LaserWeapon::ReceiveMessage(msg);
}

void C_HeavyXrayLaserWeapon::Shooting(){
  C_LaserWeapon::Shooting();
}
