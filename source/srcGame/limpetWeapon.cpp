#include "../includeGame/limpetWeapon.h"
#include "../includeGame/renderable.h"
#include "../includeGame/movement.h"
#include "../includeGame/followEntityMovement.h"
#include "../includeGame/reduceMobility.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;
#include "math.h"

// LIMPET WEAPON
C_LimpetWeapon::C_LimpetWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig) {
  String soundName;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("LimpetWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_LimpetWeapon::Update(double elapsed){
  C_Weapon::Update(elapsed);
}

void C_LimpetWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_LimpetWeapon::Shooting(){
  C_Weapon::Shooting();
  if (m_canShot){
    //Entity
    Entity* ent = getOwner()->GetEntityManager()->addEntity();
    ent->SetIdOwner(getOwner()->GetIdOwner());

    //Renderable Component
    RenderableParam *paramRenderable = new RenderableParam();
    paramRenderable->spriteName = m_spriteName;

    getOwner()->GetEntityManager()->addComponent(ent, Renderable, paramRenderable);
    delete paramRenderable;

    //Movement Component
    MovementParam *paramMovement = new MovementParam();

    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    paramMovement->initialX = mesGetSprite->sprite->GetX();
    paramMovement->initialY = mesGetSprite->sprite->GetY();
    paramMovement->angle = mesGetSprite->sprite->GetAngle();
    paramMovement->parentRadius = mesGetSprite->sprite->GetRadius();
    delete mesGetSprite;

    paramMovement->acceleration = m_speed;
    paramMovement->maxAcc = m_speed;
    paramMovement->angularSpeed = m_angularSpeed;

    getOwner()->GetEntityManager()->addComponent(ent, Movement, paramMovement);
    delete paramMovement;

    //Follow Entity Movement component
    FollowEntityMovementParam *paramFollowEntityMovement = new FollowEntityMovementParam();
    paramFollowEntityMovement->margin = 0;
    paramFollowEntityMovement->time = INFINITY;
    getOwner()->GetEntityManager()->addComponent(ent, FollowEntityEmpty, paramFollowEntityMovement);
    delete paramFollowEntityMovement;

    //Reduce mobility component
    ReduceMobilityParam *paramReduceMobility = new ReduceMobilityParam();
    paramReduceMobility->percentage = m_power;
    paramReduceMobility->owner = getOwner()->GetId();

    getOwner()->GetEntityManager()->addComponent(ent, ReduceMobility, paramReduceMobility);
    delete paramReduceMobility;
  }
}