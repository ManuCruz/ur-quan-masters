#include "../includeGame/sound.h"

#include "../include/audioSource.h"
#include "../include/resourceManager.h"

#define RES ResourceManager::Instance()

//SOUND
C_Sound::C_Sound(Entity* owner, SoundParam *param, String fileConfig) : Component(owner){
  String soundName = param->soundName;
  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(param->looping);

  m_play = false;
}

C_Sound::~C_Sound(){
  if (m_aSource)
    delete m_aSource;
}

void C_Sound::Update(double elapsed){
  if (!m_play){
    m_aSource->Play();
    m_play = true;
  }
}

void C_Sound::ReceiveMessage(Message *msg) {
}