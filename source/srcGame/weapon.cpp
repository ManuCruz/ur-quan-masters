#include "../includeGame/weapon.h"

#include "../include/audioSource.h"

//WEAPON
C_Weapon::C_Weapon(Entity* owner, WeaponParam *param, String fileConfig) : Component(owner){
  m_name = param->name;
  m_id = param->id;
  m_energy = param->energy;
  m_reload = param->reload;
  m_power = param->power;
  m_distance = param->distance;
  m_speed = param->speed;
  m_angularSpeed = param->angularSpeed;
  m_cooling = false;
  m_coolingTime = 0;
  m_shot = false;
  m_aSource = NULL;
}

C_Weapon::~C_Weapon(){
  if (m_aSource)
    delete m_aSource;
};

void C_Weapon::Update(double elapsed){
  if (m_cooling){
    m_coolingTime += elapsed;
    if (m_coolingTime >= m_reload){
      m_cooling = false;
      m_coolingTime = 0;
    }
  }

  if (m_shot && !m_cooling)
      Shooting();

  m_shot = false;
}

void C_Weapon::Shooting() {
  m_canShot = false;
  M_GetBattery *mesBattery = new M_GetBattery();
  getOwner()->ReceiveMessage(mesBattery);
  if (mesBattery->amount >= m_energy){
    SpendEnergy();
    m_canShot = true;
    m_cooling = true;
    if (m_aSource)
      if (!m_aSource->IsPlaying())
        m_aSource->Play();
  }
  delete mesBattery;
}

void C_Weapon::ReceiveMessage(Message *msg) {
  M_Shot *mesShot = dynamic_cast<M_Shot*>(msg);
  if (mesShot)
    if (mesShot->id == m_id)
      m_shot = true;
}

void C_Weapon::SpendEnergy(){
  M_SpendBattery *mesBattery = new M_SpendBattery();
  mesBattery->amount = m_energy;
  getOwner()->ReceiveMessage(mesBattery);
  delete mesBattery;
}
