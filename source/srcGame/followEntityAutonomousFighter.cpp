#include "../includeGame/followEntityAutonomousFighter.h"
#include "../includeGame/renderable.h"
#include "../includeGame/movement.h"
#include "../includeGame/lineAutoMovement.h"
#include "../includeGame/destroyAfterDistance.h"
#include "../includeGame/doDamage.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//FOLLOW ENTITY AUTONOMOUS FIGHTER
C_FollowEntityAutonomousFighter::C_FollowEntityAutonomousFighter(Entity* owner, FollowEntityMovementParam *param, String fileConfig) : C_FollowEntityMovement(owner, param, fileConfig){
  m_shot = false;
  m_currentTimeToShot = 0;

  m_spriteName = "DreadNaugh_shot_3";
  m_damage = 3;
  m_distance = 100;
  m_speed = 20;
  m_timeToShot = 1;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("FollowEntityAutonomousFighter");
    if (node){
      if (node->first_attribute("spriteBullet"))
        m_spriteName = String(node->first_attribute("spriteBullet")->value());
      if (node->first_attribute("maxDamageBullet"))
        m_damage = String(node->first_attribute("maxDamageBullet")->value()).ToInt();
      if (node->first_attribute("distanceBullet"))
        m_distance = String(node->first_attribute("distanceBullet")->value()).ToInt();
      if (node->first_attribute("speedBullet"))
        m_speed = String(node->first_attribute("speedBullet")->value()).ToInt();
      if (node->first_attribute("timeToShot"))
        m_timeToShot = String(node->first_attribute("timeToShot")->value()).ToFloat();
    }
  }
}

void C_FollowEntityAutonomousFighter::Update(double elapsed){
  C_FollowEntityMovement::Update(elapsed);
  
  m_currentTimeToShot += elapsed;

  if (m_shot && m_currentTimeToShot > m_timeToShot){
    m_currentTimeToShot = 0;
    //Entity
    Entity* ent = getOwner()->GetEntityManager()->addEntity();
    ent->SetIdOwner(getOwner()->GetIdOwner());

    //Renderable Component
    RenderableParam *paramRenderable = new RenderableParam();
    paramRenderable->spriteName = m_spriteName;

    getOwner()->GetEntityManager()->addComponent(ent, Renderable, paramRenderable);
    delete paramRenderable;

    //Movement Component
    MovementParam *paramMovement = new MovementParam();

    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    paramMovement->initialX = mesGetSprite->sprite->GetX();
    paramMovement->initialY = mesGetSprite->sprite->GetY();
    paramMovement->angle = mesGetSprite->sprite->GetAngle();
    paramMovement->parentRadius = mesGetSprite->sprite->GetRadius();
    delete mesGetSprite;

    paramMovement->acceleration = m_speed;
    paramMovement->maxAcc = m_speed;

    getOwner()->GetEntityManager()->addComponent(ent, Movement, paramMovement);
    delete paramMovement;

    //Line Auto Movement Component
    LineAutoMovementParam *paramLineAutoMovement = new LineAutoMovementParam();
    getOwner()->GetEntityManager()->addComponent(ent, LineAutoMovement, paramLineAutoMovement);
    delete paramLineAutoMovement;

    //Destroy After Distance Component
    DestroyAfterDistanceParam *paramDestroyAfterDistance = new DestroyAfterDistanceParam();
    paramDestroyAfterDistance->speed = m_speed;
    paramDestroyAfterDistance->distance = m_distance;

    getOwner()->GetEntityManager()->addComponent(ent, DestroyAfterDistance, paramDestroyAfterDistance);
    delete paramDestroyAfterDistance;

    //Do damage component
    DoDamageParam *paramDoDamage = new DoDamageParam();
    paramDoDamage->damage = (rand() % m_damage) + 1;
    paramDoDamage->owner = getOwner()->GetId();

    getOwner()->GetEntityManager()->addComponent(ent, DoDamage, paramDoDamage);
    delete paramDoDamage;

    //Destroy When Do Damage component
    getOwner()->GetEntityManager()->addComponent(ent, DestroyWhenDoDamage, NULL);
  }

  m_shot = false;
}

void C_FollowEntityAutonomousFighter::ReceiveMessage(Message *msg) {
  C_FollowEntityMovement::ReceiveMessage(msg);
}

void C_FollowEntityAutonomousFighter::OnTheTarget(){
  m_shot = true;
}

void C_FollowEntityAutonomousFighter::OnTheOrigin(){
  getOwner()->GetEntityManager()->deleteEntity(getOwner());
}