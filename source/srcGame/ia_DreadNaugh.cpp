#include "../includeGame/ia_DreadNaugh.h"
#include "../includeGame/ia_Marker.h"
#include "../includeGame/entityManager.h"

//IA
#include "../BehaviorTrees/Trees/DreadNaughBT.h"

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//IA_DREARNAUGH
C_IA_DreadNaugh::C_IA_DreadNaugh(Entity* owner, IADreadNaughParam *param, String fileConfig) : Component(owner){
  m_pIATree = new DreadNaughBT(owner, param);

  //IA_marker component
  MarkerParam *paramMarker = new MarkerParam();
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("IA_Marker");
    if (node){
      if (node->first_attribute("spriteMarker"))
        paramMarker->spriteName = String(node->first_attribute("spriteMarker")->value());
      if (node->first_attribute("distance"))
        paramMarker->distance = String(node->first_attribute("distance")->value()).ToFloat();
      if (node->first_attribute("angle"))
        paramMarker->angle = String(node->first_attribute("angle")->value()).ToFloat();
    }
  }

  getOwner()->GetEntityManager()->addComponent(getOwner(), IA_Marker, paramMarker);

  delete paramMarker;
}

void C_IA_DreadNaugh::Update(double elapsed){
  m_pIATree->run();
}

void C_IA_DreadNaugh::ReceiveMessage(Message *msg){
}
