#include "../includeGame/randomTeleportationWeapon.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/math.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//RANDOM TELEPORTATION WEAPON
C_RandomTeleportationWeapon::C_RandomTeleportationWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig){
  getOwner()->GetEntityManager()->getWorldSize(m_maxWidth, m_maxHeight);
  m_checkCollision = 0;

  String soundName;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("RandomTeleportationWeapon");
    if (node){
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_RandomTeleportationWeapon::Update(double elapsed){
  C_Weapon::Update(elapsed);

  if (m_checkCollision){
    m_checkCollision--;
    M_HasCollision *mesHasCollision = new M_HasCollision();
    getOwner()->ReceiveMessage(mesHasCollision);
    if (mesHasCollision->answer){
      M_GetLife *mesGetLife = new M_GetLife();
      getOwner()->ReceiveMessage(mesGetLife);

      M_LoseLife *mesLoseLife = new M_LoseLife();
      mesLoseLife->amount = mesGetLife->amount;
      getOwner()->ReceiveMessage(mesLoseLife);

      delete mesLoseLife;
      delete mesGetLife;
    }
    delete mesHasCollision;
  }
}

void C_RandomTeleportationWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_RandomTeleportationWeapon::Shooting(){
  C_Weapon::Shooting();
  if (m_canShot){
    if (!m_sprite){
      M_GetSprite *mesGetSprite = new M_GetSprite();
      getOwner()->ReceiveMessage(mesGetSprite);
      m_sprite = mesGetSprite->sprite;
      delete mesGetSprite;
    }
    if (m_sprite){
      m_sprite->SetPosition(WrapValue(rand(), m_maxWidth), WrapValue(rand(), m_maxHeight));
      m_checkCollision = 2;
    }
  }
}
