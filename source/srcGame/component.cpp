#include "../includeGame/component.h"

Component::Component(Entity* owner) { 
  m_Owner = owner; 
}

Component::~Component(){
}

Entity* Component::getOwner() const { 
  return m_Owner; 
}