#include "../includeGame/autoAimingLaserWeapon.h"
#include "../includeGame/renderableLaser.h"
#include "../includeGame/doDamage.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/math.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "math.h"

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//AUTO AIMING LASER WEAPON
C_AutoAimingLaserWeapon::C_AutoAimingLaserWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_LaserWeapon(owner, param, fileConfig) {
  String soundName;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("AutoAimingLaserWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_AutoAimingLaserWeapon::Update(double elapsed){
  C_LaserWeapon::Update(elapsed);
}

void C_AutoAimingLaserWeapon::ReceiveMessage(Message *msg) {
  C_LaserWeapon::ReceiveMessage(msg);
}

void C_AutoAimingLaserWeapon::Shooting(){
  C_Weapon::Shooting();

  if (m_canShot){
    //Entity
    Entity* ent = getOwner()->GetEntityManager()->addEntity();
    ent->SetIdOwner(getOwner()->GetIdOwner());

    //Renderable laser Component
    RenderableLaserParam *paramRenderableLaser = new RenderableLaserParam();
    paramRenderableLaser->spriteName = m_spriteName;
    paramRenderableLaser->lenght = m_distance;

    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    paramRenderableLaser->x = mesGetSprite->sprite->GetX();
    paramRenderableLaser->y = mesGetSprite->sprite->GetY();

    double xTarget = INFINITY;
    double yTarget = INFINITY;
    double radiusTarget = 0.;
    getOwner()->GetEntityManager()->getInfoNearestTarget(getOwner()->GetId(), getOwner()->GetIdOwner(), xTarget, yTarget, radiusTarget);

    double angle = atan2(mesGetSprite->sprite->GetY() - yTarget, xTarget - mesGetSprite->sprite->GetX()) * 57.2957795;

    if (xTarget == INFINITY || yTarget == INFINITY)
      paramRenderableLaser->angle = mesGetSprite->sprite->GetAngle();
    else
      paramRenderableLaser->angle = angle;

    paramRenderableLaser->parentRadius = mesGetSprite->sprite->GetRadius()*1.5;
    delete mesGetSprite;

    getOwner()->GetEntityManager()->addComponent(ent, RenderableLaser, paramRenderableLaser);
    delete paramRenderableLaser;

    //Do damage component
    DoDamageParam *paramDoDamage = new DoDamageParam();
    paramDoDamage->damage = m_power;
    paramDoDamage->owner = getOwner()->GetId();

    getOwner()->GetEntityManager()->addComponent(ent, DoDamage, paramDoDamage);
    delete paramDoDamage;
  }
}
