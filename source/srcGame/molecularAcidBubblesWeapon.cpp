#include "../includeGame/molecularAcidBubblesWeapon.h"
#include "../includeGame/renderable.h"
#include "../includeGame/movement.h"
#include "../includeGame/followEntityMovement.h"
#include "../includeGame/randomRotation.h"
#include "../includeGame/doDamage.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;
#include "math.h"

//MOLECULAR ACID BUBBLES WEAPON
C_MolecularAcidBubblesWeapon::C_MolecularAcidBubblesWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig){
  String soundName;

  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("MolecularAcidBubblesWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
      if (node->first_attribute("changeTime"))
        m_changeTime = String(node->first_attribute("changeTime")->value()).ToFloat();
      if (node->first_attribute("randomTime"))
        m_randomTime = String(node->first_attribute("randomTime")->value()).ToFloat();
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_MolecularAcidBubblesWeapon::Update(double elapsed){
  C_Weapon::Update(elapsed);
}

void C_MolecularAcidBubblesWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_MolecularAcidBubblesWeapon::Shooting(){
  C_Weapon::Shooting();
  if (m_canShot){
    //Entity
    Entity* ent = getOwner()->GetEntityManager()->addEntity();
    ent->SetIdOwner(getOwner()->GetIdOwner());
    getOwner()->GetEntityManager()->addTarget(ent);

    //Renderable Component
    RenderableParam *paramRenderable = new RenderableParam();
    paramRenderable->spriteName = m_spriteName;

    getOwner()->GetEntityManager()->addComponent(ent, Renderable, paramRenderable);
    delete paramRenderable;

    //Movement Component
    MovementParam *paramMovement = new MovementParam();

    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    paramMovement->initialX = mesGetSprite->sprite->GetX();
    paramMovement->initialY = mesGetSprite->sprite->GetY();
    paramMovement->angle = mesGetSprite->sprite->GetAngle();
    paramMovement->parentRadius = mesGetSprite->sprite->GetRadius();
    delete mesGetSprite;

    paramMovement->acceleration = m_speed;
    paramMovement->maxAcc = m_speed;
    paramMovement->angularSpeed = m_angularSpeed;

    getOwner()->GetEntityManager()->addComponent(ent, Movement, paramMovement);
    delete paramMovement;

    //Random Rotation component
    RandomRotationParam *paramRandomRotation = new RandomRotationParam();
    paramRandomRotation->changeTime = m_changeTime;
    paramRandomRotation->randomTime = m_randomTime;
    getOwner()->GetEntityManager()->addComponent(ent, RandomRotation, paramRandomRotation);
    delete paramRandomRotation;

    //Follow Entity Movement component
    FollowEntityMovementParam *paramFollowEntityMovement = new FollowEntityMovementParam();
    paramFollowEntityMovement->margin = 0;
    paramFollowEntityMovement->time = INFINITY;
    getOwner()->GetEntityManager()->addComponent(ent, FollowEntityEmpty, paramFollowEntityMovement);
    delete paramFollowEntityMovement;

    //Do damage component
    DoDamageParam *paramDoDamage = new DoDamageParam();
    paramDoDamage->damage = m_power;
    paramDoDamage->owner = getOwner()->GetId();

    getOwner()->GetEntityManager()->addComponent(ent, DoDamage, paramDoDamage);
    delete paramDoDamage;

    //Destroy When Do Damage component
    getOwner()->GetEntityManager()->addComponent(ent, DestroyWhenDoDamage, NULL);
  }
}
