#include "../includeGame/followEntityMovement.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "math.h"

//FOLLOW ENTITY MOVEMENT
C_FollowEntityMovement::C_FollowEntityMovement(Entity* owner, FollowEntityMovementParam *param, String fileConfig) : Component(owner){
  m_sprite = NULL;
  m_margin = param->margin;
  m_followTime = param->time;
  m_currentTime = 0;
}

void C_FollowEntityMovement::Update(double elapsed){
  m_currentTime += elapsed;

  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    double xTarget = INFINITY;
    double yTarget = INFINITY;
    double radiusTarget = 0.;
    if (m_currentTime < m_followTime)
      getOwner()->GetEntityManager()->getInfoNearestEnemy(getOwner()->GetIdOwner(), xTarget, yTarget, radiusTarget);
    else{
      m_margin = 0;
      getOwner()->GetEntityManager()->getInfoEntity(getOwner()->GetIdOwner(), xTarget, yTarget, radiusTarget);
    }

    //move (if it is need)
    M_Movement *mesMovement = new M_Movement();
    double dist = pow(m_sprite->GetX() - xTarget, 2) + pow(m_sprite->GetY() - yTarget, 2);
    double rad = pow(m_margin + radiusTarget, 2);
    if (dist > rad){
      mesMovement->push = true;
    }

    //cross product
    double xUp = m_sprite->GetUpX();
    double yUp = m_sprite->GetUpY();
    double xV = xTarget - m_sprite->GetX();
    double yV = -(yTarget - m_sprite->GetY());

    double res = xV * yUp + yV * xUp;

    //rotate myself (if it is need)
    if (res > 0)
      mesMovement->left = true;
    else
      mesMovement->right = true;

    //change direction (ajuste para que aprovecha la salida por un borde para salir por el opuesto)
    double maxWidth, maxHeight;
    getOwner()->GetEntityManager()->getWorldSize(maxWidth, maxHeight);
    bool checkX = abs(m_sprite->GetX() - xTarget) > maxWidth / 2;
    bool checkY = abs(m_sprite->GetY() - yTarget) > maxHeight / 2;
    if (checkX || checkY){
      mesMovement->left = !mesMovement->left;
      mesMovement->right = !mesMovement->right;
    }

    getOwner()->ReceiveMessage(mesMovement);
    delete mesMovement;

    if (dist < m_margin + rad){
      if (m_currentTime < m_followTime){
        OnTheTarget();
      }
      else{
        OnTheOrigin();
      }
    }
  }
}

void C_FollowEntityMovement::ReceiveMessage(Message *msg) {
}
