#include "../includeGame/movement.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"

//MOVEMENT
C_Movement::C_Movement(Entity* owner, MovementParam *param, String fileConfig) : Component(owner) {
  m_sprite = NULL;
  m_initialX = param->initialX;
  m_initialY = param->initialY;
  m_currentSpeed = 0;
  m_acceleration = param->acceleration;
  m_maxAcc = param->maxAcc;
  m_angularSpeed = param->angularSpeed;
  m_angle = param->angle;
  m_parentRadius = param->parentRadius;

  m_push, m_right, m_left = false;
  m_hasMessage = false;
  m_alwaysRun = false;
  getOwner()->GetEntityManager()->getWorldSize(m_worldWidth, m_worldHeight);
}

void C_Movement::Update(double elapsed) {
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;

    m_sprite->SetAngle(m_angle);
    double dirX = m_sprite->GetUpX();
    double dirY = m_sprite->GetUpY();
    m_sprite->SetPosition(m_initialX + dirX * m_parentRadius, m_initialY + dirY * m_parentRadius);
  }
  if (m_sprite){
    if (m_alwaysRun)
      m_push = true;

    if (m_push){
      m_currentSpeed += m_acceleration;
      if (m_currentSpeed > m_maxAcc)
        m_currentSpeed = m_maxAcc;
    }
    else if (m_currentSpeed > 0){
      m_currentSpeed -= m_acceleration;
      if (m_currentSpeed < 0)
        m_currentSpeed = 0;
    }

    if (m_right)
      m_sprite->SetAngle(m_sprite->GetAngle() - m_angularSpeed * elapsed);
    if (m_left)
      m_sprite->SetAngle(m_sprite->GetAngle() + m_angularSpeed * elapsed);

    if (m_currentSpeed > 0){
      double dirX = m_sprite->GetUpX();
      double dirY = m_sprite->GetUpY();
      double distanceThisFrame = m_currentSpeed * elapsed;
      double posX = m_sprite->GetX() + dirX * distanceThisFrame;
      double posY = m_sprite->GetY() + dirY * distanceThisFrame;
      if (posX > m_worldWidth && posX > 0)
        posX -= m_worldWidth;
      else if (posX < 0)
        posX += m_worldWidth;
      if (posY > m_worldHeight && posY > 0)
        posY -= m_worldHeight;
      else if (posY < 0)
        posY += m_worldHeight;

      m_sprite->SetPosition(posX, posY);
    }

    //input reset
    m_push, m_right, m_left = false;
    m_hasMessage = false;
  }
}

void C_Movement::ReceiveMessage(Message *msg) {
  M_Movement *mesMovement = dynamic_cast<M_Movement*>(msg);
  if (mesMovement && !m_hasMessage){
    m_push = mesMovement->push;
    m_right = mesMovement->right;
    m_left = mesMovement->left;
    m_hasMessage = true;
  }

  M_GetMaxAcc *mesGetMaxAcc = dynamic_cast<M_GetMaxAcc*>(msg);
  if (mesGetMaxAcc){
    mesGetMaxAcc->amount = m_maxAcc;
  }

  M_SetMaxAcc *mesSetMaxAcc = dynamic_cast<M_SetMaxAcc*>(msg);
  if (mesSetMaxAcc){
    m_maxAcc = mesSetMaxAcc->amount;
  }

  M_ChangeSpeed *mesChangeSpeed = dynamic_cast<M_ChangeSpeed*>(msg);
  if (mesChangeSpeed){
    m_acceleration *= mesChangeSpeed->factor;
    m_maxAcc *= mesChangeSpeed->factor;
    m_angularSpeed *= mesChangeSpeed->factor;
    m_alwaysRun = mesChangeSpeed->force;
  }
}
