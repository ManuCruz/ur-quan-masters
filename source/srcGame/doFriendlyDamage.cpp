#include "../includeGame/doFriendlyDamage.h"

//DO FRIENDLY DAMAGE
C_DoFriendlyDamage::C_DoFriendlyDamage(Entity* owner, DoFriendlyDamageParam *param, String fileConfig) : Component(owner){
  m_damage = param->damage;
  m_owner = param->owner;
}

void C_DoFriendlyDamage::Update(double elapsed){
}

void C_DoFriendlyDamage::ReceiveMessage(Message *msg) {
  M_CollisionWith *mesCollisionWith = dynamic_cast<M_CollisionWith*>(msg);
  if (mesCollisionWith){
    if (mesCollisionWith->ent->GetId() != getOwner()->GetIdOwner() && mesCollisionWith->ent->GetIdOwner() != NO_OWNER_ENTITY){
      M_LoseLife *mesLoseLife = new M_LoseLife();
      mesLoseLife->amount = m_damage;
      mesCollisionWith->ent->ReceiveMessage(mesLoseLife);
      delete mesLoseLife;

      M_IDoDamage *mesIDoDamage = new M_IDoDamage();
      getOwner()->ReceiveMessage(mesIDoDamage);
      delete mesIDoDamage;
    }
  }
}
