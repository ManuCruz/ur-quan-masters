#include "../includeGame/shipStats.h"
#include "../includeGame/renderable.h"
#include "../includeGame/autoRotation.h"
#include "../includeGame/movement.h"
#include "../includeGame/sound.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//SHIP_STATS
C_ShipStats::C_ShipStats(Entity* owner, ShipStatsParam *param, String fileConfig) : Component(owner) {
  m_name = param->name;
  m_maxLife = param->maxLife;
  m_maxBattery = param->maxBattery;
  m_value = param->value;
  m_regBattery = param->regBattery;
  m_currentLife = m_maxLife;
  m_currentBattery = m_maxBattery;
  getOwner()->SetIdOwner(getOwner()->GetId());

  m_spriteExplosion = "Explosion";
  m_soundExplosion = "Explosion_1";
  m_angularSpeedExplosion = 45;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>(const_cast<char *>(file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("ShipStats");
    if (node){
      if (node->first_attribute("spriteExplosion"))
        m_spriteExplosion = String(node->first_attribute("spriteExplosion")->value());
      if (node->first_attribute("soundExplosion"))
        m_soundExplosion = String(node->first_attribute("soundExplosion")->value());
      if (node->first_attribute("angularSpeedExplosion"))
        m_angularSpeedExplosion = String(node->first_attribute("angularSpeedExplosion")->value()).ToFloat();
    }
  }
}

void C_ShipStats::Update(double elapsed) {
  //recharge the battery
  if (m_currentBattery < m_maxBattery){
    m_currentBattery += m_regBattery * elapsed;
    if (m_currentBattery > m_maxBattery)
      m_currentBattery = m_maxBattery;
  }
}

void C_ShipStats::ReceiveMessage(Message *msg) {
  M_Stats *mesStats = dynamic_cast<M_Stats*>(msg);
  if (mesStats){
    mesStats->isShips = true;
    mesStats->name = m_name;
    mesStats->life = m_currentLife;
    mesStats->battery = (uint32)m_currentBattery;
  }

  M_LoseLife *mesLoseLife = dynamic_cast<M_LoseLife*>(msg);
  if (mesLoseLife){
    m_currentLife -= mesLoseLife->amount;
    if (m_currentLife <= 0){  //death
      getOwner()->GetEntityManager()->deleteEntity(getOwner());

      //Entity
      Entity* ent = getOwner()->GetEntityManager()->addEntity();
      ent->SetIdOwner(NO_OWNER_ENTITY);

      //Renderable Component
      RenderableParam *paramRenderable = new RenderableParam();
      paramRenderable->spriteName = m_spriteExplosion;

      getOwner()->GetEntityManager()->addComponent(ent, Renderable, paramRenderable);
      delete paramRenderable;

      //Auto Rotation Component
      AutoRotationParam *paramAutoRotation = new AutoRotationParam();
      paramAutoRotation->toRight = false;

      getOwner()->GetEntityManager()->addComponent(ent, AutoRotation, paramAutoRotation);
      delete paramAutoRotation;

      //Movement Component
      MovementParam *paramMovement = new MovementParam();

      M_GetSprite *mesGetSprite = new M_GetSprite();
      getOwner()->ReceiveMessage(mesGetSprite);
      paramMovement->initialX = mesGetSprite->sprite->GetX();
      paramMovement->initialY = mesGetSprite->sprite->GetY();
      delete mesGetSprite;

      paramMovement->angularSpeed = m_angularSpeedExplosion;

      getOwner()->GetEntityManager()->addComponent(ent, Movement, paramMovement);
      delete paramMovement;

      //Sound component
      SoundParam *paramSound = new SoundParam();
      paramSound->soundName = m_soundExplosion;
      getOwner()->GetEntityManager()->addComponent(ent, Sound, paramSound);
      delete paramSound;
    }
  }

  M_GetLife *mesGetLife = dynamic_cast<M_GetLife*>(msg);
  if (mesGetLife){
    mesGetLife->amount = m_currentLife;
  }

  M_SpendBattery *mesSpendBattery = dynamic_cast<M_SpendBattery*>(msg);
  if (mesSpendBattery){
    m_currentBattery -= mesSpendBattery->amount;
    if (m_currentBattery < 0)
      m_currentBattery = 0;
  }

  M_GetBattery *mesGetBattery = dynamic_cast<M_GetBattery*>(msg);
  if (mesGetBattery){
    mesGetBattery->amount = (uint32)m_currentBattery;
  }
}
