#include "../includeGame/randomRotation.h"
#include "../include/math.h"

//RANDOM ROTATION
C_RandomRotation::C_RandomRotation(Entity* owner, RandomRotationParam *param, String fileConfig) : Component(owner) {
  m_changeTime = param->changeTime;
  m_endTime = param->randomTime;
  m_currentTime = 0;
  m_init = false;
}

void C_RandomRotation::Update(double elapsed) {
  m_currentTime += elapsed;
  if (m_currentTime >= m_changeTime){
    if (!m_init){
      m_init = true;
      delete m_mesMovement;
      m_mesMovement = new M_Movement();
      m_mesMovement->push = true;
      if(rand()%2 == 0)
        m_mesMovement->right = true;
      else
        m_mesMovement->left = true;
      m_timer = m_changeTime + WrapValue(rand(), m_endTime);
    }
    if (m_currentTime < m_timer)
      getOwner()->ReceiveMessage(m_mesMovement);
    else
      m_currentTime = 0;
  }
  else{
    m_init = false;
  }
}

void C_RandomRotation::ReceiveMessage(Message *msg) {
}

C_RandomRotation::~C_RandomRotation(){
  delete m_mesMovement;
}