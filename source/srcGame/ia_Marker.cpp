#include "../includeGame/ia_Marker.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/math.h"

//IA_MARKER
C_IA_Marker::C_IA_Marker(Entity* owner, MarkerParam *param, String fileConfig) : Component(owner){
  m_spriteName = param->spriteName;
  m_sprite1 = param->sprite1;
  m_sprite2 = param->sprite2;
  m_distance = param->distance;
  m_angle = param->angle;
}

void C_IA_Marker::Update(double elapsed){
  if (!m_spriteOwner){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_spriteOwner = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_spriteOwner){
    m_sprite1->SetX(m_spriteOwner->GetX() + DegCos(m_spriteOwner->GetAngle() - m_angle) * m_distance);
    m_sprite1->SetY(m_spriteOwner->GetY() + DegSin(m_spriteOwner->GetAngle() - m_angle) * m_distance);

    m_sprite2->SetX(m_spriteOwner->GetX() + DegCos(m_spriteOwner->GetAngle() + m_angle) * m_distance);
    m_sprite2->SetY(m_spriteOwner->GetY() + DegSin(m_spriteOwner->GetAngle() + m_angle) * m_distance);

    //Si choca el 1, corregir movimiento hacia la izquierda
    if (m_sprite1->DidCollide()){
      uint32 nextIndex = 0;
      Entity *ent = getOwner()->GetEntityManager()->checkCollision(m_sprite1, 0, nextIndex);
      while (nextIndex != 0){
        if (ent->GetIdOwner() != getOwner()->GetIdOwner() && ent->GetIdOwner() != NO_OWNER_ENTITY){
          M_Movement *mesMovement = new M_Movement();
          mesMovement->push = true;
          mesMovement->left = true;
          getOwner()->ReceiveMessage(mesMovement);
          delete mesMovement;
        }
        ent = getOwner()->GetEntityManager()->checkCollision(m_sprite1, nextIndex, nextIndex);
      }
    }

    //si choca el dos, corregir el movimiento hacia la derecha
    if (m_sprite2->DidCollide()){
      uint32 nextIndex = 0;
      Entity *ent = getOwner()->GetEntityManager()->checkCollision(m_sprite2, 0, nextIndex);
      while (nextIndex != 0){
        if (ent->GetIdOwner() != getOwner()->GetIdOwner() && ent->GetIdOwner() != NO_OWNER_ENTITY){
          M_Movement *mesMovement = new M_Movement();
          mesMovement->push = true;
          mesMovement->right = true;
          getOwner()->ReceiveMessage(mesMovement);
          delete mesMovement;
        }
        ent = getOwner()->GetEntityManager()->checkCollision(m_sprite2, nextIndex, nextIndex);
      }
    }
  }
}

void C_IA_Marker::ReceiveMessage(Message *msg){

}