#include "../includeGame/blazerFormWeapon.h"
#include "../includeGame/doDamage.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/image.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//BLAZER FORM WEAPON
C_BlazerFormWeapon::C_BlazerFormWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig) {
  m_transform = false;

  String soundName;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("BlazerFormWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
      if (node->first_attribute("factor"))
        m_factor = String(node->first_attribute("factor")->value()).ToFloat();
      if (node->first_attribute("energyCost"))
        m_energyCost = String(node->first_attribute("energyCost")->value()).ToFloat();
    }
  }

  //Do damage component
  DoDamageParam *paramDoDamage = new DoDamageParam();
  paramDoDamage->damage = 0;
  paramDoDamage->owner = getOwner()->GetId();

  getOwner()->GetEntityManager()->addComponent(getOwner(), DoDamage, paramDoDamage);
  delete paramDoDamage;


  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);

  m_image = RES.LoadImage("data/assets/" + m_spriteName.Split("_")[0] + "/" + m_spriteName + ".png");
  m_image->SetMidHandle();
}

void C_BlazerFormWeapon::Update(double elapsed){
  if (!m_transform)
    C_Weapon::Update(elapsed);
  else{
    M_GetBattery *mesGetBattery = new M_GetBattery();
    getOwner()->ReceiveMessage(mesGetBattery);
    if (mesGetBattery->amount > 0){
      M_SpendBattery *mesSpendBattery = new M_SpendBattery();
      mesSpendBattery->amount = m_energyCost*elapsed;
      getOwner()->ReceiveMessage(mesSpendBattery);
      delete mesSpendBattery;
    }
    else
      Transform(false);
    delete mesGetBattery;
  }
}

void C_BlazerFormWeapon::ReceiveMessage(Message *msg) {
  if (!m_transform)
    C_Weapon::ReceiveMessage(msg);
}

void C_BlazerFormWeapon::Shooting(){
  if (!m_transform){
    C_Weapon::Shooting();

    if (m_canShot){
      Transform(true);
    }
  }
}

void C_BlazerFormWeapon::Transform(bool activated){
  m_transform = activated;
  if (activated){ //change
    M_ChangeImage *mesChangeImage = new M_ChangeImage();
    mesChangeImage->image = m_image;
    getOwner()->ReceiveMessage(mesChangeImage);
    delete mesChangeImage;

    M_ChangeSpeed *mesChangeSpeed = new M_ChangeSpeed();
    mesChangeSpeed->factor = m_factor;
    mesChangeSpeed->force = true;
    getOwner()->ReceiveMessage(mesChangeSpeed);
    delete mesChangeSpeed;

    M_ChangeDamage *mesChangeDamage = new M_ChangeDamage();
    mesChangeDamage->amount = m_power;
    getOwner()->ReceiveMessage(mesChangeDamage);
    delete mesChangeDamage;
  }
  else{ //restore
    M_RestartImage *mesRestartImage = new M_RestartImage();
    getOwner()->ReceiveMessage(mesRestartImage);
    delete mesRestartImage;

    M_ChangeSpeed *mesChangeSpeed = new M_ChangeSpeed();
    mesChangeSpeed->factor = 1/m_factor;
    mesChangeSpeed->force = false;
    getOwner()->ReceiveMessage(mesChangeSpeed);
    delete mesChangeSpeed;

    M_ChangeDamage *mesChangeDamage = new M_ChangeDamage();
    mesChangeDamage->amount = 0;
    getOwner()->ReceiveMessage(mesChangeDamage);
    delete mesChangeDamage;
  }
}