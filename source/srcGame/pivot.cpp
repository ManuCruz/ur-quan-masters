#include "../includeGame/pivot.h"

#include "../include/sprite.h"
#include "../include/math.h"

//PIVOT
C_Pivot::C_Pivot(Entity* owner, PivotParam *param, String fileConfig) : Component(owner){
  m_sprite = NULL;
  m_xCenter = param->xCenter;
  m_yCenter = param->yCenter;
  m_angularSpeed = param->angularSpeed;
  m_distance = param->distance;
  m_currentAngle = param->initialAngle;
}

void C_Pivot::Update(double elapsed){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    m_sprite->SetPosition(m_xCenter + DegCos(m_currentAngle) * m_distance, m_yCenter + DegSin(m_currentAngle) * m_distance);

    m_currentAngle += m_angularSpeed * elapsed;
  }
}

void C_Pivot::ReceiveMessage(Message *msg) {
  M_SetCenterPivot *mesSetCenterPivot = dynamic_cast<M_SetCenterPivot*>(msg);
  if (mesSetCenterPivot){
    m_xCenter = mesSetCenterPivot->xCenter;
    m_yCenter = mesSetCenterPivot->yCenter;
  }
}
