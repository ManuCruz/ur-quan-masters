#include "../includeGame/lineAutoMovement.h"

//LINE AUTO MOVEMENT
C_LineAutoMovement::C_LineAutoMovement(Entity* owner, LineAutoMovementParam *param, String fileConfig) : Component(owner){
}

void C_LineAutoMovement::Update(double elapsed){
  M_Movement *mesMovement = new M_Movement();
  mesMovement->push = true;
  getOwner()->ReceiveMessage(mesMovement);
  delete mesMovement;
}

void C_LineAutoMovement::ReceiveMessage(Message *msg) {
}
