#include "../includeGame/renderable.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/image.h"

//RENDERABLE
C_Renderable::C_Renderable(Entity* owner, RenderableParam *param, String fileConfig) : Component(owner) {
  m_sprite = param->sprite;
  m_sprite->SetFPS(param->fps);
  m_sprite->SetFrameRange(0, param->vframes - 1);
  m_originalImage = m_sprite->GetImage();
}

void C_Renderable::Update(double elapsed) {
}

void C_Renderable::ReceiveMessage(Message *msg) {
  M_GetSprite *mesGetSprite = dynamic_cast<M_GetSprite*>(msg);
  if (mesGetSprite)
    mesGetSprite->sprite = m_sprite;

  M_HasCollision *mesHasCollision = dynamic_cast<M_HasCollision*>(msg);
  if (mesHasCollision){
    mesHasCollision->answer = m_sprite->DidCollide();
    mesHasCollision->sprite = m_sprite;
  }

  M_RestartImage *mesRestartImage = dynamic_cast<M_RestartImage*>(msg);
  if (mesRestartImage){
    ChangeImage(m_originalImage);
  }

  M_ChangeImage *mesChangeImage = dynamic_cast<M_ChangeImage*>(msg);
  if (mesChangeImage){
    ChangeImage(mesChangeImage->image);
  }
}

void C_Renderable::ChangeImage(const Image *image){
  m_sprite->SetImage(image);
  m_sprite->SetRadius((image->GetHeight() + image->GetWidth()) / 4);
}