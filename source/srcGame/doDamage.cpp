#include "../includeGame/doDamage.h"

//DO DAMAGE
C_DoDamage::C_DoDamage(Entity* owner, DoDamageParam *param, String fileConfig) : Component(owner){
  m_damage = param->damage;
  m_owner = param->owner;
}

void C_DoDamage::Update(double elapsed){
}

void C_DoDamage::ReceiveMessage(Message *msg) {
  M_CollisionWith *mesCollisionWith = dynamic_cast<M_CollisionWith*>(msg);
  if (mesCollisionWith){
    if (mesCollisionWith->ent->GetIdOwner() != getOwner()->GetIdOwner() && mesCollisionWith->ent->GetIdOwner() != NO_OWNER_ENTITY){
      M_LoseLife *mesLoseLife = new M_LoseLife();
      mesLoseLife->amount = m_damage;
      mesCollisionWith->ent->ReceiveMessage(mesLoseLife);
      delete mesLoseLife;

      M_IDoDamage *mesIDoDamage = new M_IDoDamage();
      getOwner()->ReceiveMessage(mesIDoDamage);
      delete mesIDoDamage;
    }
  }

  M_ChangeDamage *mesChangeDamage = dynamic_cast<M_ChangeDamage*>(msg);
  if (mesChangeDamage){
    m_damage = mesChangeDamage->amount;
  }
}
