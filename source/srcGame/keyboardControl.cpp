#include "../includeGame/keyboardControl.h"

#define INPUT InputManager::Instance()

//KEYBOARD_CONTROL
C_KeyboardControl::C_KeyboardControl(Entity* owner, KeyboardControlParam *param, String fileConfig) : Component(owner){
  String id = String::FromInt(owner->GetId());
  INPUT.CreateAction("push_" + id, param->push);
  INPUT.CreateAction("right_" + id, param->right);
  INPUT.CreateAction("left_" + id, param->left);
  INPUT.CreateAction("shot1_" + id, param->shot1);
  INPUT.CreateAction("shot2_" + id, param->shot2);
}

void C_KeyboardControl::Update(double elapsed){
  bool push = false;
  bool right = false;
  bool left = false;
  String id = String::FromInt(getOwner()->GetId());

  if (INPUT.IsActionDown("push_" + id) || INPUT.IsActionPressed("push_" + id))
    push = true;
  if (INPUT.IsActionDown("right_" + id) || INPUT.IsActionPressed("right_" + id))
    right = true;
  if (INPUT.IsActionDown("left_" + id) || INPUT.IsActionPressed("left_" + id))
    left = true;

  M_Movement *mesMovement = new M_Movement();
  mesMovement->push = push;
  mesMovement->right = right;
  mesMovement->left = left;
  getOwner()->ReceiveMessage(mesMovement);
  delete mesMovement;

  if (INPUT.IsActionDown("shot1_" + id) || INPUT.IsActionPressed("shot1_" + id)){
    M_Shot *mesShot = new M_Shot();
    mesShot->id = 1;
    getOwner()->ReceiveMessage(mesShot);
    delete mesShot;
  }
  if (INPUT.IsActionDown("shot2_" + id) || INPUT.IsActionPressed("shot2_" + id)){
    M_Shot *mesShot = new M_Shot();
    mesShot->id = 2;
    getOwner()->ReceiveMessage(mesShot);
    delete mesShot;
  }
}

void C_KeyboardControl::ReceiveMessage(Message *msg) {
}