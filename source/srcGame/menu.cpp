#include "../includeGame/menu.h"

Font *Menu::font = NULL;

Menu::~Menu(){
  for (uint32 i = 0; i < controls.Size(); i++){
    controls[i]->destroy();
    delete controls[i];
  }
  controls.Clear();
}