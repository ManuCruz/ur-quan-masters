#include "../includeGame/destroyWhenDoDamage.h"
#include "../includeGame/entityManager.h"

//DESTROY WHEN DO DAMAGE
C_DestroyWhenDoDamage::C_DestroyWhenDoDamage(Entity* owner, DestroyWhenDoDamageParam *param, String fileConfig) : Component(owner){
}

void C_DestroyWhenDoDamage::Update(double elapsed){
}

void C_DestroyWhenDoDamage::ReceiveMessage(Message *msg) {
  M_IDoDamage *mesIDoDamage = dynamic_cast<M_IDoDamage*>(msg);
  if (mesIDoDamage)
    getOwner()->GetEntityManager()->deleteEntity(getOwner());
}