#include "../includeGame/followEntityEmpty.h"

//FOLLOW ENTITY EMPTY
C_FollowEntityEmpty::C_FollowEntityEmpty(Entity* owner, FollowEntityMovementParam *param, String fileConfig) : C_FollowEntityMovement(owner, param, fileConfig){
}

void C_FollowEntityEmpty::Update(double elapsed){
  C_FollowEntityMovement::Update(elapsed);
}

void C_FollowEntityEmpty::ReceiveMessage(Message *msg) {
  C_FollowEntityMovement::ReceiveMessage(msg);
}

void C_FollowEntityEmpty::OnTheTarget(){
}

void C_FollowEntityEmpty::OnTheOrigin(){
}