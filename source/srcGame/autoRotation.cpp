#include "../includeGame/autoRotation.h"

//AUTO ROTATION
C_AutoRotation::C_AutoRotation(Entity* owner, AutoRotationParam *param, String fileConfig) : Component(owner) {
  m_toRight = param->toRight;
}

void C_AutoRotation::Update(double elapsed) {
  M_Movement *mesMovement = new M_Movement();
  if (m_toRight)
    mesMovement->right = true;
  else
    mesMovement->left = true;
  getOwner()->ReceiveMessage(mesMovement);
  delete mesMovement;
}

void C_AutoRotation::ReceiveMessage(Message *msg) {
}
