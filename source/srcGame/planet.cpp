#include "../includeGame/planet.h"
#include "../includeGame/doDamage.h"
#include "../includeGame/entityManager.h"

#include "math.h"

#include "../include/sprite.h"

//PLANET
C_Planet::C_Planet(Entity* owner, PlanetParam *param, String fileConfig) : Component(owner){
  m_sprite = NULL;
  m_force = param->force;
  m_radius2 = pow(param->radius,2);
  m_repulsionforce = param->repulsionforce;

  getOwner()->SetIdOwner(getOwner()->GetId());
  
  //Do damage component
  DoDamageParam *paramDoDamage = new DoDamageParam();
  paramDoDamage->damage = param->damage;
  paramDoDamage->owner = getOwner()->GetId();

  getOwner()->GetEntityManager()->addComponent(getOwner(), DoDamage, paramDoDamage);
  delete paramDoDamage;
}

void C_Planet::Update(double elapsed){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    double currentX = m_sprite->GetX();
    double currentY = m_sprite->GetY();
    uint32 numOfShips = getOwner()->GetEntityManager()->getNOfShips();
    for (uint32 i = 0; i < numOfShips; i++){
      double x = INFINITY;
      double y = INFINITY;

      getOwner()->GetEntityManager()->getInfoShip(i, x, y);

      double dist2 = pow(currentX - x, 2) + pow(currentY - y, 2);

      if (dist2 < m_radius2){
        Entity *ent = getOwner()->GetEntityManager()->getShip(i);
        if (ent){
          M_Gravity *mesGravity = new M_Gravity();
          mesGravity->force = m_force;
          mesGravity->xForce = currentX;
          mesGravity->yForce = currentY;

          ent->ReceiveMessage(mesGravity);

          delete mesGravity;
        }
      }
    }
  }
}

void C_Planet::ReceiveMessage(Message *msg) {
  M_CollisionWith *mesCollisionWith = dynamic_cast<M_CollisionWith*>(msg);
  if (mesCollisionWith){
    if (mesCollisionWith->ent->GetIdOwner() != getOwner()->GetIdOwner() && mesCollisionWith->ent->GetIdOwner() != NO_OWNER_ENTITY){
      if (mesCollisionWith->ent){
        double currentX = m_sprite->GetX();
        double currentY = m_sprite->GetY();

        M_Gravity *mesGravity = new M_Gravity();
        mesGravity->force = -m_repulsionforce;
        mesGravity->xForce = currentX;
        mesGravity->yForce = currentY;

        mesCollisionWith->ent->ReceiveMessage(mesGravity);

        delete mesGravity;
      }
    }
  }
}