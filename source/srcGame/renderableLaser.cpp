#include "../includeGame/renderableLaser.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"

//RENDERABLE LASER
C_RenderableLaser::C_RenderableLaser(Entity* owner, RenderableLaserParam *param, String fileConfig) : Component(owner) {
  m_sprite = param->sprite;

  m_sprite->SetAngle(param->angle);
  double dirX = m_sprite->GetUpX();
  double dirY = m_sprite->GetUpY();
  m_sprite->SetPosition(param->x + dirX * param->parentRadius, param->y + dirY * param->parentRadius);
}

void C_RenderableLaser::Update(double elapsed) {
  getOwner()->GetEntityManager()->deleteEntity(getOwner());
}

void C_RenderableLaser::ReceiveMessage(Message *msg) {
  M_GetSprite *mesGetSprite = dynamic_cast<M_GetSprite*>(msg);
  if (mesGetSprite)
    mesGetSprite->sprite = m_sprite;

  M_HasCollision *mesHasCollision = dynamic_cast<M_HasCollision*>(msg);
  if (mesHasCollision){
    mesHasCollision->answer = m_sprite->DidCollide();
    mesHasCollision->sprite = m_sprite;
  }
}