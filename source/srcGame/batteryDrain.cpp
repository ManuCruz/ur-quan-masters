#include "../includeGame/batteryDrain.h"

//BATTERY DRAIN
C_BatteryDrain::C_BatteryDrain(Entity* owner, BatteryDrainParam *param, String fileConfig) : Component(owner){
  m_amount = param->amount;
  m_owner = param->owner;
}

void C_BatteryDrain::Update(double elapsed){
}

void C_BatteryDrain::ReceiveMessage(Message *msg) {
  M_CollisionWith *mesCollisionWith = dynamic_cast<M_CollisionWith*>(msg);
  if (mesCollisionWith){
    if (mesCollisionWith->ent->GetIdOwner() != getOwner()->GetIdOwner() && mesCollisionWith->ent->GetIdOwner() != NO_OWNER_ENTITY){
      M_SpendBattery *mesSpendBattery = new M_SpendBattery();
      mesSpendBattery->amount = m_amount;
      mesCollisionWith->ent->ReceiveMessage(mesSpendBattery);
      delete mesSpendBattery;
    }
  }
}
