#include "../includeGame/isAffectedByGravity.h"

#include "../include/sprite.h"

//IS AFFECTED BY GRAVITY
C_IsAffectedByGravity::C_IsAffectedByGravity(Entity* owner, IsAffectedByGravityParam *param, String fileConfig) : Component(owner){
  m_sprite = NULL;
  m_mass = param->mass;
  m_repulsionforce = param->repulsionforce;
  m_force = 0;
  m_xForce = 0;
  m_yForce = 0;
  m_apply = false;
}

void C_IsAffectedByGravity::Update(double elapsed){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite && m_apply){
    double momentum = 0;
    if (m_mass != 0) 
      momentum = m_force * elapsed / m_mass;
    double currentX = m_sprite->GetX();
    double currentY = m_sprite->GetY();

    m_sprite->SetX(currentX + (m_xForce - currentX) * momentum);
    m_sprite->SetY(currentY + (m_yForce - currentY) * momentum);

    m_apply = false;
  }
}

void C_IsAffectedByGravity::ReceiveMessage(Message *msg) {
  M_Gravity *mesGravity = dynamic_cast<M_Gravity*>(msg);
  if (mesGravity && !m_apply){
    m_apply = true;
    m_force = mesGravity->force;
    m_xForce = mesGravity->xForce;
    m_yForce = mesGravity->yForce;
  }

  M_CollisionWith *mesCollisionWith = dynamic_cast<M_CollisionWith*>(msg);
  if (mesCollisionWith){
    if (mesCollisionWith->ent->GetIdOwner() != getOwner()->GetIdOwner() && mesCollisionWith->ent->GetIdOwner() != NO_OWNER_ENTITY){
      if (mesCollisionWith->ent){
        double currentX = m_sprite->GetX();
        double currentY = m_sprite->GetY();

        M_Gravity *mesGravity = new M_Gravity();
        mesGravity->force = -m_repulsionforce;
        mesGravity->xForce = currentX;
        mesGravity->yForce = currentY;

        mesCollisionWith->ent->ReceiveMessage(mesGravity);

        delete mesGravity;
      }
    }
  }
}