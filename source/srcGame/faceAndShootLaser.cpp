#include "../includeGame/faceAndShootLaser.h"
#include "../includeGame/renderableLaser.h"
#include "../includeGame/doDamage.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "math.h"

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//LUA
extern "C" {
#include "../lib/includeLua/lauxlib.h"
#include "../lib/includeLua/lualib.h"
}

//FACE AND SHOOT LASER
C_FaceAndShootLaser::C_FaceAndShootLaser(Entity* owner, FaceAndShootLaserParam *param, String fileConfig) : Component(owner){
  m_sprite = NULL;
  m_currentTimeToShot = 0;

  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("FaceAndShootLaser");
    if (node){
      if (node->first_attribute("spriteName"))
        m_spriteName = String(node->first_attribute("spriteName")->value());
      if (node->first_attribute("distance"))
        m_distance = String(node->first_attribute("distance")->value()).ToFloat();
      if (node->first_attribute("damage"))
        m_damage = String(node->first_attribute("damage")->value()).ToFloat();
      if (node->first_attribute("timeToShot"))
        m_timeToShot = String(node->first_attribute("timeToShot")->value()).ToFloat();
    }
  }
}

void C_FaceAndShootLaser::Update(double elapsed){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    double xTarget = INFINITY;
    double yTarget = INFINITY;
    double radiusTarget = 0.;

    getOwner()->GetEntityManager()->getInfoNearestTarget(getOwner()->GetId(), getOwner()->GetIdOwner(), xTarget, yTarget, radiusTarget);
    if (xTarget != INFINITY && yTarget != INFINITY){
      //Face
      lua_State *luaState = luaL_newstate();
      luaL_openlibs(luaState);
      int error = luaL_dofile(luaState, "lua/orientation.lua");

      double angle = 0;
      double sign = 1;
      if (!error){
        //llamar a la funci�n LUA
        lua_getglobal(luaState, "orientation");
        lua_pushnumber(luaState, m_sprite->GetX());
        lua_pushnumber(luaState, m_sprite->GetY());
        lua_pushnumber(luaState, xTarget);
        lua_pushnumber(luaState, yTarget);

        if (!lua_pcall(luaState, 4, 2, 0) != 0){
          //recuperar los valores que LUA puso en la pila
          if (lua_isnumber(luaState, -2) && lua_isnumber(luaState, -1)){
            angle = lua_tonumber(luaState, -2);
            sign = lua_tonumber(luaState, -1);
          }
          lua_pop(luaState, 2);
        }
      }
      lua_close(luaState);

      m_sprite->SetAngle(180 - sign * angle);

      //Shoot Laser
      m_currentTimeToShot += elapsed;

      if (m_currentTimeToShot > m_timeToShot){
        m_currentTimeToShot = 0;
        double dist = pow(m_sprite->GetX() - xTarget, 2) + pow(m_sprite->GetY() - yTarget, 2);
        if (dist <= pow(m_distance + radiusTarget, 2))
          ShootLaser();
      }
    }
  }
}

void C_FaceAndShootLaser::ReceiveMessage(Message *msg) {
}

void C_FaceAndShootLaser::ShootLaser() {
  //Entity
  Entity* ent = getOwner()->GetEntityManager()->addEntity();
  ent->SetIdOwner(getOwner()->GetIdOwner());

  //Renderable laser Component
  RenderableLaserParam *paramRenderableLaser = new RenderableLaserParam();
  paramRenderableLaser->spriteName = m_spriteName;
  paramRenderableLaser->lenght = m_distance;
  paramRenderableLaser->x = m_sprite->GetX();
  paramRenderableLaser->y = m_sprite->GetY();
  paramRenderableLaser->angle = m_sprite->GetAngle();
  paramRenderableLaser->parentRadius = m_sprite->GetRadius();

  getOwner()->GetEntityManager()->addComponent(ent, RenderableLaser, paramRenderableLaser);
  delete paramRenderableLaser;

  //Do damage component
  DoDamageParam *paramDoDamage = new DoDamageParam();
  paramDoDamage->damage = m_damage;
  paramDoDamage->owner = getOwner()->GetId();

  getOwner()->GetEntityManager()->addComponent(ent, DoDamage, paramDoDamage);
  delete paramDoDamage;
}
