#include "../includeGame/entityManager.h"
#include "../includeGame/entity.h"
#include "../includeGame/ecsInclude.h"

#include "../include/resourceManager.h"
#include "../include/image.h"
#include "../include/filemanager.h"
#include "../include/scene.h"
#include "../include/sprite.h"

#define RES ResourceManager::Instance()

#include "../lib/rapidxml.hpp"
using namespace rapidxml;

EntityManager::EntityManager(Scene *scene, String filename){
  m_scene = scene;
  m_needRemove = false;
  m_fileConfig = filename;
  String file = FileManager::Instance().LoadString(filename);

  if (file != ""){
    xml_document<> doc;
    doc.parse<0>(const_cast<char *>(file.ToCString()));

    xml_node<>* nGame = doc.first_node("game");

    xml_node<>* nGeneral = nGame->first_node("general");
    if (nGeneral){
      xml_node<>* nResolution = nGeneral->first_node("resolution");
      if (nResolution){
        if (nResolution->first_attribute("maxWidth"))
          m_maxWidth = String(nResolution->first_attribute("maxWidth")->value()).ToInt();
        else
          m_maxWidth = Screen::Instance().GetWidth();
        if (nResolution->first_attribute("maxHeight"))
          m_maxHeight = String(nResolution->first_attribute("maxHeight")->value()).ToInt();
        else
          m_maxHeight = Screen::Instance().GetHeight();
      }
    }

    xml_node<>* nEntities = nGame->first_node("entities");
    if (nEntities){
      xml_node<>* nEntity = nEntities->first_node("entity");
      while (nEntity){
        m_aEntities.Add(new Entity(this));

        //component Renderable
        xml_node<>* nRenderable = nEntity->first_node("Renderable");
        if (nRenderable){
          RenderableParam *param = new RenderableParam();

          if (nRenderable->first_attribute("sprite"))
            param->spriteName = String(nRenderable->first_attribute("sprite")->value());

          addComponent(m_aEntities.Last(), Renderable, param);
          delete param;
        }

        //component ShipStats
        xml_node<>* nShipStats = nEntity->first_node("ShipStats");
        if (nShipStats){
          ShipStatsParam *param = new ShipStatsParam();

          if (nShipStats->first_attribute("name"))
            param->name = String(nShipStats->first_attribute("name")->value());
          if (nShipStats->first_attribute("maxLife"))
            param->maxLife = String(nShipStats->first_attribute("maxLife")->value()).ToInt();
          if (nShipStats->first_attribute("maxBattery"))
            param->maxBattery = String(nShipStats->first_attribute("maxBattery")->value()).ToInt();
          if (nShipStats->first_attribute("value"))
            param->value = String(nShipStats->first_attribute("value")->value()).ToInt();
          if (nShipStats->first_attribute("regBattery"))
            param->regBattery = String(nShipStats->first_attribute("regBattery")->value()).ToFloat();

          addComponent(m_aEntities.Last(), ShipStats, param);
          delete param;
        }

        //component Movement
        xml_node<>* nShipMovement = nEntity->first_node("Movement");
        if (nShipMovement){
          MovementParam *param = new MovementParam();

          if (nShipMovement->first_attribute("x"))
            param->initialX = String(nShipMovement->first_attribute("x")->value()).ToFloat();
          if (nShipMovement->first_attribute("y"))
            param->initialY = String(nShipMovement->first_attribute("y")->value()).ToFloat();
          if (nShipMovement->first_attribute("acceleration"))
            param->acceleration = String(nShipMovement->first_attribute("acceleration")->value()).ToFloat();
          if (nShipMovement->first_attribute("maxAcc"))
            param->maxAcc = String(nShipMovement->first_attribute("maxAcc")->value()).ToFloat();
          if (nShipMovement->first_attribute("angularSpeed"))
            param->angularSpeed = String(nShipMovement->first_attribute("angularSpeed")->value()).ToFloat();
          if (nShipMovement->first_attribute("angle"))
            param->angle = String(nShipMovement->first_attribute("angle")->value()).ToFloat();
          addComponent(m_aEntities.Last(), Movement, param);
          delete param;
        }

        //component KeyboardControl
        xml_node<>* nKeyboardControl = nEntity->first_node("KeyboardControl");
        if (nKeyboardControl){
          KeyboardControlParam *param = new KeyboardControlParam();

          if (nKeyboardControl->first_attribute("push"))
            param->push = (eInputCode)String(nKeyboardControl->first_attribute("push")->value()).ToInt();
          if (nKeyboardControl->first_attribute("right"))
            param->right = (eInputCode)String(nKeyboardControl->first_attribute("right")->value()).ToInt();
          if (nKeyboardControl->first_attribute("left"))
            param->left = (eInputCode)String(nKeyboardControl->first_attribute("left")->value()).ToInt();
          if (nKeyboardControl->first_attribute("shot1"))
            param->shot1 = (eInputCode)String(nKeyboardControl->first_attribute("shot1")->value()).ToInt();
          if (nKeyboardControl->first_attribute("shot2"))
            param->shot2 = (eInputCode)String(nKeyboardControl->first_attribute("shot2")->value()).ToInt();

          addComponent(m_aEntities.Last(), KeyboardControl, param);
          delete param;
        }

        //components Weapons
        xml_node<>* nWeapon = nEntity->first_node("Weapon");
        while (nWeapon){
          WeaponParam *param = new WeaponParam();

          if (nWeapon->first_attribute("name"))
            param->name = String(nWeapon->first_attribute("name")->value());
          if (nWeapon->first_attribute("id"))
            param->id = String(nWeapon->first_attribute("id")->value()).ToInt();
          if (nWeapon->first_attribute("energy"))
            param->energy = String(nWeapon->first_attribute("energy")->value()).ToInt();
          if (nWeapon->first_attribute("reload"))
            param->reload = String(nWeapon->first_attribute("reload")->value()).ToInt();
          if (nWeapon->first_attribute("power"))
            param->power = String(nWeapon->first_attribute("power")->value()).ToInt();
          if (nWeapon->first_attribute("distance"))
            param->distance = String(nWeapon->first_attribute("distance")->value()).ToInt();
          if (nWeapon->first_attribute("speed"))
            param->speed = String(nWeapon->first_attribute("speed")->value()).ToInt();
          if (nWeapon->first_attribute("angularSpeed"))
            param->angularSpeed = String(nWeapon->first_attribute("angularSpeed")->value()).ToInt();

          addComponent(m_aEntities.Last(), Weapon, param);
          delete param;

          nWeapon = nWeapon->next_sibling("Weapon");
        }

        //component LineAutoMovement
        xml_node<>* nLineAutoMovement = nEntity->first_node("LineAutoMovement");
        if (nLineAutoMovement){
          LineAutoMovementParam *param = new LineAutoMovementParam();
          addComponent(m_aEntities.Last(), LineAutoMovement, param);
          delete param;
        }

        //component CameraController
        xml_node<>* nCameraController = nEntity->first_node("CameraController");
        if (nCameraController){
          CameraControllerParam *param = new CameraControllerParam();
          addComponent(m_aEntities.Last(), CameraController, param);
          delete param;
        }

        //component Planet
        xml_node<>* nPlanet = nEntity->first_node("Planet");
        if (nPlanet){
          PlanetParam *param = new PlanetParam();

          if (nPlanet->first_attribute("force"))
            param->force = String(nPlanet->first_attribute("force")->value()).ToFloat();
          if (nPlanet->first_attribute("radius"))
            param->radius = String(nPlanet->first_attribute("radius")->value()).ToFloat();
          if (nPlanet->first_attribute("damage"))
            param->damage = String(nPlanet->first_attribute("damage")->value()).ToInt();
          if (nPlanet->first_attribute("repulsionforce"))
            param->repulsionforce = String(nPlanet->first_attribute("repulsionforce")->value()).ToFloat();

          addComponent(m_aEntities.Last(), Planet, param);
          delete param;
        }

        //component IsAffectedByGravity
        xml_node<>* nIsAffectedByGravity = nEntity->first_node("IsAffectedByGravity");
        if (nIsAffectedByGravity){
          IsAffectedByGravityParam *param = new IsAffectedByGravityParam();

          if (nIsAffectedByGravity->first_attribute("mass"))
            param->mass = String(nIsAffectedByGravity->first_attribute("mass")->value()).ToFloat();
          if (nIsAffectedByGravity->first_attribute("repulsionforce"))
            param->repulsionforce = String(nIsAffectedByGravity->first_attribute("repulsionforce")->value()).ToFloat();

          addComponent(m_aEntities.Last(), IsAffectedByGravity, param);
          delete param;
        }

        //component ZapSatsLaserTurret
        xml_node<>* nZapSatsLaserTurret = nEntity->first_node("ZapSatsLaserTurret");
        while (nZapSatsLaserTurret){
          ZapSatsLaserTurretParam *param = new ZapSatsLaserTurretParam();

          if (nZapSatsLaserTurret->first_attribute("angle"))
            param->angle = String(nZapSatsLaserTurret->first_attribute("angle")->value()).ToFloat();

          addComponent(m_aEntities.Last(), ZapSatsLaserTurret, param);
          delete param;

          nZapSatsLaserTurret = nZapSatsLaserTurret->next_sibling("ZapSatsLaserTurret");
        }

        //component IA_DreadNaugh
        xml_node<>* nIA_DreadNaugh = nEntity->first_node("IA_DreadNaugh");
        if (nIA_DreadNaugh){
          IADreadNaughParam *param = new IADreadNaughParam();

          if (nIA_DreadNaugh->first_attribute("lifeThresholdToEscape"))
            param->lifeThresholdToEscape = String(nIA_DreadNaugh->first_attribute("lifeThresholdToEscape")->value()).ToInt();
          if (nIA_DreadNaugh->first_attribute("marginToFollowEnemy"))
            param->marginToFollowEnemy = String(nIA_DreadNaugh->first_attribute("marginToFollowEnemy")->value()).ToFloat();
          if (nIA_DreadNaugh->first_attribute("distanceToShoot1"))
            param->distanceToShoot1 = String(nIA_DreadNaugh->first_attribute("distanceToShoot1")->value()).ToFloat();
          if (nIA_DreadNaugh->first_attribute("distanceToShoot2"))
            param->distanceToShoot2 = String(nIA_DreadNaugh->first_attribute("distanceToShoot2")->value()).ToFloat();

          addComponent(m_aEntities.Last(), IA_DreadNaugh, param);
          delete param;
        }

        //component IA_Avatar
        xml_node<>* nIA_Avatar = nEntity->first_node("IA_Avatar");
        if (nIA_Avatar){
          IAAvatarParam *param = new IAAvatarParam();

          if (nIA_Avatar->first_attribute("numberOfZapsToRun"))
            param->numberOfZapsToRun = String(nIA_Avatar->first_attribute("numberOfZapsToRun")->value()).ToInt();
          if (nIA_Avatar->first_attribute("distanceToShoot1"))
            param->distanceToShoot1 = String(nIA_Avatar->first_attribute("distanceToShoot1")->value()).ToFloat();
          if (nIA_Avatar->first_attribute("distanceToShoot2"))
            param->distanceToShoot2 = String(nIA_Avatar->first_attribute("distanceToShoot2")->value()).ToFloat();

          addComponent(m_aEntities.Last(), IA_Avatar, param);
          delete param;
        }

        //next entity
        nEntity = nEntity->next_sibling("entity");
      }
    }
  }
}

EntityManager::~EntityManager(){
  uint32 size = m_aEntities.Size();
  for (uint32 i = 0; i < size; i++)
    delete m_aEntities[i];

  m_aEntities.Clear();
  m_aShips.Clear();
  m_aTargets.Clear();
}

bool EntityManager::isValid() const{
  return m_aEntities.Size() > 0;
}

void EntityManager::update(double elapsed){
  uint32 size = m_aEntities.Size();
  for (uint32 i = 0; i < size; i++)
    m_aEntities[i]->Update(elapsed);

  if (m_needRemove)
    removeInactiveEntities();
}

Entity* EntityManager::addEntity(){
  m_aEntities.Add(new Entity(this));
  return m_aEntities.Last();
}

void EntityManager::addComponent(Entity *entity, Components idComponent, void *param){
  switch (idComponent) {
  case Renderable:{
    RenderableParam *p = (RenderableParam*)param;

    Image *iSpr = RES.LoadImage("data/assets/" + p->spriteName.Split("_")[0] + "/" + p->spriteName + ".png", p->hframes, p->vframes);
    iSpr->SetMidHandle();
    p->sprite = m_scene->CreateSprite(iSpr, m_scene->LAYER_FRONT);
    p->sprite->SetCollision(Sprite::COLLISION_CIRCLE);
    p->sprite->SetRadius((iSpr->GetHeight() + iSpr->GetWidth()) / 4);

    entity->AddComponent(new C_Renderable(entity, p, m_fileConfig));
    }
    break;
  case RenderableLaser:{
    RenderableLaserParam *p = (RenderableLaserParam*)param;

    Image *iSpr = RES.LoadImage("data/assets/" + p->spriteName.Split("_")[0] + "/" + p->spriteName + ".png");
    iSpr->SetHandle(0, iSpr->GetHeight() / 2);
    p->sprite = m_scene->CreateSprite(iSpr, m_scene->LAYER_FRONT);
    p->sprite->SetCollision(Sprite::COLLISION_LINE);
    p->sprite->SetScaleX(p->lenght / iSpr->GetWidth());

    entity->AddComponent(new C_RenderableLaser(entity, p, m_fileConfig));
    }
    break;
  case ShipStats:{
    ShipStatsParam *p = (ShipStatsParam*)param;
    entity->AddComponent(new C_ShipStats(entity, p, m_fileConfig));
    m_aShips.Add(entity);
    m_aTargets.Add(entity);
    }
    break;
  case Movement:{
    MovementParam *p = (MovementParam*)param;
    entity->AddComponent(new C_Movement(entity, p, m_fileConfig));
    }
    break;
  case KeyboardControl:{
    KeyboardControlParam *p = (KeyboardControlParam*)param;
    entity->AddComponent(new C_KeyboardControl(entity, p, m_fileConfig));
    }
    break;
  case DoDamage:{
    DoDamageParam *p = (DoDamageParam*)param;
    entity->AddComponent(new C_DoDamage(entity, p, m_fileConfig));
    }
    break;
  case DoFriendlyDamage:{
    DoFriendlyDamageParam *p = (DoFriendlyDamageParam*)param;
    entity->AddComponent(new C_DoFriendlyDamage(entity, p, m_fileConfig));
    }
    break;
  case DestroyWhenDoDamage:{
    DestroyWhenDoDamageParam *p = (DestroyWhenDoDamageParam*)param;
    entity->AddComponent(new C_DestroyWhenDoDamage(entity, p, m_fileConfig));
    }
    break;
  case LineAutoMovement:{
    LineAutoMovementParam *p = (LineAutoMovementParam*)param;
    entity->AddComponent(new C_LineAutoMovement(entity, p, m_fileConfig));
    }
    break;
  case AutoRotation:{
    AutoRotationParam *p = (AutoRotationParam*)param;
    entity->AddComponent(new C_AutoRotation(entity, p, m_fileConfig));
    }
    break;
  case FollowEntityAutonomousFighter:{
    FollowEntityMovementParam *p = (FollowEntityMovementParam*)param;
    entity->AddComponent(new C_FollowEntityAutonomousFighter(entity, p, m_fileConfig));
    }
    break;
  case Life:{
    LifeParam *p = (LifeParam*)param;
    entity->AddComponent(new C_Life(entity, p, m_fileConfig));
    }
    break;
  case Sound:{
    SoundParam *p = (SoundParam*)param;
    entity->AddComponent(new C_Sound(entity, p, m_fileConfig));
    }
    break;
  case DestroyAfterDistance:{
    DestroyAfterDistanceParam *p = (DestroyAfterDistanceParam*)param;
    entity->AddComponent(new C_DestroyAfterDistance(entity, p, m_fileConfig));
    }
    break;
  case CameraController:{
    CameraControllerParam *p = (CameraControllerParam*)param;
    entity->AddComponent(new C_CameraController(entity, p, m_fileConfig));
    }
    break;
  case Planet:{
    PlanetParam *p = (PlanetParam*)param;
    entity->AddComponent(new C_Planet(entity, p, m_fileConfig));
    }
    break;
  case IsAffectedByGravity:{
    IsAffectedByGravityParam *p = (IsAffectedByGravityParam*)param;
    entity->AddComponent(new C_IsAffectedByGravity(entity, p, m_fileConfig));
    }
    break;
  case ZapSatsLaserTurret:{
    ZapSatsLaserTurretParam *p = (ZapSatsLaserTurretParam*)param;
    entity->AddComponent(new C_ZapSatsLaserTurret(entity, p, m_fileConfig));
    }
    break;
  case Pivot:{
    PivotParam *p = (PivotParam*)param;
    entity->AddComponent(new C_Pivot(entity, p, m_fileConfig));
    }
    break;
  case FaceAndShootLaser:{
    FaceAndShootLaserParam *p = (FaceAndShootLaserParam*)param;
    entity->AddComponent(new C_FaceAndShootLaser(entity, p, m_fileConfig));
    }
    break;
  case FollowEntityEmpty:{
    FollowEntityMovementParam *p = (FollowEntityMovementParam*)param;
    entity->AddComponent(new C_FollowEntityEmpty(entity, p, m_fileConfig));
    }
    break;
  case BatteryDrain:{
    BatteryDrainParam *p = (BatteryDrainParam*)param;
    entity->AddComponent(new C_BatteryDrain(entity, p, m_fileConfig));
    }
    break;
  case ReduceMobility:{
    ReduceMobilityParam *p = (ReduceMobilityParam*)param;
    entity->AddComponent(new C_ReduceMobility(entity, p, m_fileConfig));
    }
    break;
  case RandomRotation:{
    RandomRotationParam *p = (RandomRotationParam*)param;
    entity->AddComponent(new C_RandomRotation(entity, p, m_fileConfig));
    }
    break;


  case Weapon:{
    WeaponParam *p = (WeaponParam*)param;

    if (p->name == WeaponTypes[WeaponTypes::FusionBlaster])
      entity->AddComponent(new C_FusionBlasterWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::AutonomousFighter])
      entity->AddComponent(new C_AutonomousFighterWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::HeavyXrayLaser])
      entity->AddComponent(new C_HeavyXrayLaserWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::TractorBean])
      entity->AddComponent(new C_TractorBeanWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::PhotonCrystalShard])
      entity->AddComponent(new C_PhotonCrystalShardWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::DOGI])
      entity->AddComponent(new C_DOGIWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::MolecularAcidBubbles])
      entity->AddComponent(new C_MolecularAcidBubblesWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::BlazerForm])
      entity->AddComponent(new C_BlazerFormWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::GigaWattLaser])
      entity->AddComponent(new C_GigaWattLaserWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::Limpet])
      entity->AddComponent(new C_LimpetWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::AutoAimingLaser])
      entity->AddComponent(new C_AutoAimingLaserWeapon(entity, p, m_fileConfig));
    else if (p->name == WeaponTypes[WeaponTypes::RandomTeleportation])
      entity->AddComponent(new C_RandomTeleportationWeapon(entity, p, m_fileConfig));
    }
    break;



  case IA_DreadNaugh:{
    IADreadNaughParam *p = (IADreadNaughParam*)param;
    entity->AddComponent(new C_IA_DreadNaugh(entity, p, m_fileConfig));
    }
    break;
  case IA_Avatar:{
    IAAvatarParam *p = (IAAvatarParam*)param;
    entity->AddComponent(new C_IA_Avatar(entity, p, m_fileConfig));
    }
    break;
  case IA_Marker:{
    MarkerParam *p = (MarkerParam*)param;

    Image *iSpr = RES.LoadImage("data/assets/" + p->spriteName.Split("_")[0] + "/" + p->spriteName + ".png");
    iSpr->SetMidHandle();
    p->sprite1 = m_scene->CreateSprite(iSpr, m_scene->LAYER_FRONT);
    p->sprite1->SetCollision(Sprite::COLLISION_CIRCLE);
    p->sprite1->SetRadius((iSpr->GetHeight() + iSpr->GetWidth()) / 4);
    p->sprite2 = m_scene->CreateSprite(iSpr, m_scene->LAYER_FRONT);
    p->sprite2->SetCollision(Sprite::COLLISION_CIRCLE);
    p->sprite2->SetRadius((iSpr->GetHeight() + iSpr->GetWidth()) / 4);

    entity->AddComponent(new C_IA_Marker(entity, p, m_fileConfig));
    }
    break;
  }
}

void EntityManager::deleteEntity(Entity *entity){
  entity->SetInactive();
  m_needRemove = true;
}

void EntityManager::removeInactiveEntities(){
  uint32 sizeShips = m_aShips.Size();
  uint32 sizeTargets = m_aTargets.Size();
  uint32 sizeEntities = m_aEntities.Size();

  for (int32 i = sizeShips - 1; i >= 0; i--)
    if (m_aShips[i])
      if (!m_aShips[i]->GetActive()){
        for (uint32 j = 0; j < sizeEntities; j++){
          if (m_aEntities[j]->GetIdOwner() == m_aShips[i]->GetIdOwner())
            m_aEntities[j]->SetInactive();
        }
        m_aShips[i] = NULL;
      }

  for (int32 i = sizeTargets - 1; i >= 0; i--)
    if (!m_aTargets[i]->GetActive()){
      m_aTargets.RemoveAt(i);
    }
  
  for (int32 i = sizeEntities - 1; i >= 0; i--)
    if (!m_aEntities[i]->GetActive()){
      M_GetSprite *mesGetSprite = new M_GetSprite();
      m_aEntities[i]->ReceiveMessage(mesGetSprite);
      if (mesGetSprite->sprite)
        m_scene->DeleteSprite(mesGetSprite->sprite);
      delete mesGetSprite;

      delete m_aEntities[i];

      m_aEntities.RemoveAt(i);
    }

  m_needRemove = false;
}

uint32 EntityManager::getNOfShips() const{
  return m_aShips.Size();
}

M_Stats EntityManager::getStats(uint32 nOfShip) const{
  M_Stats m;

  if (m_aShips[nOfShip]){
    M_Stats *mesStats = new M_Stats();

    m_aShips[nOfShip]->ReceiveMessage(mesStats);

    m.isShips = mesStats->isShips;
    m.name = mesStats->name;
    m.life = mesStats->life;
    m.battery = mesStats->battery;

    delete mesStats;
  }

  return m;
}

void EntityManager::checkCollisions(){
  uint32 size = m_aEntities.Size();
  for (uint32 i = 0; i < size - 1; i++){
    M_HasCollision * mesHasCollision1 = new M_HasCollision();
    m_aEntities[i]->ReceiveMessage(mesHasCollision1);
    if (mesHasCollision1->answer){
      for (uint32 j = i + 1; j < size; j++){
        M_HasCollision * mesHasCollision2 = new M_HasCollision();
        m_aEntities[j]->ReceiveMessage(mesHasCollision2);
        if (mesHasCollision2->answer){
          if (mesHasCollision1->sprite->CheckCollision(mesHasCollision2->sprite)){ //si han colisionado entre ellas, analizar el posible efecto
            M_CollisionWith *mesCollisionWith1 = new M_CollisionWith();
            M_CollisionWith *mesCollisionWith2 = new M_CollisionWith();

            mesCollisionWith1->ent = m_aEntities[j];
            mesCollisionWith2->ent = m_aEntities[i];

            m_aEntities[i]->ReceiveMessage(mesCollisionWith1);
            m_aEntities[j]->ReceiveMessage(mesCollisionWith2);
            delete mesCollisionWith1;
            delete mesCollisionWith2;
          }
        }
        delete mesHasCollision2;
      }
    }
    delete mesHasCollision1;
  }
}

void EntityManager::setASpriteShips(){
  uint32 sizeAShips = m_aShips.Size();
  uint32 sizeASprite = m_aSpriteShips.Size();
  if (sizeAShips != sizeASprite){
    m_aSpriteShips.Clear();
    for (uint32 i = 0; i < sizeAShips; i++){
      if (m_aShips[i]){
        M_GetSprite *mesGetSprite = new M_GetSprite();
        m_aShips[i]->ReceiveMessage(mesGetSprite);
        m_aSpriteShips.Add(mesGetSprite->sprite);
        delete mesGetSprite;
      }
    }
  }
}

void EntityManager::setASpriteTargets(){
  uint32 sizeATargets = m_aTargets.Size();
  uint32 sizeASprite = m_aSpriteTargets.Size();
  if (sizeATargets != sizeASprite){
    m_aSpriteTargets.Clear();
    for (uint32 i = 0; i < sizeATargets; i++){
      if (m_aTargets[i]){
        M_GetSprite *mesGetSprite = new M_GetSprite();
        m_aTargets[i]->ReceiveMessage(mesGetSprite);
        m_aSpriteTargets.Add(mesGetSprite->sprite);
        delete mesGetSprite;
      }
    }
  }
}

void EntityManager::addTarget(Entity *ent){
  m_aTargets.Add(ent);
}

void EntityManager::getInfoEntity(uint32 idShip, double &x, double &y, double &radius){
  setASpriteShips();

  uint32 sizeAShips = m_aShips.Size();

  for (uint32 i = 0; i < sizeAShips; i++)
    if (m_aShips[i])
      if (m_aShips[i]->GetId() == idShip){
        x = m_aSpriteShips[i]->GetX();
        y = m_aSpriteShips[i]->GetY();
        radius = m_aSpriteShips[i]->GetRadius();
        break;
      }
}

void EntityManager::getInfoNearestEnemy(uint32 idOwnerShip, double &x, double &y, double &radius){
  setASpriteShips();

  uint32 sizeAShips = m_aShips.Size();

  double minDistance2 = INFINITY;
  double xShip = 0.;
  double yShip = 0.;
  for (uint32 i = 0; i < sizeAShips; i++)
    if (m_aShips[i])
      if (m_aShips[i]->GetId() == idOwnerShip){
        xShip = m_aSpriteShips[i]->GetX();
        yShip = m_aSpriteShips[i]->GetY();
      }

  for (uint32 i = 0; i < sizeAShips; i++)
    if (m_aShips[i])
      if (m_aShips[i]->GetId() != idOwnerShip){
        double xEnemy = m_aSpriteShips[i]->GetX();
        double yEnemy = m_aSpriteShips[i]->GetY();

        double distance2 = pow(xShip - xEnemy, 2) + pow(yShip - yEnemy, 2);
        if (distance2 < minDistance2){
          minDistance2 = distance2;
          x = m_aSpriteShips[i]->GetX();
          y = m_aSpriteShips[i]->GetY();
          radius = m_aSpriteShips[i]->GetRadius();
        }
      }
}

void EntityManager::getInfoNearestTarget(uint32 id, uint32 idOwner, double &x, double &y, double &radius){
  setASpriteTargets();

  uint32 sizeATargets = m_aTargets.Size();

  double minDistance2 = INFINITY;
  double myX = 0.;
  double myY = 0.;
  for (uint32 i = 0; i < sizeATargets; i++)
    if (m_aTargets[i])
      if (m_aTargets[i]->GetId() == id){
        myX = m_aSpriteTargets[i]->GetX();
        myY = m_aSpriteTargets[i]->GetY();
      }

  for (uint32 i = 0; i < sizeATargets; i++)
    if (m_aTargets[i])
      if (m_aTargets[i]->GetIdOwner() != idOwner){
        double xEnemy = m_aSpriteTargets[i]->GetX();
        double yEnemy = m_aSpriteTargets[i]->GetY();

        double distance2 = pow(myX - xEnemy, 2) + pow(myY - yEnemy, 2);
        if (distance2 < minDistance2){
          minDistance2 = distance2;
          x = m_aSpriteTargets[i]->GetX();
          y = m_aSpriteTargets[i]->GetY();
          radius = m_aSpriteTargets[i]->GetRadius();
        }
      }
}

void EntityManager::getInfoShip(uint32 indexShip, double &x, double &y){
  setASpriteShips();

  if (m_aShips[indexShip]){
    x = m_aSpriteShips[indexShip]->GetX();
    y = m_aSpriteShips[indexShip]->GetY();
  }
}

Entity* EntityManager::getShip(uint32 indexShip) const{
  if (m_aShips[indexShip])
    return m_aShips[indexShip];
  return NULL;
}

void EntityManager::setPositionCamera(double x, double y){
  m_scene->GetCamera().SetPosition(x, y);
}

void EntityManager::getWorldSize(double &width, double &height){
  width = m_maxWidth;
  height = m_maxHeight;
}

Entity* EntityManager::checkCollision(Sprite *spr, uint32 index, uint32 &nextIndex){
  uint32 size = m_aEntities.Size();
  Entity *ent = NULL;
  nextIndex = 0;
  for (uint32 i = index; i < size; i++){
    M_HasCollision * mesHasCollision1 = new M_HasCollision();
    m_aEntities[i]->ReceiveMessage(mesHasCollision1);
    if (mesHasCollision1->answer)
      if (spr->CheckCollision(mesHasCollision1->sprite)){
        ent = m_aEntities[i];
        nextIndex = i+1;
        break;
      }
    delete mesHasCollision1;
  }

  return ent;
}
