#include "../includeGame/laserWeapon.h"
#include "../includeGame/renderableLaser.h"
#include "../includeGame/doDamage.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//LASER WEAPON
C_LaserWeapon::C_LaserWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig) {
}

void C_LaserWeapon::Update(double elapsed){
  C_Weapon::Update(elapsed);
}

void C_LaserWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_LaserWeapon::Shooting(){
  C_Weapon::Shooting();

  if (m_canShot){
    //Entity
    Entity* ent = getOwner()->GetEntityManager()->addEntity();
    ent->SetIdOwner(getOwner()->GetIdOwner());

    //Renderable laser Component
    RenderableLaserParam *paramRenderableLaser = new RenderableLaserParam();
    paramRenderableLaser->spriteName = m_spriteName;
    paramRenderableLaser->lenght = m_distance;

    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    paramRenderableLaser->x = mesGetSprite->sprite->GetX();
    paramRenderableLaser->y = mesGetSprite->sprite->GetY();
    paramRenderableLaser->angle = mesGetSprite->sprite->GetAngle();
    paramRenderableLaser->parentRadius = mesGetSprite->sprite->GetRadius();
    delete mesGetSprite;

    getOwner()->GetEntityManager()->addComponent(ent, RenderableLaser, paramRenderableLaser);
    delete paramRenderableLaser;

    //Do damage component
    DoDamageParam *paramDoDamage = new DoDamageParam();
    paramDoDamage->damage = m_power;
    paramDoDamage->owner = getOwner()->GetId();

    getOwner()->GetEntityManager()->addComponent(ent, DoDamage, paramDoDamage);
    delete paramDoDamage;
  }
}
