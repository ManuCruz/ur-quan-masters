#include "../includeGame/game.h"
#include "../includeGame/menu.h"

#include "../include/screen.h"
#include "../include/renderer.h"
#include "../include/resourceManager.h"
#include "../include/image.h"
#include "../include/emitter.h"
#include "../include/math.h"
#include "../include/inputManager.h"
#include "../include/filemanager.h"

#define SCR Screen::Instance()
#define REN Renderer::Instance()
#define RES ResourceManager::Instance()
#define INPUT InputManager::Instance()

#include "../lib/rapidxml.hpp"
using namespace rapidxml;

Game* Game::game = NULL;

Game::Game(){
  countDown = 0;
  tempCount = 0.;
  fileConfig = "data/games/config.xml";
  endGame = false;
}

Game::~Game(){
  uint32 cSize = controls.Size();
  for (uint32 i = 0; i < cSize; i++){
    controls[i]->destroy();
    delete controls[i];
  }
  controls.Clear();
  delete eManager;
  delete scene;
}

void Game::init(){
  //delete previous Match
  if (scene){
    delete eManager;
    delete scene;
    controls.Clear();
  }

  //new match  
  String file = FileManager::Instance().LoadString(fileConfig);

  Image *background1 = NULL;
  Image *background2 = NULL;

  if (file != ""){
    xml_document<> doc;
    doc.parse<0>(const_cast<char *>(file.ToCString()));

    xml_node<>* nGame = doc.first_node("game");

    xml_node<>* nGeneral = nGame->first_node("general");
    if (nGeneral){
      xml_node<>* nResolution = nGeneral->first_node("resolution");
      if (nResolution){
        uint32 width = 0;
        uint32 height = 0;
        
        if (nResolution->first_attribute("width"))
          width = String(nResolution->first_attribute("width")->value()).ToInt();
        if (nResolution->first_attribute("height"))
          height = String(nResolution->first_attribute("height")->value()).ToInt();

        SCR.Resize(width, height);
      }
      
      xml_node<>* nBackground = nGeneral->first_node("background");
      if (nBackground){
        if (nBackground->first_attribute("image1")){
          String name = String(nBackground->first_attribute("image1")->value());
          background1 = RES.LoadImage("data/assets/" + name.Split("_")[0] + "/" + name + ".png");
        }
        if (nBackground->first_attribute("image2")){
          String name = String(nBackground->first_attribute("image2")->value());
          background2 = RES.LoadImage("data/assets/" + name.Split("_")[0] + "/" + name + ".png");
        }
      }

      xml_node<>* nCountDown= nGeneral->first_node("countDown");
      if (nCountDown){
        if (nCountDown->first_attribute("value"))
          countDown = String(nCountDown->first_attribute("value")->value()).ToInt();
      }

    }
  }

  scene = new ParallaxScene(background1, background2);
  scene->SetAutoBackSpeed(16., 20);
  scene->SetRelativeBackSpeed(0, 0);
  scene->SetAutoFrontSpeed(28, 24);
  scene->SetRelativeFrontSpeed(0, 0);

  eManager = new EntityManager(scene, fileConfig);
  if (eManager->isValid()){
    //setting
    countTime = 0.;
    isPlaying = true;
    tempCount = 0.;
  }

  controls.Add(new Window());
  ((Window*)controls.Last())->init("Ventana", NULL, NULL);

  controls.Add(new Label());
  ((Label*)controls.Last())->init("l_count", Vector2(-1.f, SCR.GetHeight()/2.f), "", Menu::getFont(), 255, 0, 255, NULL, controls[0]);

  GUI.setRootControl(controls[0]);
}

void Game::run(){
  GUI.setRootControl(controls[0]);
  double elapsed = SCR.ElapsedTime();

  //countDown to start
  if (countDown > tempCount){
    tempCount += elapsed;
    ((Label*)controls[1])->setText(String::FromInt((int32)(countDown - tempCount + 1)));
  }
  else{
    ((Label*)controls[1])->setText("");
    if (isPlaying){
      countTime += elapsed;

      //update entities
      eManager->update(elapsed);

      //update scene
      scene->Update(elapsed);

      //collision
      eManager->checkCollisions();
    }
    else{
      setNextState(NULL);
    }

    //render all
    scene->Render();

    //SCORE
    renderInfo();

    if (INPUT.IsActionUp("exit")){
      setNextState(NULL);
    }
  }
}

void Game::end(){
}

void Game::renderInfo(){
  Font * f = Menu::getFont();
  int32 countPJ = 0;
  uint32 index = 0;
  uint32 numOfControls = controls.Size();
  uint32 numOfShips = eManager->getNOfShips();

  for (uint32 i = 0; i < numOfShips; i++){
    M_Stats mStats = eManager->getStats(i);
    if (mStats.isShips){
      countPJ++;
      index = i;
      float x = 15.f + i * 200;
      float y = 15.f;
      
      if (numOfControls == 2){
        controls.Add(new Label());
        ((Label*)controls.Last())->init("l_name" + String::FromInt(i), Vector2(x, y), mStats.name, f, 255, 0, 255, NULL, controls[0]);
        y += (float)(f->GetSize()*1.5);
        controls.Add(new Label());
        ((Label*)controls.Last())->init("l_life" + String::FromInt(i), Vector2(x, y), LOC("life") + ": " + String::FromInt(mStats.life), f, 255, 0, 255, NULL, controls[0]);
        y += (float)(f->GetSize()*1.5);
        controls.Add(new Label());
        ((Label*)controls.Last())->init("l_battery" + String::FromInt(i), Vector2(x, y), LOC("battery") + ": " + String::FromInt(mStats.battery), f, 255, 0, 255, NULL, controls[0]);
      }
      else{
        ((Label*)controls[2 + 3 * i + 1])->setText(LOC("life") + ": " + String::FromInt(mStats.life));
        ((Label*)controls[2 + 3 * i + 2])->setText(LOC("battery") + ": " + String::FromInt(mStats.battery));
        y += (float)(f->GetSize()*1.5);
      }
    }
  }

  if (!endGame){
    if (countPJ == 1){
      endGame = true;
      M_Stats mStats = eManager->getStats(index);
      if (mStats.isShips){
        controls.Add(new Label());
        ((Label*)controls.Last())->init("l_endMessage", Vector2(-1, SCR.GetHeight() / 2), LOC("victory") + ": " + mStats.name, f, 255, 0, 0, NULL, controls[0]);
      }
    }
    if (countPJ == 0){
      endGame = true;
      controls.Add(new Label());
      ((Label*)controls.Last())->init("l_endMessage", Vector2(-1, SCR.GetHeight() / 2), LOC("defeat"), f, 255, 0, 0, NULL, controls[0]);
    }
  }
}