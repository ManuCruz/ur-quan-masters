#include "../includeGame/gigaWattLaserWeapon.h"

#include "../include/sprite.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//GIGA WATT LASER WEAPON
C_GigaWattLaserWeapon::C_GigaWattLaserWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_LaserWeapon(owner, param, fileConfig) {
  String soundName;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("GigaWattLaserWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_GigaWattLaserWeapon::Update(double elapsed){
  C_LaserWeapon::Update(elapsed);
}

void C_GigaWattLaserWeapon::ReceiveMessage(Message *msg) {
  C_LaserWeapon::ReceiveMessage(msg);
}

void C_GigaWattLaserWeapon::Shooting(){
  C_LaserWeapon::Shooting();
}
