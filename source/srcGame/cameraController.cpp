#include "../includeGame/cameraController.h"
#include "../includeGame/entityManager.h"

#include "../include/screen.h"
#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//CAMERA CONTROLLER
C_CameraController::C_CameraController(Entity* owner, CameraControllerParam *param, String fileConfig) : Component(owner){
  m_firstUpdate = true;

  getOwner()->GetEntityManager()->getWorldSize(m_maxWidth, m_maxHeight);
  m_ratio = (double)(Screen::Instance().GetWidth()) / Screen::Instance().GetHeight();

  m_margin = 100;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("CameraController");
    if (node){
      if (node->first_attribute("margin"))
        m_margin = String(node->first_attribute("margin")->value()).ToFloat();
    }
  }
}

void C_CameraController::Update(double elapsed){
  if (m_firstUpdate){
    m_firstUpdate = false;
  }
  else{
    //get position ships
    uint32 nShips = getOwner()->GetEntityManager()->getNOfShips();
    double minX = INFINITY;
    double minY = INFINITY;
    double maxX = -minX;
    double maxY = -minY;
    uint32 shipsCount = 0;
    for (uint32 i = 0; i < nShips; i++){
      double x = INFINITY;
      double y = INFINITY;

      getOwner()->GetEntityManager()->getInfoShip(i, x, y);

      if (x != INFINITY && y != INFINITY){
        shipsCount++;
        if (x < minX)
          minX = x;
        if (x > maxX)
          maxX = x;
        if (y < minY)
          minY = y;
        if (y > maxY)
          maxY = y;
      }
    }
    if (shipsCount > 1){
      //calculate the zoom
      double w = maxX - minX + m_margin * 2;
      double h = maxY - minY + m_margin * 2;
      double tempRatio = w / h;

      if (tempRatio > m_ratio){ //increase h
        h = w / m_ratio;
      }
      else if (tempRatio < m_ratio){ //increase w
        w = m_ratio * h;
      }

      if (w > m_maxWidth && h > m_maxHeight){
        w = m_maxWidth;
        h = m_maxHeight;
      }

      //zoom
      Screen::Instance().Zoom((uint32)w, (uint32)h);
      //camera SetPosition 
      getOwner()->GetEntityManager()->setPositionCamera(minX - m_margin, minY - m_margin);
    }
    else{
      //zoom
      Screen::Instance().Zoom((uint32)m_maxWidth, (uint32)m_maxHeight);
      //camera SetPosition 
      getOwner()->GetEntityManager()->setPositionCamera(0,0);
    }
  }
}

void C_CameraController::ReceiveMessage(Message *msg) {
}