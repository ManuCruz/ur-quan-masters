#include "../includeGame/zapSatsLaserTurret.h"
#include "../includeGame/renderable.h"
#include "../includeGame/pivot.h"
#include "../includeGame/faceAndShootLaser.h"
#include "../includeGame/life.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//ZAP SATS LASER TURRET
C_ZapSatsLaserTurret::C_ZapSatsLaserTurret(Entity* owner, ZapSatsLaserTurretParam *param, String fileConfig) : Component(owner){
  m_spriteParent = NULL;
  m_angle = param->angle;
  m_zap = NULL;
  m_init = false;
  m_spriteName = "Avatar_ZapSats";
  m_distance = 100;

  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("ZapSatsLaserTurret");
    if (node){
      if (node->first_attribute("spriteName"))
        m_spriteName = String(node->first_attribute("spriteName")->value());
      if (node->first_attribute("vframes"))
        m_vframes = (uint16)String(node->first_attribute("vframes")->value()).ToInt();
      if (node->first_attribute("fps"))
        m_fps = (int16)String(node->first_attribute("fps")->value()).ToInt();
      if (node->first_attribute("distance"))
        m_distance = String(node->first_attribute("distance")->value()).ToFloat();
      if (node->first_attribute("angularSpeed"))
        m_angularSpeed = String(node->first_attribute("angularSpeed")->value()).ToFloat();
      if (node->first_attribute("life"))
        m_life = (uint32)String(node->first_attribute("life")->value()).ToInt();
    }
  }
}

void C_ZapSatsLaserTurret::Update(double elapsed){
  if (!m_spriteParent){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getOwner()->ReceiveMessage(mesGetSprite);
    m_spriteParent = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_spriteParent){
    if (!m_init){
      m_init = true;

      m_zap = getOwner()->GetEntityManager()->addEntity();
      m_zap->SetIdOwner(getOwner()->GetIdOwner());
      getOwner()->GetEntityManager()->addTarget(m_zap);

      //Renderable Component
      RenderableParam *paramRenderable = new RenderableParam();
      paramRenderable->spriteName = m_spriteName;
      paramRenderable->vframes = m_vframes;
      paramRenderable->fps = m_fps;

      getOwner()->GetEntityManager()->addComponent(m_zap, Renderable, paramRenderable);
      delete paramRenderable;

      //Pivot Component
      PivotParam *paramPivot = new PivotParam();
      paramPivot->xCenter = m_spriteParent->GetX();
      paramPivot->yCenter = m_spriteParent->GetY();
      paramPivot->distance = m_distance;
      paramPivot->initialAngle = m_angle;
      paramPivot->angularSpeed = m_angularSpeed;

      getOwner()->GetEntityManager()->addComponent(m_zap, Pivot, paramPivot);
      delete paramPivot;

      //FaceAndShoot Component
      FaceAndShootLaserParam *paramFaceAndShootLaser = new FaceAndShootLaserParam();
      getOwner()->GetEntityManager()->addComponent(m_zap, FaceAndShootLaser, paramFaceAndShootLaser);
      delete paramFaceAndShootLaser;

      //Life component
      LifeParam *paramLife = new LifeParam();
      paramLife->life = m_life;
      getOwner()->GetEntityManager()->addComponent(m_zap, Life, paramLife);
      delete paramLife;
    }
    else {
      //send instruccion to zapSats
      if (m_zap->GetIdOwner() == getOwner()->GetId()){
        M_SetCenterPivot *mesSetCenterPivot = new M_SetCenterPivot();
        mesSetCenterPivot->xCenter = m_spriteParent->GetX();
        mesSetCenterPivot->yCenter = m_spriteParent->GetY();
        m_zap->ReceiveMessage(mesSetCenterPivot);
        delete mesSetCenterPivot;
      }
    }
  }
}

void C_ZapSatsLaserTurret::ReceiveMessage(Message *msg) {
  M_GetActivedZaps *mesGetActivatedZaps = dynamic_cast<M_GetActivedZaps*>(msg);
  if (mesGetActivatedZaps){
    if (m_zap->GetIdOwner() == getOwner()->GetId())
      mesGetActivatedZaps->amount++;
  }
}
