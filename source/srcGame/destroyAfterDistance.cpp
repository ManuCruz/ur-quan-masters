#include "../includeGame/destroyAfterDistance.h"
#include "../includeGame/entityManager.h"

//DESTROY AFTER DISTANCE
C_DestroyAfterDistance::C_DestroyAfterDistance(Entity* owner, DestroyAfterDistanceParam *param, String fileConfig) : Component(owner){
  m_speed = param->speed;
  m_distance = param->distance;
  m_currentDistance = 0;
}

void C_DestroyAfterDistance::Update(double elapsed){
  m_currentDistance += m_speed * elapsed;
  if (m_currentDistance >= m_distance)
    getOwner()->GetEntityManager()->deleteEntity(getOwner());
}

void C_DestroyAfterDistance::ReceiveMessage(Message *msg) {
}