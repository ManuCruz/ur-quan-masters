#include "../includeGame/dogiWeapon.h"
#include "../includeGame/renderable.h"
#include "../includeGame/movement.h"
#include "../includeGame/followEntityMovement.h"
#include "../includeGame/life.h"
#include "../includeGame/isAffectedByGravity.h"
#include "../includeGame/batteryDrain.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;
#include "math.h"

// DOGI WEAPON
C_DOGIWeapon::C_DOGIWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig) {
  m_numOfDOGI = 0;
  String soundName;
  String soundNameEnd;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("DOGIWeapon");
    if (node){
      if (node->first_attribute("spriteShot"))
        m_spriteName = String(node->first_attribute("spriteShot")->value());
      if (node->first_attribute("soundNameStart"))
        soundName = String(node->first_attribute("soundNameStart")->value());
      if (node->first_attribute("soundNameEnd"))
        soundNameEnd = String(node->first_attribute("soundNameEnd")->value());
      if (node->first_attribute("limit"))
        m_limit = String(node->first_attribute("limit")->value()).ToInt();
      if (node->first_attribute("life"))
        m_life = String(node->first_attribute("life")->value()).ToInt();
      if (node->first_attribute("repulsionforce"))
        m_repulsionforce = String(node->first_attribute("repulsionforce")->value()).ToFloat();
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
  m_aSourceEnd = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundNameEnd.Split("_")[0] + "/" + soundNameEnd + ".wav"));
  m_aSourceEnd->SetLooping(false);
}

C_DOGIWeapon::~C_DOGIWeapon(){
  if (m_aSourceEnd)
    delete m_aSourceEnd;
}

void C_DOGIWeapon::Update(double elapsed){
  C_Weapon::Update(elapsed);

  checkDOGI();
}

void C_DOGIWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_DOGIWeapon::Shooting(){
  if (m_aDOGI.Size() < m_limit){
    C_Weapon::Shooting();
    if (m_canShot){
      //Entity
      Entity* ent = getOwner()->GetEntityManager()->addEntity();
      m_aDOGI.Add(ent);
      ent->SetIdOwner(getOwner()->GetIdOwner());
      getOwner()->GetEntityManager()->addTarget(ent);

      //Renderable Component
      RenderableParam *paramRenderable = new RenderableParam();
      paramRenderable->spriteName = m_spriteName;

      getOwner()->GetEntityManager()->addComponent(ent, Renderable, paramRenderable);
      delete paramRenderable;

      //Movement Component
      MovementParam *paramMovement = new MovementParam();

      M_GetSprite *mesGetSprite = new M_GetSprite();
      getOwner()->ReceiveMessage(mesGetSprite);
      paramMovement->initialX = mesGetSprite->sprite->GetX();
      paramMovement->initialY = mesGetSprite->sprite->GetY();
      paramMovement->angle = mesGetSprite->sprite->GetAngle();
      paramMovement->parentRadius = mesGetSprite->sprite->GetRadius();
      delete mesGetSprite;

      paramMovement->acceleration = m_speed;
      paramMovement->maxAcc = m_speed;
      paramMovement->angularSpeed = m_angularSpeed;

      getOwner()->GetEntityManager()->addComponent(ent, Movement, paramMovement);
      delete paramMovement;

      //Follow Entity Movement component
      FollowEntityMovementParam *paramFollowEntityMovement = new FollowEntityMovementParam();
      paramFollowEntityMovement->margin = 0;
      paramFollowEntityMovement->time = INFINITY;
      getOwner()->GetEntityManager()->addComponent(ent, FollowEntityEmpty, paramFollowEntityMovement);
      delete paramFollowEntityMovement;

      //Life component
      LifeParam *paramLife = new LifeParam();
      paramLife->life = m_life;
      getOwner()->GetEntityManager()->addComponent(ent, Life, paramLife);
      delete paramLife;

      //Is affected by gravity component
      IsAffectedByGravityParam *paramIsAffectedByGravity = new IsAffectedByGravityParam();
      paramIsAffectedByGravity->repulsionforce = m_repulsionforce;
      getOwner()->GetEntityManager()->addComponent(ent, IsAffectedByGravity, paramIsAffectedByGravity);
      delete paramIsAffectedByGravity;

      //Battery drain component
      BatteryDrainParam *paramBatteryDrain = new BatteryDrainParam();
      paramBatteryDrain->amount = m_power;
      paramBatteryDrain->owner = getOwner()->GetId();

      getOwner()->GetEntityManager()->addComponent(ent, BatteryDrain, paramBatteryDrain);
      delete paramBatteryDrain;
    }
  }
}


void C_DOGIWeapon::checkDOGI(){
  int32 size = m_aDOGI.Size();
  for (int32 i = size-1; i >= 0; i--){
    if (m_aDOGI[i]){
      if (m_aDOGI[i]->GetIdOwner() != getOwner()->GetId())
        m_aDOGI.RemoveAt(i);
    }
    else
      m_aDOGI.RemoveAt(i);
  }

  if (m_numOfDOGI > size && m_aSourceEnd)
    m_aSourceEnd->Play();

  m_numOfDOGI = size;
}