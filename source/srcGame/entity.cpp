#include "../includeGame/entity.h"
#include "../includeGame/component.h"
#include "../includeGame/messages.h"
#include "../includeGame/entityManager.h"

Entity::Entity(EntityManager *eManager){ 
  m_eManager = eManager;  
  m_id = id_entity;
  id_entity++;
  m_active = true;
}

Entity::~Entity(){
  uint32 size = m_Components.Size();
  for (uint32 i = 0; i < size; i++)
    delete m_Components[i];
}

void Entity::Update(double elapsed) {
  uint32 size = m_Components.Size();
  for (uint32 i = 0; i < size; i++)
    m_Components[i]->Update(elapsed);
}

void Entity::ReceiveMessage(Message *msg){
  uint32 size = m_Components.Size();
  for (uint32 i = 0; i < size; i++)
    m_Components[i]->ReceiveMessage(msg);
}

