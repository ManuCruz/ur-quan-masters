#include "../includeGame/tractorBeanWeapon.h"
#include "../includeGame/entityManager.h"

#include "../include/sprite.h"
#include "math.h"
#include "../include/audioSource.h"
#include "../include/resourceManager.h"
#define RES ResourceManager::Instance()

#include "../include/fileManager.h"
#include "../lib/rapidxml.hpp"
using namespace rapidxml;

//TRACTOR BEAN WEAPON
C_TractorBeanWeapon::C_TractorBeanWeapon(Entity* owner, WeaponParam *param, String fileConfig) : C_Weapon(owner, param, fileConfig) {
  m_spriteParent = NULL;
  m_spriteName = NULL;
  String soundName;
  String file = FileManager::Instance().LoadString(fileConfig);
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>
      (const_cast<char *>
      (file.ToCString()));

    xml_node<>* node = doc.first_node("game")->first_node("entityRepository")->first_node("TractorBeanWeapon");
    if (node){
      if (node->first_attribute("soundName"))
        soundName = String(node->first_attribute("soundName")->value());
    }
  }

  m_aSource = new AudioSource(RES.LoadAudioBuffer("data/assets/" + soundName.Split("_")[0] + "/" + soundName + ".wav"));
  m_aSource->SetLooping(false);
}

void C_TractorBeanWeapon::Update(double elapsed){
  C_Weapon::Update(elapsed);
}

void C_TractorBeanWeapon::ReceiveMessage(Message *msg) {
  C_Weapon::ReceiveMessage(msg);
}

void C_TractorBeanWeapon::Shooting(){
  C_Weapon::Shooting();

  if (m_canShot){
    if (!m_spriteParent){
      M_GetSprite *mesGetSprite = new M_GetSprite();
      getOwner()->ReceiveMessage(mesGetSprite);
      m_spriteParent = mesGetSprite->sprite;
      delete mesGetSprite;
    }
    if (m_spriteParent){
      double currentX = m_spriteParent->GetX();
      double currentY = m_spriteParent->GetY();
      uint32 numOfShips = getOwner()->GetEntityManager()->getNOfShips();
      for (uint32 i = 0; i < numOfShips; i++){
        double x = INFINITY;
        double y = INFINITY;

        getOwner()->GetEntityManager()->getInfoShip(i, x, y);

        Entity *ent = getOwner()->GetEntityManager()->getShip(i);
        if (ent)
          if (ent->GetId() != getOwner()->GetId()){
            M_Gravity *mesGravity = new M_Gravity();
            mesGravity->force = m_power;
            mesGravity->xForce = currentX;
            mesGravity->yForce = currentY;

            ent->ReceiveMessage(mesGravity);

            delete mesGravity;
          }
      }
    }
  }
}
