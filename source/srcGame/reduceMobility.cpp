#include "../includeGame/reduceMobility.h"

//REDUCE MOBILITY
C_ReduceMobility::C_ReduceMobility(Entity* owner, ReduceMobilityParam *param, String fileConfig) : Component(owner){
  m_percentage = param->percentage;
  m_owner = param->owner;
  m_active = true;
}

void C_ReduceMobility::Update(double elapsed){
}

void C_ReduceMobility::ReceiveMessage(Message *msg) {
  if (m_active){
    M_CollisionWith *mesCollisionWith = dynamic_cast<M_CollisionWith*>(msg);
    if (mesCollisionWith){
      if (mesCollisionWith->ent->GetIdOwner() != getOwner()->GetIdOwner() && mesCollisionWith->ent->GetIdOwner() != NO_OWNER_ENTITY){
        m_active = false;

        M_GetMaxAcc *mesGetMaxAcc = new M_GetMaxAcc();
        mesCollisionWith->ent->ReceiveMessage(mesGetMaxAcc);
        double enemyMaxAcc = mesGetMaxAcc->amount;
        delete mesGetMaxAcc;

        enemyMaxAcc *= ((double)(100 - m_percentage)) / 100;

        M_SetMaxAcc *mesSetMaxAcc = new M_SetMaxAcc();
        mesSetMaxAcc->amount = enemyMaxAcc;
        mesCollisionWith->ent->ReceiveMessage(mesSetMaxAcc);
        getOwner()->ReceiveMessage(mesSetMaxAcc);
        delete mesSetMaxAcc;
      }
    }
  }
}
