#include "../includeGame/life.h"
#include "../includeGame/entityManager.h"

//LIFE
C_Life::C_Life(Entity* owner, LifeParam *param, String fileConfig) : Component(owner){
  m_life = param->life;
}

void C_Life::Update(double elapsed){
  if (m_life <= 0)
    getOwner()->GetEntityManager()->deleteEntity(getOwner());
}

void C_Life::ReceiveMessage(Message *msg) {
  M_LoseLife *mesLoseLife = dynamic_cast<M_LoseLife*>(msg);
  if (mesLoseLife)
    m_life -= mesLoseLife->amount;
}