#pragma comment(linker, "/SUBSYSTEM:windows /ENTRY:mainCRTStartup")

#include "include/u-gine.h"
#include "uGui/GuiManager.h"
#include "includeGame/state.h"
#include "includeGame/menu.h"
#include "includeGame/game.h"

#define SCR Screen::Instance()
#define REN Renderer::Instance()
#define RES ResourceManager::Instance()
#define LOCAL LocalizationManager::Instance()
#define GUI GuiManager::Instance()
#define INPUT InputManager::Instance()
#define AUD AudioEngine::Instance()

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void MouseButtonCallback(int button, int action){
  int x, y;
  glfwGetMousePos(&x, &y);

  if (action == GLFW_PRESS)
    GUI.injectInput(MessagePointerButtonDown(button, (float)x, (float)y));
  else if (action == GLFW_RELEASE)
    GUI.injectInput(MessagePointerButtonUp(button, (float)x, (float)y));
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void MousePosCallback(int x, int y){
  GUI.injectInput(MessagePointerMove((float)x, (float)y));
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void main() {
  SCR.Open(0, 0, false);

  GUI.init();
  AUD.Init();

  glfwSetMouseButtonCallback(MouseButtonCallback);
  glfwSetMousePosCallback(MousePosCallback);

  Font *font = RES.LoadFont("data/fonts/font.png");
  Menu::setFont(font);

  LOCAL.ParseLanguage("data/language/en.xml");
  LOCAL.ParseLanguage("data/language/es.xml");

  SCR.SetTitle(LOCAL.GetString("title"));

  State::setCurrentState(NULL);
  State::setNextState(Game::Instance());

  INPUT.Init();

  INPUT.CreateAction("exit", KEY_ESC);

  while (SCR.IsOpened()) {
    REN.Clear();

    if (State::getCurrentState() != State::getNextState()){
      if (State::getCurrentState() != NULL)
        State::getCurrentState()->end();
      if (State::getNextState() == NULL)
        break;
      State::setCurrentState(State::getNextState());
      State::getCurrentState()->init();
    }

    INPUT.Update();

    GUI.update();

    State::getCurrentState()->run();

    GUI.render();

    SCR.Refresh();
  }

  INPUT.End();
  GUI.end();
  RES.FreeResources();
  AUD.Finish();
}

