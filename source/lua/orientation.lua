function orientation(x, y, xTarget, yTarget)
	local w = x - xTarget
	local h = y - yTarget
	local hipo = math.sqrt(w*w + h*h)

	local angle = math.deg(math.acos(w/hipo))
	local sign;
	if (yTarget < y) then
		sign = 1
	else
		sign = -1
	end
	
	return angle, sign
end