#include "Window.h"
#include "GuiManager.h"
#include "../include/Renderer.h"
#include "../include/Image.h"
#include "../include/ResourceManager.h"

Window::Window(){
	m_canvas = NULL;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Window::init( const String name, const Vector2& position, const String& backgroungImage, IEventListener* eventListener, Control* parent){
  m_name = name;
  m_position = position;
  m_canvas = ResourceManager::Instance().LoadImage(backgroungImage);
  m_size = Vector2((float)m_canvas->GetWidth(), (float)m_canvas->GetHeight());

	setEventListener( eventListener );
	setParent( parent );

	return true;
}


//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Window::init( const String name, IEventListener* eventListener, Control* parent ){
  m_name = name;
  m_position = Vector2(0, 0);
  m_size = GuiManager::Instance().getScreenSize();

	setEventListener( eventListener );
	setParent( parent );

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Window::update(){

}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Window::render(){
	if( m_canvas && m_visible )	{
		Vector2 pos = getAbsolutePosition();

		Renderer::Instance().SetBlendMode( Renderer::ALPHA );
		Renderer::Instance().DrawImage( m_canvas, pos.x, pos.y );
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Window::onInputEvent(const MessageGui& message){
	switch( message.type ){
	case mtPointerButtonUp:
		NOTIFY_LISTENERS( onClick( this ) );
    return true;
		break;
	}	
  return false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Window::destroy(){
  m_canvas = NULL;
}