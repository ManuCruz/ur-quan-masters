#ifndef _LABEL_H_
#define _LABEL_H_

#include "Control.h"
#include "../include/string.h"

class Font;

class Label : public Control{
public:
	Label();

  bool init(const String& name, const Vector2& position, const String text, Font* font, uint8 r, uint8 g, uint8 b, IEventListener* eventListener, Control* parent);

	virtual void update();
	virtual void render();
  virtual bool onInputEvent(const MessageGui& message);
	virtual void destroy();

  void setText(const String text);
  void reset(const Vector2& position, const String text);
protected:
	Font* m_font;
  String m_text;
  uint8 m_r, m_g, m_b;
};

#endif // _LABEL_H_