#include "Button.h"
#include "GuiManager.h"
#include "../include/Renderer.h"
#include "../include/Image.h"
#include "../include/ResourceManager.h"

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
Button::Button(){
  m_normalImage = NULL;
  m_pushImage = NULL;
	m_pushed = false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Button::init(const String& name, const Vector2& position, const String& normalImage, const String& pushImage, IEventListener* eventListener, Control* parent){
  m_name = name;
  m_normalImage = ResourceManager::Instance().LoadImage(normalImage);
  m_pushImage = ResourceManager::Instance().LoadImage(pushImage);
  m_size = Vector2((float)m_normalImage->GetWidth(), (float)m_normalImage->GetHeight());

  m_position = position;
  if (m_position.x == -1)
    m_position.x = (float)((parent->getSize().x - m_normalImage->GetWidth()) / 2.);

	setEventListener( eventListener );
	setParent( parent );

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Button::update(){
	if( !m_pointerIsOver )
		m_pushed = false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Button::render(){
	if( m_visible )	{
		Vector2 pos = getAbsolutePosition();

		Renderer::Instance().SetBlendMode( Renderer::ALPHA );
		if( m_pushed )
			Renderer::Instance().DrawImage( m_pushImage, pos.x, pos.y );
    else if (!m_pushed)
			Renderer::Instance().DrawImage( m_normalImage, pos.x, pos.y );
	}
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Button::onInputEvent(const MessageGui& message){
	switch( message.type ){
	case mtPointerButtonDown:
		m_pushed = true;
    return true;
		break;

	case mtPointerButtonUp:
		if( m_pushed )
			NOTIFY_LISTENERS( onClick( this ) );
		m_pushed = false;
    return true;
		break;
	}
  return false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Button::destroy(){
  m_normalImage = NULL;
  m_pushImage = NULL;
}