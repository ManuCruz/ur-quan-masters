#include "Slider.h"
#include "GuiManager.h"
#include "Button.h"
#include "SliderButton.h"
#include "../include/Renderer.h"
#include "../include/Image.h"
#include "../include/ResourceManager.h"

class SliderListener : public IEventListener{
private:
  Slider* m_owner;

public:
  void onClick(Control* sender)	{
    if (sender->getName() == (m_owner->getName() + "b_left")){
      m_owner->decreaseValue();
      m_owner->setPositionBullet();
    }
    else if (sender->getName() == (m_owner->getName() + "b_right")){
      m_owner->increaseValue();
      m_owner->setPositionBullet();
    }
  }

  void setOwner(Slider* owner) { m_owner = owner; }
};

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
Slider::Slider(){
  m_barImage = NULL;
  m_bulletImage = NULL;
  m_sbBullet = NULL;
  m_bLeft = m_bRight = NULL;
  sListener = NULL;

  m_pushedBar = false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Slider::init(const String name, const Vector2& position, const String& barImage, const String& bulletImage, float initialValue, float minValue, float maxValue, float step, IEventListener* eventListener, Control* parent, bool arrow, const String& LNImage, const String& LPImage, const String& RNImage, const String& RPImage){
  m_name = name;
  m_barImage = ResourceManager::Instance().LoadImage(barImage);
  m_bulletImage = ResourceManager::Instance().LoadImage(bulletImage);
  m_value = initialValue;
  m_minValue = minValue;
  m_maxValue = maxValue;
  m_step = step;

  m_size = Vector2((float)m_barImage->GetWidth(), (float)m_barImage->GetHeight());

  m_position = position;
  if (m_position.x == -1)
    m_position.x = (float)((parent->getSize().x - m_barImage->GetWidth()) / 2.);

	setEventListener( eventListener );
	setParent( parent );

  sListener = new SliderListener();
  sListener->setOwner(this);


  float offsetXBullet = getOffsetX(m_value);

  m_sbBullet = new SliderButton();
  m_sbBullet->init(name + "sb_bullet", Vector2(offsetXBullet, (float)(-m_bulletImage->GetHeight() / 2)), bulletImage, sListener, this);


  m_bLeft = new Button();
  m_bRight = new Button();

  if (arrow){
    Image* leftImage = ResourceManager::Instance().LoadImage("data/GUI/Slider_Left_Normal.png");
    Image* rightImage = ResourceManager::Instance().LoadImage("data/GUI/Slider_Right_Normal.png");
    m_bLeft->init(name + "b_left", Vector2((float)(-leftImage->GetWidth() - 10), (float)(-leftImage->GetHeight() / 2.)), "data/GUI/Slider_Left_Normal.png", "data/GUI/Slider_Left_Push.png", sListener, this);
    m_bRight->init(name + "b_right", Vector2((float)(m_size.x + 10), (float)(-rightImage->GetHeight() / 2.)), "data/GUI/Slider_Right_Normal.png", "data/GUI/Slider_Right_Push.png", sListener, this);
  }
  else if (LNImage != "" && RNImage != ""){
    Image* leftImage = ResourceManager::Instance().LoadImage(LNImage);
    Image* rightImage = ResourceManager::Instance().LoadImage(RNImage);
    m_bLeft->init(name + "b_left", Vector2((float)(-leftImage->GetWidth() - 10), (float)(-leftImage->GetHeight() / 2.)), LNImage, LPImage, sListener, this);
    m_bRight->init(name + "b_right", Vector2((float)(m_size.x + 10), (float)(-rightImage->GetHeight() / 2.)), RNImage, RPImage, sListener, this);
  }

	return true;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Slider::update(){
  if (!m_pointerIsOver)
    m_pushedBar = false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Slider::render(){
	if( m_visible )	{
		Vector2 pos = getAbsolutePosition();

		Renderer::Instance().SetBlendMode( Renderer::ALPHA );

    Renderer::Instance().DrawImage(m_barImage, pos.x, pos.y);



//    Renderer::Instance().DrawImage(m_bulletImage, pos.x + offsetXBullet, pos.y - m_bulletImage->GetHeight() / 2);
    

	}
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
bool Slider::onInputEvent(const MessageGui& message){
	switch( message.type ){
	case mtPointerButtonDown:
    m_pushedBar = true;
    return true;
		break;
	case mtPointerButtonUp:
		if( m_pointerIsOver && m_pushedBar ){
      const MessagePointerButtonUp* messagePointer = static_cast<const MessagePointerButtonUp*>(&message);
      Vector2 pos = getAbsolutePosition();
      float offsetXBullet = getOffsetX(m_value);;

      Vector2 posCenterBullet;
      posCenterBullet.x = pos.x + offsetXBullet + m_bulletImage->GetWidth() / 2;
      posCenterBullet.y = pos.y;

      if (messagePointer->x < posCenterBullet.x){
        m_pushedBar = false;
        decreaseValue();
        setPositionBullet();
      }
      else if (messagePointer->x > posCenterBullet.x){
        m_pushedBar = false;
        increaseValue();
        setPositionBullet();
      }
    }
    return true;
    break;
	}

  return false;
}

//------------------------------------------------------------------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------------------------------------------------------------------
void Slider::destroy(){
  m_barImage = NULL;
  m_bulletImage = NULL;
  m_sbBullet->destroy();
  m_sbBullet = NULL;
  m_bLeft->destroy();
  m_bRight->destroy();
  m_bLeft = m_bRight = NULL;
  delete sListener;
  sListener = NULL;
}

void Slider::increaseValue(){
  m_value += m_step;
  if (m_value > m_maxValue)
    m_value = m_maxValue;
}

void Slider::decreaseValue(){
  m_value -= m_step;
  if (m_value < m_minValue)
    m_value = m_minValue;
}

void Slider::setPositionBullet(){
  Vector2 pos;
  pos.x = getOffsetX(m_value);
  pos.y = -m_bulletImage->GetHeight() / 2.f;
  m_sbBullet->setPosition(pos);
}

float Slider::getOffsetX(float value){
  return ((value - m_minValue)*m_barImage->GetWidth()) / (m_maxValue - m_minValue) - m_bulletImage->GetWidth() / 2;
}