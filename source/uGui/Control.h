#ifndef _CONTROL_H_
#define _CONTROL_H_

#include "Vector2.h"
#include "IEventListener.h"
#include "../include/String.h"

using namespace std;

class Control{
  friend class GuiManager;

public:
	Control();
	virtual ~Control(){};

	// A implementar en clases derivadas
	virtual void update() = 0;
	virtual void render() = 0;
  virtual bool onInputEvent(const MessageGui& message) = 0;
	virtual void destroy() = 0;

	void setEventListener( IEventListener* eventListener );

	String getName() const;
	virtual Vector2 getAbsolutePosition() const;

	void setVisible( bool show );
	bool isVisible() const;

	void setEnabled( bool enable );
	bool isEnabled() const;

	void setPosition( const Vector2& position );
	Vector2 getPosition() const;

	Vector2 getSize() const;

	void setFocus( bool focus );
	bool hasFocus() const;

	void setParent( Control* parent );
  Control* getParent() const;

	bool isPointInside( const Vector2& point ) const;

protected:
	void processUpdate();
	void processRender();
	void processDestroy();
  bool injectInput(const MessageGui& message);

	void addControl( Control* control );
	void removeControl( Control* control );
	Control* findControlByName( const String& name );

	// Variables
  String m_name;
  Vector2 m_position;
  Vector2 m_size;
  bool m_enabled;
  bool m_visible;
  bool m_focus;
  bool m_pointerIsOver;
  Array<Control*> m_children;
  Control*	m_parent;
  Array<IEventListener*> m_eventListeners;

  static int s_id;
};

// Macro para facilitar la llamada a los listeners
#define NOTIFY_LISTENERS( callback ) \
{ \
  for(uint32 i = 0; i < m_eventListeners.Size(); i++) \
{ \
	m_eventListeners[i]->callback; \
} \
}

#endif // _CONTROL_H_