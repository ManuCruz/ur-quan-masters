#ifndef _SLIDER_BUTTON_H_
#define _SLIDER_BUTTON_H_

#include "Control.h"
#include "../include/string.h"

class Image;

class SliderButton : public Control {
public:
  SliderButton();

  bool init(const String& name, const Vector2& position, const String& normalImage, IEventListener* eventListener, Control* parent );

	virtual void update();
	virtual void render();
  virtual bool onInputEvent(const MessageGui& message);
	virtual void destroy();

protected:
	Image* m_normalImage;
	bool m_pushed;
};

#endif // _BUTTON_H_