#ifndef _BUTTON_H_
#define _BUTTON_H_

#include "Control.h"
#include "../include/string.h"

class Image;

class Button : public Control {
public:
	Button();

  bool init(const String& name, const Vector2& position, const String& normalImage, const String& pushImage, IEventListener* eventListener, Control* parent );

	virtual void update();
	virtual void render();
  virtual bool onInputEvent(const MessageGui& message);
	virtual void destroy();

protected:
	Image* m_normalImage;
	Image* m_pushImage;
	bool m_pushed;
};

#endif // _BUTTON_H_