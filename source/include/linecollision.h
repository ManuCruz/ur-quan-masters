#ifndef UGINE_LINECOLLISION_H
#define UGINE_LINECOLLISION_H

#include "collision.h"
#include "collisionmanager.h"

class LineCollision : public Collision {
public:
  LineCollision(double* x0, double* y0, double* angle, double* width, double*height) : x0(x0), y0(y0), angle(angle), width(width), height(height) {}
  virtual ~LineCollision() {}

  virtual bool DoesCollide(const Collision* other) const { return other->DoesCollide(*x0, *y0, *angle, *width, *height); }
  virtual bool DoesCollide(double cx, double cy, double cradius) const { return CollisionManager::Instance().CircleToLine(cx, cy, cradius, *x0, *y0, *angle, *width, *height); }
  virtual bool DoesCollide(double rx, double ry, double rwidth, double rheight) const { return CollisionManager::Instance().RectToLine(rx, ry, rwidth, rheight, *x0, *y0, *angle, *width, *height); }
  virtual bool DoesCollide(const CollisionPixelData* pixels, double px, double py) const { return CollisionManager::Instance().PixelsToLine(pixels, px, py, *x0, *y0, *angle, *width, *height); }
  virtual bool DoesCollide(double x02, double y02, double angle2, double width2, double height2) const { return CollisionManager::Instance().LineToLine(x02, y02, angle2, width2, height2, *x0, *y0, *angle, *width, *height); }


private:
  double* x0;
  double* y0;
  double* angle;
  double* width;
  double* height;
};

#endif
