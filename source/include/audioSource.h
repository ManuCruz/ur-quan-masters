#ifndef UGINE_AUDIO_SOURCE_H
#define UGINE_AUDIO_SOURCE_H

#include "types.h"
#include "string.h"

class AudioBuffer;
class AudioStream;

class AudioSource {
public:
  AudioSource(AudioBuffer* buffer);
  AudioSource(const String& filename);
  ~AudioSource();

  void SetPitch(float pitch);
  void SetGain(float gain);
  void SetLooping(bool loop);
  void SetPosition(float x, float y, float z);
  void SetVelocity(float x, float y, float z);

  void Play();
  void Stop();
  void Pause();
  bool IsPlaying() const;

  uint32 getSource() { return source; }
private:
  uint32 source;
  AudioBuffer* buffer;
  AudioStream* stream;
};

#endif
