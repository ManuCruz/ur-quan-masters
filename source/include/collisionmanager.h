#ifndef UGINE_COLLISIONMANAGER_H
#define UGINE_COLLISIONMANAGER_H

class CollisionPixelData;

class CollisionManager {
public:
  static const CollisionManager& Instance() { if (!manager) manager = new CollisionManager(); return *manager; }
  virtual bool CircleToCircle(double x1, double y1, double r1, double x2, double y2, double r2) const;
  virtual bool CircleToPixels(double cx, double cy, double cr, const CollisionPixelData* pixels, double px, double py) const;
  virtual bool CircleToRect(double cx, double cy, double cr, double rx, double ry, double rw, double rh) const;
  virtual bool PixelsToPixels(const CollisionPixelData* p1, double x1, double y1, const CollisionPixelData* p2, double x2, double y2) const;
  virtual bool PixelsToRect(const CollisionPixelData* pixels, double px, double py, double rx, double ry, double rw, double rh) const;
  virtual bool RectToRect(double x1, double y1, double w1, double h1, double x2, double y2, double w2, double h2) const;

  virtual bool CircleToLine(double cx, double cy, double cr, double x0, double y0, double angle, double width, double height) const;
  virtual bool RectToLine(double rx, double ry, double rw, double rh, double x0, double y0, double angle, double width, double height) const;
  virtual bool PixelsToLine(const CollisionPixelData* pixels, double px, double py, double x0, double y0, double angle, double width, double height) const;
  virtual bool LineToLine(double x0, double y0, double angle, double width, double height, double x02, double y02, double angle2, double width2, double height2) const;

protected:
  CollisionManager() {}
  virtual ~CollisionManager() {}

private:
  static CollisionManager* manager;
};


#endif
