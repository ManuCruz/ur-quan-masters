/*
base64.cpp and base64.h
Copyright (C) 2004-2008 Ren� Nyffenegger

Adapted by Manuel Cruz Ramirez in 2014
*/

#ifndef UGINE_BASE64_H
#define UGINE_BASE64_H

#include "string.h"
#include "types.h"

static inline String base64_encode(uint8 const*, uint32 len);
static inline String base64_decode(String const& s);

static const String B64_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

static inline bool is_base64(uint8 c) {
  return (isalnum(c) || (c == '+') || (c == '/'));
}

static String base64_encode(uint8 const* bytes_to_encode, uint32 in_len) {
  String ret;
  int32 i = 0;
  int32 j = 0;
  uint8 char_array_3[3];
  uint8 char_array_4[4];

  while (in_len--) {
    char_array_3[i++] = *(bytes_to_encode++);
    if (i == 3) {
      char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
      char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
      char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
      char_array_4[3] = char_array_3[2] & 0x3f;

      for (i = 0; (i <4); i++)
        ret += B64_CHARS[char_array_4[i]];
      i = 0;
    }
  }

  if (i) {
    for (j = i; j < 3; j++)
      char_array_3[j] = '\0';

    char_array_4[0] = (char_array_3[0] & 0xfc) >> 2;
    char_array_4[1] = ((char_array_3[0] & 0x03) << 4) + ((char_array_3[1] & 0xf0) >> 4);
    char_array_4[2] = ((char_array_3[1] & 0x0f) << 2) + ((char_array_3[2] & 0xc0) >> 6);
    char_array_4[3] = char_array_3[2] & 0x3f;

    for (j = 0; (j < i + 1); j++)
      ret += B64_CHARS[char_array_4[j]];

    while ((i++ < 3))
      ret += '=';
  }

  return ret;
}

static String base64_decode(String const& encoded_string) {
  int32 in_len = encoded_string.Length();
  int32 i = 0;
  int32 j = 0;
  int32 in_ = 0;
  uint8 char_array_4[4], char_array_3[3];
  String ret;

  while (in_len-- && (encoded_string[in_] != '=') && is_base64(encoded_string[in_])) {
    char_array_4[i++] = encoded_string[in_]; in_++;
    if (i == 4) {
      for (i = 0; i < 4; i++)
        char_array_4[i] = static_cast<uint8>(B64_CHARS.Find(String(1, char_array_4[i]), 0));

      char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
      char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
      char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

      for (i = 0; (i < 3); i++)
        ret += String::HexFromInt(char_array_3[i]);

      i = 0;
    }
  }

  if (i) {
    for (j = i; j <4; j++)
      char_array_4[j] = 0;

    for (j = 0; j <4; j++)
      char_array_4[j] = static_cast<uint8>(B64_CHARS.Find(String(1, char_array_4[j]), 0));

    char_array_3[0] = (char_array_4[0] << 2) + ((char_array_4[1] & 0x30) >> 4);
    char_array_3[1] = ((char_array_4[1] & 0xf) << 4) + ((char_array_4[2] & 0x3c) >> 2);
    char_array_3[2] = ((char_array_4[2] & 0x3) << 6) + char_array_4[3];

    for (j = 0; (j < i - 1); j++) 
      ret += String::HexFromInt(char_array_3[j]);
  }

  return ret;
}

#endif