#ifndef UGINE_AUDIO_BUFFER_H
#define UGINE_AUDIO_BUFFER_H

#include "types.h"
#include "string.h"

class AudioBuffer {
public:
  AudioBuffer(const String& filename);
  ~AudioBuffer();

  bool IsValid() const { return alBuffer != 0; }
  const String& GetFilename() const { return filename; }
  uint32 GetBuffer() const { return alBuffer; }

private:
  String filename;
  uint32 alBuffer;

  void LoadWav(const String& filename);
  void LoadOgg(const String& filename);
};

#endif
