#ifndef _PLANET_H
#define _PLANET_H

#include "component.h"

class Sprite;

struct PlanetParam{
  double force = 0;
  double radius = 0;
  uint32 damage = 0;
  double repulsionforce = 0;
};

class C_Planet : public Component{
  Sprite *m_sprite;
  double m_force;
  double m_radius2;
  double m_repulsionforce;
public:
  C_Planet(Entity* owner, PlanetParam *param, String fileConfig);
  virtual ~C_Planet(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif