#ifndef _FOLLOW_ENTITY_MOVEMENT_H
#define _FOLLOW_ENTITY_MOVEMENT_H

#include "component.h"

class Sprite;

struct FollowEntityMovementParam{
  double margin = 0;
  double time = 0;
};

class C_FollowEntityMovement : public Component{
  const Sprite *m_sprite;
  double m_margin;
  double m_followTime, m_currentTime;

  virtual void OnTheTarget() = 0;
  virtual void OnTheOrigin() = 0;
public:
  C_FollowEntityMovement(Entity* owner, FollowEntityMovementParam *param, String fileConfig);
  virtual ~C_FollowEntityMovement(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif