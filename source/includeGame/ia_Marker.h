#ifndef _IA_MARKER_H
#define _IA_MARKER_H

#include "component.h"

class Sprite;

struct MarkerParam{
  String spriteName = "";
  Sprite *sprite1 = NULL;
  Sprite *sprite2 = NULL;
  double distance = 0;
  double angle = 0;
};

class C_IA_Marker : public Component{
  String m_spriteName;
  Sprite *m_spriteOwner;
  Sprite *m_sprite1, *m_sprite2;
  double m_distance;
  double m_angle;
public:
  C_IA_Marker(Entity* owner, MarkerParam *param, String fileConfig);
  virtual ~C_IA_Marker(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif