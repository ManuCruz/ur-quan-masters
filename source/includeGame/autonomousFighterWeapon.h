#ifndef _AUTONOMOUS_FIGHTER_WEAPON_H
#define _AUTONOMOUS_FIGHTER_WEAPON_H

#include "weapon.h"

class C_AutonomousFighterWeapon : public C_Weapon{
  virtual void Shooting();
  double m_margin, m_time;
  double m_outputAngle;
  uint32 m_life;
public:
  C_AutonomousFighterWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_AutonomousFighterWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif