#ifndef _HEAVY_XRAY_LASER_WEAPON_H
#define _HEAVY_XRAY_LASER_WEAPON_H

#include "laserWeapon.h"

class C_HeavyXrayLaserWeapon : public C_LaserWeapon{
  virtual void Shooting();
public:
  C_HeavyXrayLaserWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_HeavyXrayLaserWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif