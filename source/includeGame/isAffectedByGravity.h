#ifndef _IS_AFFECTED_BY_GRAVITY_H
#define _IS_AFFECTED_BY_GRAVITY_H

#include "component.h"

class Sprite;

struct IsAffectedByGravityParam{
  double mass = 0;
  double repulsionforce = 0;
};

class C_IsAffectedByGravity : public Component{
  Sprite *m_sprite;
  double m_mass;
  double m_repulsionforce;
  double m_force;
  double m_xForce, m_yForce;
  bool m_apply;
public:
  C_IsAffectedByGravity(Entity* owner, IsAffectedByGravityParam *param, String fileConfig);
  virtual ~C_IsAffectedByGravity(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif