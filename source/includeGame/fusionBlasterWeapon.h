#ifndef _FUSION_BLASTER_WEAPON_H
#define _FUSION_BLASTER_WEAPON_H

#include "weapon.h"

class C_FusionBlasterWeapon : public C_Weapon{
  virtual void Shooting();
public:
  C_FusionBlasterWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_FusionBlasterWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif