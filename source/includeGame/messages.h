#ifndef _MESSAGES_H
#define _MESSAGES_H

#include "../include/array.h"
#include "../include/types.h"
#include "../include/string.h"

class Sprite;
class Entity;
class Image;

//MESSAGES-------------------------
class Message{
public:
  virtual ~Message() {};
};

class M_Stats : public Message{
public:
  bool isShips = false;
  String name = "";
  uint32 life = 0;
  double battery = 0;
};

class M_Movement : public Message{
public:
  bool push = false;
  bool right = false;
  bool left = false;
};

class M_LoseLife : public Message{
public:
  uint32 amount = 0;
};

class M_GetLife : public Message{
public:
  uint32 amount = 0;
};

class M_SpendBattery : public Message{
public:
  double amount = 0;
};

class M_GetBattery : public Message{
public:
  double amount = 0;
};

class M_Shot : public Message{
public:
  uint32 id = 0;
};

class M_GetSprite : public Message{
public:
  Sprite *sprite = NULL;
};

class M_HasCollision : public Message{
public:
  bool answer = false;
  Sprite *sprite = NULL;
};

class M_CollisionWith : public Message{
public:
  Entity *ent = NULL;
};

class M_IDoDamage : public Message{
};

class M_Gravity : public Message{
public:
  double force = 0;
  double xForce = 0;
  double yForce = 0;
};

class M_SetCenterPivot : public Message{
public:
  double xCenter = 0;
  double yCenter = 0;
};

class M_GetActivedZaps : public Message{
public:
  uint32 amount = 0;
};

class M_GetMaxAcc : public Message{
public:
  double amount = 0;
};

class M_SetMaxAcc : public Message{
public:
  double amount = 0;
};

class M_RestartImage : public Message{
};

class M_ChangeImage : public Message{
public:
  Image *image = NULL;
};

class M_ChangeSpeed : public Message{
public:
  double factor = 1;
  double force = false;
};

class M_ChangeDamage : public Message{
public:
  double amount = 0;
};
#endif