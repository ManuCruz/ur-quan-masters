#ifndef _RENDERABLE_LASER_H
#define _RENDERABLE_LASER_H

#include "component.h"

class Sprite;

struct RenderableLaserParam{
  String spriteName = "";
  Sprite *sprite = NULL;
  uint32 lenght = 0;
  double x = 0;
  double y = 0;
  double angle = 0;
  double parentRadius = 0;
};

class C_RenderableLaser : public Component{
  Sprite *m_sprite;
public:
  C_RenderableLaser(Entity* owner, RenderableLaserParam *param, String fileConfig);
  virtual ~C_RenderableLaser(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif