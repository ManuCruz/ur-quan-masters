#ifndef _GIGA_WATT_LASER_WEAPON_H
#define _GIGA_WATT_LASER_WEAPON_H

#include "laserWeapon.h"

class C_GigaWattLaserWeapon : public C_LaserWeapon{
  virtual void Shooting();
public:
  C_GigaWattLaserWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_GigaWattLaserWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif