#ifndef _GAME_H_
#define _GAME_H_

#include "state.h"
#include "../include/types.h"
#include "../include/array.h"
#include "../include/parallaxScene.h"

#include "../lib/glfw.h"

#include "../include/screen.h"
#define SCR Screen::Instance()

#include "../include/localizationmanager.h"
#define LOC(s) LocalizationManager::Instance().GetString(s)

#include "entityManager.h"

#include "../uGui/GuiManager.h"
#define GUI GuiManager::Instance()

class Game : public State {
public:
  static Game* Instance() { if (!game) game = new Game(); return game; }

  virtual void init();
  virtual void run();
  virtual void end();

  Scene* getScene() { return scene; }
  void renderInfo();

  int32 getCountDown() { return countDown; }
  void setCountDown(int32 CD) { countDown = CD; }

  void setFileConfig(String file) { fileConfig = file; }

  Control* getRootControl() { return controls[0]; };

private:
  Game();
  ~Game();

  static Game* game;

  ParallaxScene *scene;
  EntityManager * eManager;
  double countTime;
  bool isPlaying;
  int32 countDown;
  double tempCount;

  String fileConfig;
  Array<Control*> controls;

  bool endGame;
};

#endif
