#ifndef _FOLLOW_ENTITY_AUTONOMOUS_FIGHTER_H
#define _FOLLOW_ENTITY_AUTONOMOUS_FIGHTER_H

#include "followEntityMovement.h"

class C_FollowEntityAutonomousFighter : public C_FollowEntityMovement{
  bool m_shot;
  uint32 m_damage, m_distance, m_speed;
  double m_timeToShot, m_currentTimeToShot;
  String m_spriteName;

  virtual void OnTheTarget();
  virtual void OnTheOrigin();
public:
  C_FollowEntityAutonomousFighter(Entity* owner, FollowEntityMovementParam *param, String fileConfig);
  virtual ~C_FollowEntityAutonomousFighter(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif