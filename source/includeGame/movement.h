#ifndef _MOVEMENT_H
#define _MOVEMENT_H

#include "component.h"

class Sprite;

struct MovementParam{
  double initialX = 0;
  double initialY = 0;
  double acceleration = 0;
  double maxAcc = 0;
  double angularSpeed = 0;
  double angle = 0;
  double parentRadius = 0;
};

class C_Movement : public Component{
  Sprite *m_sprite;
  double m_initialX, m_initialY;
  double m_initialRotation;
  double m_currentSpeed;
  double m_acceleration, m_maxAcc, m_angularSpeed;
  double m_angle, m_parentRadius;
  bool m_push, m_right, m_left;
  double m_worldWidth, m_worldHeight;
  bool m_hasMessage;
  bool m_alwaysRun;
public:
  C_Movement(Entity* owner, MovementParam *param, String fileConfig);
  virtual ~C_Movement(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif