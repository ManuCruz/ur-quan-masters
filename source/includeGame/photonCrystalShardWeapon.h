#ifndef _PHOTON_CRYSTAL_SHARD_WEAPON_H
#define _PHOTON_CRYSTAL_SHARD_WEAPON_H

#include "weapon.h"

class C_PhotonCrystalShardWeapon : public C_Weapon{
  bool m_initShoot;
  bool m_shooting;
  AudioSource *m_aSourceEnd;

  Entity* m_bomb;
  int m_numOfFragment;
  String m_spriteFragmentName;
  double m_damageFragment;
  double m_distanceFragment;
  double m_speedFragment;

  virtual void Shooting();
  void Detonate();
public:
  C_PhotonCrystalShardWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_PhotonCrystalShardWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif