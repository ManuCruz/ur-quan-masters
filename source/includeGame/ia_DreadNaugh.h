#ifndef _IA_DREADNAUGH_H
#define _IA_DREADNAUGH_H

#include "component.h"

struct IADreadNaughParam{
  uint32 lifeThresholdToEscape = 0;
  double marginToFollowEnemy = 0;
  double distanceToShoot1 = 0;
  double distanceToShoot2 = 0;
};

class C_IA_DreadNaugh : public Component{
  class DreadNaughBT *m_pIATree;
public:
  C_IA_DreadNaugh(Entity* owner, IADreadNaughParam *param, String fileConfig);
  virtual ~C_IA_DreadNaugh(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif