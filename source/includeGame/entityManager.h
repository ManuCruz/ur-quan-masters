#ifndef _ENTITYMANAGER_H_
#define _ENTITYMANAGER_H_

#include "../include/types.h"
#include "../include/array.h"
#include "../include/string.h"

#include "component.h"
#include "messages.h"

class Entity;
class Scene;
class Sprite;

class EntityManager {
public:
  EntityManager(Scene *scene, String filename);
  ~EntityManager();

  bool isValid() const;
  void update(double elapsed);
  Entity* addEntity();
  void addComponent(Entity *entity, Components idComponent, void *param);

  void deleteEntity(Entity *entity);
  void removeInactiveEntities();

  uint32 getNOfShips() const;
  M_Stats getStats(uint32 nOfShip) const;

  void checkCollisions();
  Entity* checkCollision(Sprite *spr, uint32 index, uint32 &nextIndex);

  void addTarget(Entity *ent);

  void setASpriteShips();
  void setASpriteTargets();
  
  void getInfoEntity(uint32 idShip, double &x, double &y, double &radius);
  void getInfoNearestEnemy(uint32 idOwnerShip, double &x, double &y, double &radius);
  void getInfoNearestTarget(uint32 id, uint32 idOwner, double &x, double &y, double &radius);
  void getInfoShip(uint32 indexShip, double &x, double &y);
  Entity* getShip(uint32 indexShip) const;

  void setPositionCamera(double x, double y);
  void getWorldSize(double &width, double &height);

private:
  Scene *m_scene;
  String m_fileConfig;
  Array<Entity*> m_aEntities;
  Array<Entity*> m_aShips;
  Array<Sprite*> m_aSpriteShips;
  Array<Entity*> m_aTargets;
  Array<Sprite*> m_aSpriteTargets;

  bool m_needRemove;

  double m_maxWidth, m_maxHeight;
};

#endif
