#ifndef _TRACTOR_BEAN_WEAPON_H
#define _TRACTOR_BEAN_WEAPON_H

#include "weapon.h"

class Sprite;

class C_TractorBeanWeapon : public C_Weapon{
  Sprite *m_spriteParent;
  virtual void Shooting();
public:
  C_TractorBeanWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_TractorBeanWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif