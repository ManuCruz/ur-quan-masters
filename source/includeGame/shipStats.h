#ifndef _SHIP_STATS_H
#define _SHIP_STATS_H

#include "component.h"

struct ShipStatsParam{
  String name = "";
  uint32 maxLife = 0;
  uint32 maxBattery = 0;
  uint32 value = 0;
  double regBattery = 0;
};

class C_ShipStats : public Component{
  String m_name;
  uint32 m_maxLife, m_maxBattery, m_value;
  double m_regBattery;
  int32 m_currentLife;
  double m_currentBattery;
  String m_spriteExplosion;
  String m_soundExplosion;
  double m_angularSpeedExplosion;
public:
  C_ShipStats(Entity* owner, ShipStatsParam *param, String fileConfig);
  virtual ~C_ShipStats(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif