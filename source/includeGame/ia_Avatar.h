#ifndef _IA_AVATAR_H
#define _IA_AVATAR_H

#include "component.h"

struct IAAvatarParam{
  uint32 numberOfZapsToRun = 0;
  double distanceToShoot1 = 0;
  double distanceToShoot2 = 0;
};

class C_IA_Avatar : public Component{
  class AvatarBT *m_pIATree;
public:
  C_IA_Avatar(Entity* owner, IAAvatarParam *param, String fileConfig);
  virtual ~C_IA_Avatar(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif