#ifndef _LIMPET_WEAPON_H
#define _LIMPET_WEAPON_H

#include "weapon.h"

class C_LimpetWeapon : public C_Weapon{
  virtual void Shooting();
public:
  C_LimpetWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_LimpetWeapon() {};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif