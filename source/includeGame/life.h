#ifndef _LIFE_H
#define _LIFE_H

#include "component.h"

struct LifeParam{
  uint32 life = 0;
};

class C_Life : public Component{
  int32 m_life;
public:
  C_Life(Entity* owner, LifeParam *param, String fileConfig);
  virtual ~C_Life(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif