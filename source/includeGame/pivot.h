#ifndef _PIVOT_H
#define _PIVOT_H

#include "component.h"

class Sprite;

struct PivotParam{
  double angularSpeed = 0;
  double distance = 0;
  double xCenter = 0;
  double yCenter = 0;
  double initialAngle = 0;
};

class C_Pivot : public Component{
  Sprite *m_sprite;
  double m_xCenter, m_yCenter;
  double m_angularSpeed;
  double m_distance;
  double m_currentAngle;
public:
  C_Pivot(Entity* owner, PivotParam *param, String fileConfig);
  virtual ~C_Pivot(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif