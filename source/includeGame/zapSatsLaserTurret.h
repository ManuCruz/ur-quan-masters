#ifndef _ZAP_SATS_LASER_TURRET_H
#define _ZAP_SATS_LASER_TURRET_H

#include "component.h"

class Sprite;

struct ZapSatsLaserTurretParam{
  double angle = 0;
};

class C_ZapSatsLaserTurret : public Component{
  Sprite *m_spriteParent;
  String m_spriteName;
  double m_angle;
  Entity *m_zap;
  bool m_init;
  uint16 m_vframes;
  int16 m_fps;
  double m_distance;
  double m_angularSpeed;
  uint32 m_life;
public:
  C_ZapSatsLaserTurret(Entity* owner, ZapSatsLaserTurretParam *param, String fileConfig);
  virtual ~C_ZapSatsLaserTurret(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif