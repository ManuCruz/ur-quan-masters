#ifndef _CAMERA_CONTROLLER_H
#define _CAMERA_CONTROLLER_H

#include "component.h"
#include "math.h"

struct CameraControllerParam{
  double maxWidth = INFINITY;
  double maxHeight = INFINITY;
};

class C_CameraController : public Component{
  double m_maxWidth, m_maxHeight;
  bool m_firstUpdate;
  double m_ratio;
  double m_margin;
public:
  C_CameraController(Entity* owner, CameraControllerParam *param, String fileConfig);
  virtual ~C_CameraController(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif