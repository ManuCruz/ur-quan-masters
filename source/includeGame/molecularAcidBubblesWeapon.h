#ifndef _MOLECULAR_ACID_BUBBLES_WEAPON_H
#define _MOLECULAR_ACID_BUBBLES_WEAPON_H

#include "weapon.h"

class C_MolecularAcidBubblesWeapon : public C_Weapon{
  virtual void Shooting();
  double m_changeTime;
  double m_randomTime;
public:
  C_MolecularAcidBubblesWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_MolecularAcidBubblesWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif