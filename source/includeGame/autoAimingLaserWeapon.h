#ifndef _AUTO_AIMING_LASER_WEAPON_H
#define _AUTO_AIMING_LASER_WEAPON_H

#include "laserWeapon.h"

class C_AutoAimingLaserWeapon : public C_LaserWeapon{
  virtual void Shooting();
public:
  C_AutoAimingLaserWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_AutoAimingLaserWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};


#endif