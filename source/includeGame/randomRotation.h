#ifndef _RANDOM_ROTATION_H
#define _RANDOM_ROTATION_H

#include "component.h"

struct RandomRotationParam{
  double changeTime = 0;
  double randomTime = 0;
};

class C_RandomRotation : public Component{
  double m_changeTime;
  double m_currentTime;
  double m_endTime;
  double m_timer;
  bool m_init;
  M_Movement *m_mesMovement;
public:
  C_RandomRotation(Entity* owner, RandomRotationParam *param, String fileConfig);
  virtual ~C_RandomRotation();
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif