#ifndef _DESTROY_WHEN_DO_DAMAGE_H
#define _DESTROY_WHEN_DO_DAMAGE_H

#include "component.h"

struct DestroyWhenDoDamageParam{
};

class C_DestroyWhenDoDamage : public Component{
public:
  C_DestroyWhenDoDamage(Entity* owner, DestroyWhenDoDamageParam *param, String fileConfig);
  virtual ~C_DestroyWhenDoDamage(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif