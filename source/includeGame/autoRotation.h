#ifndef _AUTO_ROTATION_H
#define _AUTO_ROTATION_H

#include "component.h"

struct AutoRotationParam{
  bool toRight = false;
};

class C_AutoRotation : public Component{
  bool m_toRight;
public:
  C_AutoRotation(Entity* owner, AutoRotationParam *param, String fileConfig);
  virtual ~C_AutoRotation(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif