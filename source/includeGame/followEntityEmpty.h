#ifndef _FOLLOW_ENTITY_EMPTY_H
#define _FOLLOW_ENTITY_EMPTY_H

#include "followEntityMovement.h"

class C_FollowEntityEmpty : public C_FollowEntityMovement{
  virtual void OnTheTarget();
  virtual void OnTheOrigin();
public:
  C_FollowEntityEmpty(Entity* owner, FollowEntityMovementParam *param, String fileConfig);
  virtual ~C_FollowEntityEmpty(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif