#ifndef _FACE_AND_SHOOT_LASER_H
#define _FACE_AND_SHOOT_LASER_H

#include "component.h"

class Sprite;

struct FaceAndShootLaserParam{
};

class C_FaceAndShootLaser : public Component{
  Sprite *m_sprite;
  String m_spriteName;
  double m_distance;
  double m_damage;
  double m_timeToShot, m_currentTimeToShot;

  void ShootLaser();
public:
  C_FaceAndShootLaser(Entity* owner, FaceAndShootLaserParam *param, String fileConfig);
  virtual ~C_FaceAndShootLaser(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif