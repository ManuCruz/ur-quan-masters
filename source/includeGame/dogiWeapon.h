#ifndef _DOGI_WEAPON_H
#define _DOGI_WEAPON_H

#include "weapon.h"

class C_DOGIWeapon : public C_Weapon{
  uint32 m_life;
  uint32 m_limit;
  double m_repulsionforce;

  Array<Entity *> m_aDOGI;
  AudioSource *m_aSourceEnd;
  uint32 m_numOfDOGI;

  virtual void Shooting();
  void checkDOGI();
public:
  C_DOGIWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_DOGIWeapon();
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif