#ifndef _DO_DAMAGE_H
#define _DO_DAMAGE_H

#include "component.h"

struct DoDamageParam{
  uint32 damage = 0;
  uint32 owner = 0;
};

class C_DoDamage : public Component{
  uint32 m_damage, m_owner;
public:
  C_DoDamage(Entity* owner, DoDamageParam *param, String fileConfig);
  virtual ~C_DoDamage(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif