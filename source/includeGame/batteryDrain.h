#ifndef _BATTERY_DRAIN_H
#define _BATTERY_DRAIN_H

#include "component.h"

struct BatteryDrainParam{
  uint32 amount = 0;
  uint32 owner = 0;
};

class C_BatteryDrain : public Component{
  uint32 m_amount, m_owner;
public:
  C_BatteryDrain(Entity* owner, BatteryDrainParam *param, String fileConfig);
  virtual ~C_BatteryDrain(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif