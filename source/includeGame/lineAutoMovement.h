#ifndef _LINE_AUTO_MOVEMENT_H
#define _LINE_AUTO_MOVEMENT_H

#include "component.h"

struct LineAutoMovementParam{
};

class C_LineAutoMovement : public Component{
public:
  C_LineAutoMovement(Entity* owner, LineAutoMovementParam *param, String fileConfig);
  virtual ~C_LineAutoMovement(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif