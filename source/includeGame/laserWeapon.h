#ifndef _LASER_WEAPON_H
#define _LASER_WEAPON_H

#include "weapon.h"

class C_LaserWeapon : public C_Weapon{
protected:
  virtual void Shooting();
public:
  C_LaserWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_LaserWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif