#ifndef _REDUCE_MOBILIY_H
#define _REDUCE_MOBILIY_H

#include "component.h"

struct ReduceMobilityParam{
  uint32 percentage = 0;
  uint32 owner = 0;
};

class C_ReduceMobility : public Component{
  uint32 m_percentage, m_owner;
  bool m_active;
public:
  C_ReduceMobility(Entity* owner, ReduceMobilityParam *param, String fileConfig);
  virtual ~C_ReduceMobility(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif