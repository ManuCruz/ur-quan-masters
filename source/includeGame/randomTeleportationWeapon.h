#ifndef _RANDOM_TELEPORTATION_WEAPON_H
#define _RANDOM_TELEPORTATION_WEAPON_H

#include "weapon.h"

class C_RandomTeleportationWeapon : public C_Weapon{
  virtual void Shooting();
  Sprite *m_sprite;
  double m_maxWidth, m_maxHeight;
  int32 m_checkCollision;
public:
  C_RandomTeleportationWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_RandomTeleportationWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif