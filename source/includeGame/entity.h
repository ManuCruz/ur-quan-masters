#ifndef _ENTITY_H
#define _ENTITY_H

#include "../include/array.h"
#include "../include/types.h"

class EntityManager;
class Message;
class Component;

static uint32 id_entity = 1;
class Entity{
  EntityManager *m_eManager;
  Array<Component*> m_Components;
  uint32 m_id;
  bool m_active;
  uint32 m_idOwner;
public:
  Entity() {}
  Entity(EntityManager *eManager);
  virtual ~Entity();
  virtual void Update(double elapsed);
  void AddComponent(Component* comp) { m_Components.Add(comp); }
  void ReceiveMessage(Message *msg);
  uint32 GetId() { return m_id; }
  EntityManager* GetEntityManager() { return m_eManager; }
  bool GetActive() { return m_active; }
  void SetInactive() { m_active = false; }
  uint32 GetIdOwner() { return m_idOwner; }
  void SetIdOwner(uint32 idOwner) { m_idOwner = idOwner; }
};

#endif