#ifndef _KEYBOARD_CONTROL_H
#define _KEYBOARD_CONTROL_H

#include "../include/inputManager.h"

#include "component.h"

struct KeyboardControlParam{
  eInputCode push = NONE;
  eInputCode right = NONE;
  eInputCode left = NONE;
  eInputCode shot1 = NONE;
  eInputCode shot2 = NONE;
};

class C_KeyboardControl : public Component{
public:
  C_KeyboardControl(Entity* owner, KeyboardControlParam *param, String fileConfig);
  virtual ~C_KeyboardControl(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif