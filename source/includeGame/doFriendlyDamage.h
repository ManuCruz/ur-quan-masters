#ifndef _DO_FRIENDLY_DAMAGE_H
#define _DO_FRIENDLY_DAMAGE_H

#include "component.h"

struct DoFriendlyDamageParam{
  uint32 damage = 0;
  uint32 owner = 0;
};

class C_DoFriendlyDamage : public Component{
  uint32 m_damage, m_owner;
public:
  C_DoFriendlyDamage(Entity* owner, DoFriendlyDamageParam *param, String fileConfig);
  virtual ~C_DoFriendlyDamage(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif