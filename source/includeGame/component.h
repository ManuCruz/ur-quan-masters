#ifndef _COMPONENT_H
#define _COMPONENT_H

#include "../include/string.h"

#include "entity.h"
#include "messages.h"

#define NO_OWNER_ENTITY 0

enum Components{
  Renderable,
  RenderableLaser,
  ShipStats,
  Movement,
  KeyboardControl,
  Weapon,
  LineAutoMovement,
  AutoRotation,
  DoDamage,
  DoFriendlyDamage,
  DestroyWhenDoDamage,
  FollowEntityAutonomousFighter,
  FollowEntityEmpty,
  Life,
  Sound,
  DestroyAfterDistance,
  CameraController,
  Planet,
  IsAffectedByGravity,
  ZapSatsLaserTurret,
  Pivot,
  FaceAndShootLaser,
  BatteryDrain,
  ReduceMobility,
  RandomRotation,
  IA_DreadNaugh,
  IA_Avatar,
  IA_Marker,
};

class Component{
  Entity* m_Owner;
public:
  Component(Entity* owner);
  virtual ~Component();
  virtual void Update(double elapsed) = 0;
  virtual void ReceiveMessage(Message *msg) = 0;
  Entity* getOwner() const;
};

#endif