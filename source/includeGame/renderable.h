#ifndef _RENDERABLE_H
#define _RENDERABLE_H

#include "component.h"

class Sprite;
class Image;

struct RenderableParam{
  String spriteName = "";
  Sprite *sprite = NULL;
  uint16 hframes = 1;
  uint16 vframes = 1;
  int16 fps = 0;
};

class C_Renderable : public Component{
  Sprite *m_sprite;
  const Image *m_originalImage;

  void ChangeImage(const Image *image);
public:
  C_Renderable(Entity* owner, RenderableParam *param, String fileConfig);
  virtual ~C_Renderable(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif