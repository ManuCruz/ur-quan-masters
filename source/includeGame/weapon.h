#ifndef _WEAPON_H
#define _WEAPON_H

#include "component.h"

class AudioSource;

enum WeaponTypes{
  FusionBlaster = 0,
  AutonomousFighter,
  HeavyXrayLaser,
  TractorBean,
  PhotonCrystalShard,
  DOGI,
  MolecularAcidBubbles,
  BlazerForm,
  GigaWattLaser,
  Limpet,
  AutoAimingLaser,
  RandomTeleportation,
};

static const char *WeaponTypes[] = {"FusionBlaster", 
                                    "AutonomousFighter",
                                    "HeavyXrayLaser",
                                    "TractorBean",
                                    "PhotonCrystalShard",
                                    "DOGI",
                                    "MolecularAcidBubbles",
                                    "BlazerForm",
                                    "GigaWattLaser",
                                    "Limpet",
                                    "AutoAimingLaser",
                                    "RandomTeleportation" };

struct WeaponParam{
  String name = "";
  uint32 id = 0;
  uint32 energy = 0;
  uint32 reload = 0;
  uint32 power = 0;
  uint32 distance = 0;
  uint32 speed = 0;
  uint32 angularSpeed = 0;
};

class C_Weapon : public Component{
protected:
  String m_name;
  String m_spriteName;
  uint32 m_id, m_energy, m_reload, m_power, m_distance, m_speed, m_angularSpeed;
  bool m_cooling;
  double m_coolingTime;
  bool m_shot;
  bool m_canShot;

  AudioSource *m_aSource;

  void SpendEnergy();
  virtual void Shooting();
public:
  C_Weapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_Weapon();
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif