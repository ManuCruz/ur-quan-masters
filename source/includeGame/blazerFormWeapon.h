#ifndef _BLAZER_FORM_WEAPON_H
#define _BLAZER_FORM_WEAPON_H

#include "weapon.h"
class Image;

class C_BlazerFormWeapon : public C_Weapon{
  virtual void Shooting();
  void Transform(bool activated);

  double m_factor;
  double m_energyCost;
  bool m_transform;
  Image *m_image;
public:
  C_BlazerFormWeapon(Entity* owner, WeaponParam *param, String fileConfig);
  virtual ~C_BlazerFormWeapon(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};


#endif