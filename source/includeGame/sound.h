#ifndef _SOUND_H
#define _SOUND_H

#include "component.h"

class AudioSource;

struct SoundParam{
  String soundName = "";
  bool looping = false;
};

class C_Sound : public Component{
  AudioSource *m_aSource;
  bool m_play;
public:
  C_Sound(Entity* owner, SoundParam *param, String fileConfig);
  virtual ~C_Sound();
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif