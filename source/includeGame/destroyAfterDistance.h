#ifndef _DESTROY_AFTER_DISTANCE_H
#define _DESTROY_AFTER_DISTANCE_H

#include "component.h"

struct DestroyAfterDistanceParam{
  double distance = 0;
  double speed = 0;
};

class C_DestroyAfterDistance : public Component{
  double m_speed, m_distance;
  double m_currentDistance;
public:
  C_DestroyAfterDistance(Entity* owner, DestroyAfterDistanceParam *param, String fileConfig);
  virtual ~C_DestroyAfterDistance(){};
  virtual void Update(double elapsed);
  virtual void ReceiveMessage(Message *msg);
};

#endif