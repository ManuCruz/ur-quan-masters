#include "DreadNaughBT.h"	

#include "../../includeGame/ia_DreadNaugh.h"

#include "../SequenceNode.h"
#include "../TaskNodes/FleeCheckTN.h"
#include "../TaskNodes/SimpleShootTN.h"
#include "../TaskNodes/FrontShootTN.h"
#include "../TaskNodes/SelectNearestEnemyTN.h"
#include "../TaskNodes/SelectSafePositionTN.h"
#include "../TaskNodes/GotoPointTN.h"

// ********************************************************************
// Si la vida baja por debajo de un umbral, huye hacia en direcci�n contraria al enemigo m�s cercano mientras dispara 
// siempre que pueda el segundo arma
// 
// Si la vida es superior al umbral, se dirige hacia el enemigo m�s cercano disparando siempre que pueda el primer arma (si
// el enemigo est� delante de la nave
//
// ********************************************************************
DreadNaughBT::DreadNaughBT(Entity *pEntity, IADreadNaughParam *param) : CSelectorNode(pEntity, &m_context){
	// El nodo r�iz es un nodo selector al que vamos a�adir el resto de nodos de la conducta
	addNode(
		(new CSequenceNode(pEntity, &m_context))-> // HUIR
	  	addNode(new FleeCheckTN(pEntity, &m_context, param->lifeThresholdToEscape))->
      addNode(new SelectSafePositionTN(pEntity, &m_context))->
      addNode(new GotoPointTN(pEntity, &m_context, 0))->
      addNode(new SimpleShootTN(pEntity, &m_context, 2, param->distanceToShoot2)))->
  addNode(
    (new CSequenceNode(pEntity, &m_context))-> // ATACAR
      addNode(new SelectNearestEnemyTN(pEntity, &m_context))->
      addNode(new GotoPointTN(pEntity, &m_context, param->marginToFollowEnemy))->
      addNode(new FrontShootTN(pEntity, &m_context, 1, param->distanceToShoot1)));
}
