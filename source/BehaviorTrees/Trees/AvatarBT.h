#ifndef _AVATAR_BT_H
#define _AVATAR_BT_H

#include "../SelectorNode.h"
#include "../TreeContext.h"

class Entity;
struct IAAvatarParam;
class AvatarBT : public CSelectorNode{
public:
  AvatarBT(Entity *pEntity, IAAvatarParam *param);

private:
	CTreeContext m_context;
};

#endif