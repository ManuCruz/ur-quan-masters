#ifndef _DREADNAUGH_BT_H
#define _DREADNAUGH_BT_H

#include "../SelectorNode.h"
#include "../TreeContext.h"

class Entity;
struct IADreadNaughParam;
class DreadNaughBT : public CSelectorNode{
public:
  DreadNaughBT(Entity *pEntity, IADreadNaughParam *param);

private:
	CTreeContext m_context;
};

#endif