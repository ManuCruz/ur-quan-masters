#include "AvatarBT.h"	

#include "../../includeGame/ia_Avatar.h"

#include "../SequenceNode.h"
#include "../TaskNodes/ZapSatsCheckTN.h"
#include "../TaskNodes/FaceEnemyTN.h"
#include "../TaskNodes/AttractShootTN.h"
#include "../TaskNodes/FrontShootTN.h"
#include "../TaskNodes/SelectNearestEnemyTN.h"
#include "../TaskNodes/SelectSafePositionTN.h"
#include "../TaskNodes/GotoPointTN.h"

// ********************************************************************
// Si tiene 1 zapsats o menos, huye en direcci�n contraria al enemigo m�s cercano y dispara el arma 1 (l�ser) si se encuentra un
// enemigo dentro de su "cono de visi�n"
//
// Si tiene 2 o m�s zapsats, gira sobre si mismo para encarar al enemigo m�s cercano mientras lo atrae hasta una determinada distancia.
// Cuando el enemigo est� m�s cerca de discha distancia, deja de atraerlo y dispara el l�ser
//
// ********************************************************************
AvatarBT::AvatarBT(Entity *pEntity, IAAvatarParam *param) : CSelectorNode(pEntity, &m_context){
	// El nodo r�iz es un nodo selector al que vamos a�adir el resto de nodos de la conducta
	addNode(
		(new CSequenceNode(pEntity, &m_context))-> // HUIR
      addNode(new ZapSatsCheckTN(pEntity, &m_context, param->numberOfZapsToRun))->
      addNode(new SelectSafePositionTN(pEntity, &m_context))->
      addNode(new GotoPointTN(pEntity, &m_context, 0))->
      addNode(new FrontShootTN(pEntity, &m_context, 1, param->distanceToShoot1)))->
  addNode(
    (new CSequenceNode(pEntity, &m_context))-> // ATACAR
      addNode(new SelectNearestEnemyTN(pEntity, &m_context))->
      addNode(new FaceEnemyTN(pEntity, &m_context))->
      addNode(new AttractShootTN(pEntity, &m_context, 2, param->distanceToShoot2))->
      addNode(new FrontShootTN(pEntity, &m_context, 1, param->distanceToShoot1)));
}
