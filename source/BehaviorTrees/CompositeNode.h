#ifndef ___CCompositeNode___
#define ___CCompositeNode___

#include "TreeNode.h"
#include <vector>

class CCompositeNode : public CTreeNode
{
public:
	CCompositeNode(Entity *pEntity, CTreeContext *pContext);

	// FORM ITreeNode
	virtual void init() override;

	CCompositeNode *addNode(ITreeNode *pChild);

protected:
	ITreeNode::TResult runNode    (unsigned int idx);
	unsigned int       getNumNodes() const;

private:
	std::vector<ITreeNode *> m_vNodes;
	std::vector<bool>        m_vInits;
};
#endif