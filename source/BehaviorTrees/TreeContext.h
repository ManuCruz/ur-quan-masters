#ifndef ___CTreeContext___
#define ___CTreeContext___

#include <map>

class Entity;
class CTreeContext
{
public:
	struct TPoint
	{
		int x;
		int y;
	};

	CTreeContext();

	// Context Keys
	enum TKey
	{
		EDestPoint,
    EDestPointX,
    EDestPointY,
    EDestRadius,
		//EDestPointResult,

		//EDestOrientation,

		//ETargetAttackEntity,
	};

	void setValuePoint	(TKey key, TPoint *pValue);
	void setValueDouble	(TKey key, double  dValue);
  void setValueEntity (TKey key, Entity *pValue);

	bool getValuePoint (TKey key, TPoint  *pValue = NULL);
	bool getValueDouble(TKey key, double  *pValue = NULL);
  bool getValueEntity(TKey key, Entity **pValue = NULL);

private:
	std::map<TKey, double>		m_mapDoubles;
	std::map<TKey, TPoint*>		m_mapPoints;
  std::map<TKey, Entity*>	  m_mapEntities;
};

#endif