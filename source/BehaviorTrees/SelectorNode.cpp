#include "SelectorNode.h"

// ****************************************************************
// CSelectorNode
// ****************************************************************
CSelectorNode::CSelectorNode(Entity *pEntity, CTreeContext *pContext) : CCompositeNode(pEntity, pContext)
{
}

// ****************************************************************
// run
// ****************************************************************
ITreeNode::TResult CSelectorNode::run()
{
	TResult ret = EFail;

	unsigned int i				= 0;
	unsigned int numNodes = getNumNodes();
	while ((i < numNodes) && (ret == EFail))
		ret = runNode(i++);

	return ret;
}