#include "SequenceNode.h"

// ****************************************************************
// CSequenceNode
// ****************************************************************
CSequenceNode::CSequenceNode(Entity *pEntity, CTreeContext *pContext) : CCompositeNode(pEntity, pContext)
{
}

// ****************************************************************
// run
// ****************************************************************
ITreeNode::TResult CSequenceNode::run()
{
	TResult ret = ESuccess;

	unsigned int i				= 0;
	unsigned int numNodes = getNumNodes();
	while ((i < numNodes) && (ret == ESuccess))
		ret = runNode(i++);

	return ret;
}