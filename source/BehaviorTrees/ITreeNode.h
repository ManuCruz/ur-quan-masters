#ifndef ___ITreeNode___
#define ___ITreeNode___

#include "../include/types.h"

class ITreeNode
{
public:
	virtual ~ITreeNode(){};

	enum TResult
	{
		ERunning,
		ESuccess,
		EFail
	};

	virtual void    init() = 0;
	virtual TResult run () = 0;
};

#endif