#ifndef ___CTreeNode___
#define ___CTreeNode___

#include "ITreeNode.h"
class Entity;
class CTreeContext;

class CTreeNode : public ITreeNode
{
public:
  CTreeNode(Entity *pEntity, CTreeContext *pContext);

protected:
  Entity *getEntity();
	CTreeContext *getContext();

private:
  Entity *m_pEntity;
	CTreeContext *m_pContext;
};

#endif