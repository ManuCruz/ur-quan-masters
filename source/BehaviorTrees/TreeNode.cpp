#include "TreeNode.h"

// ****************************************************************
// CTreeNode
// ****************************************************************
CTreeNode::CTreeNode(Entity *pEntity, CTreeContext *pContext)
{
	m_pEntity  = pEntity;
	m_pContext = pContext;
}

// ****************************************************************
// getEntity
// ****************************************************************
Entity *CTreeNode::getEntity()
{
	return m_pEntity;
}

// ****************************************************************
// getContext
// ****************************************************************
CTreeContext *CTreeNode::getContext()
{
	return m_pContext;
}