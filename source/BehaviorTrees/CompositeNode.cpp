#include "CompositeNode.h"

// ****************************************************************
// CCompositeNode
// ****************************************************************
CCompositeNode::CCompositeNode(Entity *pEntity, CTreeContext *pContext) : CTreeNode(pEntity, pContext)
{
}

// ****************************************************************
// init
// ****************************************************************
void CCompositeNode::init()
{
	int iNumElem = m_vNodes.size();
	for (int i = 0; i < iNumElem; i++)
		m_vInits[i] = false;
}

// ****************************************************************
// addNode
// ****************************************************************
CCompositeNode *CCompositeNode::addNode(ITreeNode *pChild)
{
	if (pChild)
	{
		m_vNodes.push_back(pChild);
		m_vInits.push_back(false);
	}
	return this;
}

// ****************************************************************
// getNumNodes
// ****************************************************************
unsigned int CCompositeNode::getNumNodes() const
{
	return m_vNodes.size();
}

// ****************************************************************
// getNode
// ****************************************************************
ITreeNode::TResult CCompositeNode::runNode(unsigned int idx)
{
	TResult ret = EFail;
	ITreeNode *pNode = idx < m_vNodes.size() ? m_vNodes[idx] : NULL;
	if (pNode)
	{
		if (!m_vInits[idx])
		{
			pNode->init();
			m_vInits[idx] = true;
		}
		ret = pNode->run();
	}

	return ret;
}