#include "TreeContext.h"	

// ****************************************************************
// TreeContext
// ****************************************************************
CTreeContext::CTreeContext()
{
}

// ****************************************************************
// setValuePoint
// ****************************************************************
void CTreeContext::setValuePoint(TKey key, TPoint *pValue)
{
	if (pValue)
		m_mapPoints[key] = pValue;
}

// ****************************************************************
// 
// ****************************************************************
void CTreeContext::setValueDouble(TKey key, double dValue)
{
	m_mapDoubles[key] = dValue;
}

// ****************************************************************
// 
// ****************************************************************
void CTreeContext::setValueEntity(TKey key, Entity *pValue)
{
	m_mapEntities[key] = pValue;
}

// ****************************************************************
// 
// ****************************************************************
bool CTreeContext::getValuePoint(TKey key, CTreeContext::TPoint *pValue)
{
	bool bRet = false;
	std::map<TKey, TPoint*>::iterator it = m_mapPoints.find(key);
	if (it != m_mapPoints.end())
	{
		bRet = true;
		if (pValue)
			*pValue = *m_mapPoints[key]; 
	}

	return bRet;
}

// ****************************************************************
// 
// ****************************************************************
bool CTreeContext::getValueDouble(TKey key, double *pValue)
{
	bool bRet = false;
  std::map<TKey, double>::iterator it = m_mapDoubles.find(key);
	if (it != m_mapDoubles.end())
	{
		bRet = true;
		if (pValue)
      *pValue = m_mapDoubles[key];
	}

	return bRet;
}

// ****************************************************************
// 
// ****************************************************************
bool CTreeContext::getValueEntity(TKey key, Entity **pValue)
{
	bool bRet = false;
  std::map<TKey, Entity *>::iterator it = m_mapEntities.find(key);
	if (it != m_mapEntities.end())
	{
		bRet = true;
		if (pValue)
			*pValue = m_mapEntities[key]; 
	}

	return bRet;
}