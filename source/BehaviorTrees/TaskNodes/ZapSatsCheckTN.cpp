#include "ZapSatsCheckTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"

// ***********************************************************************
// ZapSatsCheckTN
// ***********************************************************************
ZapSatsCheckTN::ZapSatsCheckTN(Entity *pEntity, CTreeContext *pContext, int iNumberOfZapsToRun) : CTreeNode(pEntity, pContext){
  m_iNumberOfZapsToRun = iNumberOfZapsToRun;
}

// ***********************************************************************
// init
// ***********************************************************************
void ZapSatsCheckTN::init(){
}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult ZapSatsCheckTN::run(){
	TResult ret = EFail;

  M_GetActivedZaps *mesGetActivatedZaps = new M_GetActivedZaps();
  getEntity()->ReceiveMessage(mesGetActivatedZaps);
  if (mesGetActivatedZaps->amount <= m_iNumberOfZapsToRun)
      ret = ESuccess;
  delete mesGetActivatedZaps;

	return ret;
}