#include "FrontShootTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"

#include "../../include/sprite.h"


// ***********************************************************************
// FrontShootTN
// ***********************************************************************
FrontShootTN::FrontShootTN(Entity *pEntity, CTreeContext *pContext, uint32 id, double distance) : CTreeNode(pEntity, pContext){
  m_id = id;
  m_distance2 = pow(distance, 2);
  m_sprite = NULL;
}

// ***********************************************************************
// init
// ***********************************************************************
void FrontShootTN::init(){
}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult FrontShootTN::run(){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getEntity()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    if (m_distance2 == 0)
      shoot();
    else{
      double xTarget, yTarget;
      bool resX = getContext()->getValueDouble(CTreeContext::EDestPointX, &xTarget);
      bool resY = getContext()->getValueDouble(CTreeContext::EDestPointY, &yTarget);
      if (resX && resY){
        double dist = pow(m_sprite->GetX() - xTarget, 2) + pow(m_sprite->GetY() - yTarget, 2);
        if (dist <= m_distance2){
          //cross product
          double xUp = m_sprite->GetUpX();
          double yUp = m_sprite->GetUpY();
          double xV = xTarget - m_sprite->GetX();
          double yV = -(yTarget - m_sprite->GetY());

          double res = xV * yUp + yV * xUp;

          if (abs(res) < 10)
            shoot();
        }
      }
    }
  }

  return ESuccess;
}

void FrontShootTN::shoot(){
  M_Shot *mesShot = new M_Shot();
  mesShot->id = m_id;
  getEntity()->ReceiveMessage(mesShot);
  delete mesShot;
}