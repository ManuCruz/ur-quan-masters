#ifndef ___GOTOPOINT_TN___
#define ___GOTOPOINT_TN___

#include "../TreeNode.h"
#include "../TreeContext.h"

class Sprite;

class GotoPointTN : public CTreeNode{
public:
  GotoPointTN(Entity *pEntity, CTreeContext *pContext, double margin);

	virtual void    init();
	virtual TResult run ();

private:
  Sprite *m_sprite;
  double m_margin2;
};

#endif