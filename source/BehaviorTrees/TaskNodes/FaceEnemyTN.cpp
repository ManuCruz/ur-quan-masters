#include "FaceEnemyTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"
#include "../../includeGame/entityManager.h"

#include "../../include/sprite.h"

#define TOLERANCE 3

// ***********************************************************************
// FaceEnemyTN
// ***********************************************************************
FaceEnemyTN::FaceEnemyTN(Entity *pEntity, CTreeContext *pContext) : CTreeNode(pEntity, pContext){
  m_sprite = NULL;
}

// ***********************************************************************
// init
// ***********************************************************************
void FaceEnemyTN::init(){

}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult FaceEnemyTN::run(){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getEntity()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    double xDest, yDest;
    bool resX = getContext()->getValueDouble(CTreeContext::EDestPointX, &xDest);
    bool resY = getContext()->getValueDouble(CTreeContext::EDestPointY, &yDest);
    if (resX && resY){
      M_Movement *mesMovement = new M_Movement();
      //cross product
      double xUp = m_sprite->GetUpX();
      double yUp = m_sprite->GetUpY();
      double xV = xDest - m_sprite->GetX();
      double yV = -(yDest - m_sprite->GetY());

      double res = xV * yUp + yV * xUp;

      //rotate myself (if it is need)
      if (abs(res) > TOLERANCE)
        (res > 0) ? mesMovement->left = true : mesMovement->right = true;

      //change direction
      double maxWidth, maxHeight;
      getEntity()->GetEntityManager()->getWorldSize(maxWidth, maxHeight);
      bool checkX = abs(m_sprite->GetX() - xDest) > maxWidth / 2;
      bool checkY = abs(m_sprite->GetY() - yDest) > maxHeight / 2;
      if (checkX || checkY){
        mesMovement->left = !mesMovement->left;
        mesMovement->right = !mesMovement->right;
      }

      getEntity()->ReceiveMessage(mesMovement);
      delete mesMovement;
    }
  }

  return ESuccess;
}