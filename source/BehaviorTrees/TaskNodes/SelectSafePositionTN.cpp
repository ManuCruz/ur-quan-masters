#include "SelectSafePositionTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"
#include "../../includeGame/entityManager.h"

#include "../../include/math.h"
#include "../../include/sprite.h"

// ***********************************************************************
// SelectSafePositionTN
// ***********************************************************************
SelectSafePositionTN::SelectSafePositionTN(Entity *pEntity, CTreeContext *pContext) : CTreeNode(pEntity, pContext){
  m_sprite = NULL;
}

// ***********************************************************************
// init
// ***********************************************************************
void SelectSafePositionTN::init(){
}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult SelectSafePositionTN::run(){
  TResult ret = EFail;

  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getEntity()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    double xTarget = INFINITY;
    double yTarget = INFINITY;
    double radiusTarget = 0.;

    getEntity()->GetEntityManager()->getInfoNearestEnemy(getEntity()->GetIdOwner(), xTarget, yTarget, radiusTarget);

    if (xTarget != INFINITY){
      double maxWidth, maxHeight;
      getEntity()->GetEntityManager()->getWorldSize(maxWidth, maxHeight);

      getContext()->setValueDouble(CTreeContext::EDestPointX, WrapValue(m_sprite->GetX() + m_sprite->GetX() - xTarget, maxWidth));
      getContext()->setValueDouble(CTreeContext::EDestPointY, WrapValue(m_sprite->GetX() + m_sprite->GetY() - yTarget, maxHeight));

      ret = ESuccess;
    }
  }
  return ret;
}