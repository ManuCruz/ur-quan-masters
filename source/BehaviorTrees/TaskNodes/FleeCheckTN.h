#ifndef ___FLEECHECK_TN___
#define ___FLEECHECK_TN___

#include "../TreeNode.h"

class FleeCheckTN : public CTreeNode{
public:
  FleeCheckTN(Entity *pEntity, CTreeContext *pContext, int iThreshold);

	virtual void    init();
	virtual TResult run ();

private:
	uint32 m_iThreshold;
};

#endif