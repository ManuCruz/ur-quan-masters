#ifndef ___ATTRACTSHOOT_TN___
#define ___ATTRACTSHOOT_TN___

#include "../TreeNode.h"
#include "../TreeContext.h"

class Sprite;

class AttractShootTN : public CTreeNode{
public:
  AttractShootTN(Entity *pEntity, CTreeContext *pContext, uint32 id, double distance);

	virtual void    init();
	virtual TResult run ();

private:
  uint32 m_id;
  double m_distance2;
  Sprite *m_sprite;

  void shoot();
};

#endif