#include "SelectNearestEnemyTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"
#include "../../includeGame/entityManager.h"

// ***********************************************************************
// SelectNearestEnemyTN
// ***********************************************************************
SelectNearestEnemyTN::SelectNearestEnemyTN(Entity *pEntity, CTreeContext *pContext) : CTreeNode(pEntity, pContext){
}

// ***********************************************************************
// init
// ***********************************************************************
void SelectNearestEnemyTN::init(){
}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult SelectNearestEnemyTN::run(){
  TResult ret = EFail;

  double xTarget = INFINITY;
  double yTarget = INFINITY;
  double radiusTarget = 0.;

  getEntity()->GetEntityManager()->getInfoNearestEnemy(getEntity()->GetIdOwner(), xTarget, yTarget, radiusTarget);

  if (xTarget != INFINITY && yTarget != INFINITY){
    //CTreeContext::TPoint Dest;
    //Dest.x = xTarget;
    //Dest.y = yTarget;
    //getContext()->setValuePoint(CTreeContext::EDestPoint, &Dest);
    getContext()->setValueDouble(CTreeContext::EDestPointX, xTarget);
    getContext()->setValueDouble(CTreeContext::EDestPointY, yTarget);

    ret = ESuccess;
  }

  return ret;
}