#ifndef ___ZAPSATSCHECK_TN___
#define ___ZAPSATSCHECK_TN___

#include "../TreeNode.h"

class ZapSatsCheckTN : public CTreeNode{
public:
  ZapSatsCheckTN(Entity *pEntity, CTreeContext *pContext, int iNumberOfZapsToRun);

	virtual void    init();
	virtual TResult run ();

private:
  uint32 m_iNumberOfZapsToRun;
};

#endif