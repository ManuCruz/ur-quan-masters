#ifndef ___SELECTNEARESTENEMY_TN___
#define ___SELECTNEARESTENEMY_TN___

#include "../TreeNode.h"
#include "../TreeContext.h"

class SelectNearestEnemyTN : public CTreeNode{
public:
  SelectNearestEnemyTN(Entity *pEntity, CTreeContext *pContext);

	virtual void    init();
	virtual TResult run ();

private:
};

#endif