#ifndef ___SELECTSAFEPOSITION_TN___
#define ___SELECTSAFEPOSITION_TN___

#include "../TreeNode.h"
#include "../TreeContext.h"

class Sprite;

class SelectSafePositionTN : public CTreeNode{
public:
  SelectSafePositionTN(Entity *pEntity, CTreeContext *pContext);

	virtual void    init();
	virtual TResult run ();

private:
  Sprite *m_sprite;
};

#endif