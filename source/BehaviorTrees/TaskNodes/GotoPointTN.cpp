#include "GotoPointTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"
#include "../../includeGame/entityManager.h"

#include "../../include/sprite.h"

#define TOLERANCE 3

// ***********************************************************************
// CGotoPointTN
// ***********************************************************************
GotoPointTN::GotoPointTN(Entity *pEntity, CTreeContext *pContext, double margin) : CTreeNode(pEntity, pContext){
  m_sprite = NULL;
  m_margin2 = pow(margin, 2);;
}

// ***********************************************************************
// init
// ***********************************************************************
void GotoPointTN::init(){

}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult GotoPointTN::run(){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getEntity()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    //CTreeContext::TPoint *Dest;
    //bool res = getContext()->getValuePoint(CTreeContext::EDestPoint, Dest);
    double xDest, yDest;
    bool resX = getContext()->getValueDouble(CTreeContext::EDestPointX, &xDest);
    bool resY = getContext()->getValueDouble(CTreeContext::EDestPointY, &yDest);
    if (resX && resY){
      //move (if it is need)
      M_Movement *mesMovement = new M_Movement();
      double dist = pow(m_sprite->GetX() - xDest, 2) + pow(m_sprite->GetY() - yDest, 2);
      if (dist > m_margin2)
        mesMovement->push = true;

      //cross product
      double xUp = m_sprite->GetUpX();
      double yUp = m_sprite->GetUpY();
      double xV = xDest - m_sprite->GetX();
      double yV = -(yDest - m_sprite->GetY());

      double res = xV * yUp + yV * xUp;

      //rotate myself (if it is need)
      if (abs(res) > TOLERANCE)
        (res > 0) ? mesMovement->left = true : mesMovement->right = true;

      //change direction (ajuste para que aprovecha la salida por un borde para salir por el opuesto)
      double maxWidth, maxHeight;
      getEntity()->GetEntityManager()->getWorldSize(maxWidth, maxHeight);
      bool checkX = abs(m_sprite->GetX() - xDest) > maxWidth / 2;
      bool checkY = abs(m_sprite->GetY() - yDest) > maxHeight / 2;
      if (checkX || checkY){
        mesMovement->left = !mesMovement->left;
        mesMovement->right = !mesMovement->right;
      }

      getEntity()->ReceiveMessage(mesMovement);
      delete mesMovement;
    }
  }

  return ESuccess;
}