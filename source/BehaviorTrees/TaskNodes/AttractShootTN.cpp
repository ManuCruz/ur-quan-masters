#include "AttractShootTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"

#include "../../include/sprite.h"


// ***********************************************************************
// AttractShootTN
// ***********************************************************************
AttractShootTN::AttractShootTN(Entity *pEntity, CTreeContext *pContext, uint32 id, double distance) : CTreeNode(pEntity, pContext){
  m_id = id;
  m_distance2 = pow(distance, 2);
  m_sprite = NULL;
}

// ***********************************************************************
// init
// ***********************************************************************
void AttractShootTN::init(){
}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult AttractShootTN::run(){
  if (!m_sprite){
    M_GetSprite *mesGetSprite = new M_GetSprite();
    getEntity()->ReceiveMessage(mesGetSprite);
    m_sprite = mesGetSprite->sprite;
    delete mesGetSprite;
  }
  if (m_sprite){
    if (m_distance2 == 0)
      shoot();
    else{
      double xTarget, yTarget;
      bool resX = getContext()->getValueDouble(CTreeContext::EDestPointX, &xTarget);
      bool resY = getContext()->getValueDouble(CTreeContext::EDestPointY, &yTarget);
      if (resX && resY){
        double dist = pow(m_sprite->GetX() - xTarget, 2) + pow(m_sprite->GetY() - yTarget, 2);
        if (dist >= m_distance2)
          shoot();
      }
    }
  }

  return ESuccess;
}

void AttractShootTN::shoot(){
  M_Shot *mesShot = new M_Shot();
  mesShot->id = m_id;
  getEntity()->ReceiveMessage(mesShot);
  delete mesShot;
}