#include "FleeCheckTN.h"

#include "../../includeGame/messages.h"
#include "../../includeGame/entity.h"

// ***********************************************************************
// FleeCheckTN
// ***********************************************************************
FleeCheckTN::FleeCheckTN(Entity *pEntity, CTreeContext *pContext, int iThreshold) : CTreeNode(pEntity, pContext){
	m_iThreshold	= iThreshold;
}

// ***********************************************************************
// init
// ***********************************************************************
void FleeCheckTN::init(){
}

// ***********************************************************************
// run 
// ***********************************************************************
ITreeNode::TResult FleeCheckTN::run(){
	TResult ret = EFail;

  M_GetLife *mesLife = new M_GetLife();
  getEntity()->ReceiveMessage(mesLife);
  if (mesLife->amount <= m_iThreshold)   //si la vida es menor que el umbral, se devuelve ESuccess para iniciar el proceso de huida
      ret = ESuccess;
  delete mesLife;

	return ret;
}