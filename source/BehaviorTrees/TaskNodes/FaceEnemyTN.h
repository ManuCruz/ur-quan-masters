#ifndef ___FACEENEMY_TN___
#define ___FACEENEMY_TN___

#include "../TreeNode.h"
#include "../TreeContext.h"

class Sprite;

class FaceEnemyTN : public CTreeNode{
public:
  FaceEnemyTN(Entity *pEntity, CTreeContext *pContext);

	virtual void    init();
	virtual TResult run ();

private:
  Sprite *m_sprite;
};

#endif