#ifndef ___CSequenceNode___
#define ___CSequenceNode___

#include "CompositeNode.h"
class CSequenceNode : public CCompositeNode
{
public:
  CSequenceNode(Entity *pEntity, CTreeContext *pContext);

	// FROM ITreeNode
	virtual TResult run () override;
};

#endif