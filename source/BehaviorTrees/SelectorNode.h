#ifndef ___CSelectorNode___
#define ___CSelectorNode___

#include "CompositeNode.h"
class CSelectorNode : public CCompositeNode
{
public:
  CSelectorNode(Entity *pEntity, CTreeContext *pContext);

	// FROM ITreeNode
	virtual TResult run () override;
};

#endif