#include "../include/inputManager.h"
#include "../include/math.h"

#include "../include/glinclude.h"

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <XInput.h>

#pragma comment(lib, "XInput.lib")


InputManager* InputManager::inputManager = NULL;

InputManager::InputManager() {
  valid = false;
  powerLeft = 0;
  powerRight = 0;
}

InputManager::~InputManager() {
  End();
}

InputManager& InputManager::Instance() {
  if (!inputManager)
    inputManager = new InputManager();
  return *inputManager;
}

bool InputManager::Init(){
  valid = true;
  return valid;
}

void InputManager::End(){
  valid = false;
}

bool InputManager::IsOk(){
  return valid;
}

bool checkPadButtom(XINPUT_STATE state, std::map<String, Action>::iterator &itr, int hex){
  if (state.Gamepad.wButtons & hex){
    itr->second.state = (itr->second.prevState == down) ? pressed : down;
    return true;
  }
  itr->second.state = (itr->second.prevState == pressed || itr->second.prevState == down) ? up : notPressed;
  return false;
}

bool checkPadButtomPressed(XINPUT_STATE state, int hex){
  if (state.Gamepad.wButtons & hex){
    return true;
  }
  return false;
}

void InputManager::Update(){
  std::map<String, Action>::iterator itr;
  for (itr = dicAction.begin(); itr != dicAction.end(); ++itr){
    itr->second.prevState = itr->second.state;
    for (uint32 i = 0; i < itr->second.inputCodes.Size(); i++){
      eInputCode code = itr->second.inputCodes[i];
      if (ValueInRange(code, KEYBOARD_OFFSET, MOUSE_OFFSET - 1)){
        if (glfwGetKey(code) == GLFW_PRESS){
          itr->second.state = (itr->second.prevState == down) ? pressed : down;
          break;
        }
        else
          itr->second.state = (itr->second.prevState == pressed || itr->second.prevState == down) ? up : notPressed;
      }
      else if (ValueInRange(code, MOUSE_OFFSET, PAD_OFFSET - 1)){
        if (code <= MOUSE_8){
          int32 value = code - MOUSE_OFFSET;
          if (glfwGetMouseButton(value) == GLFW_PRESS){
            itr->second.state = (itr->second.prevState == down) ? pressed : down;
            break;
          }
          else
            itr->second.state = (itr->second.prevState == pressed || itr->second.prevState == down) ? up : notPressed;
        }
      }
      else{ // code >= PAD_OFFSET
        XINPUT_STATE state;
        ZeroMemory(&state, sizeof(XINPUT_STATE));
        bool exit = false;

        if (XInputGetState(0, &state) == ERROR_SUCCESS){
          switch (code){
          case PAD_DPAD_UP:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_DPAD_UP);
            break;
          case PAD_DPAD_DOWN:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_DPAD_DOWN);
            break;
          case PAD_DPAD_LEFT:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_DPAD_LEFT);
            break;
          case PAD_DPAD_RIGHT:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_DPAD_RIGHT);
            break;
          case PAD_START:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_START);
            break;
          case PAD_BACK:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_BACK);
            break;
          case PAD_LEFT_THUMB:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_LEFT_THUMB);
            break;
          case PAD_RIGHT_THUMB:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_RIGHT_THUMB);
            break;
          case PAD_LEFT_SHOULDER:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_LEFT_SHOULDER);
            break;
          case PAD_RIGHT_SHOULDER:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_RIGHT_SHOULDER);
            break;
          case PAD_A:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_A);
            break;
          case PAD_B:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_B);
            break;
          case PAD_X:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_X);
            break;
          case PAD_Y:
            exit = checkPadButtom(state, itr, XINPUT_GAMEPAD_Y);
            break;
          }
          if (exit)
            break;
        }
      }
    }
  }

  std::map<String, ActionAxis>::iterator itrAxis;
  for (itrAxis = dicActionAxis.begin(); itrAxis != dicActionAxis.end(); ++itrAxis){
    for (uint32 i = 0; i < itrAxis->second.inputCodesPos.Size(); i++){
      //positive
      eInputCode codePos = itrAxis->second.inputCodesPos[i];
      if (ValueInRange(codePos, KEYBOARD_OFFSET, MOUSE_OFFSET - 1)){
        if (glfwGetKey(codePos) == GLFW_PRESS){
          itrAxis->second.prevValue = itrAxis->second.value;
          if (itrAxis->second.value < MAX_ABS)
            itrAxis->second.value = itrAxis->second.prevValue + STEP;
          break;
        }
      }
      else if (ValueInRange(codePos, MOUSE_OFFSET, PAD_OFFSET - 1)){

      }
      else{ // codePos >= PAD_OFFSET
        XINPUT_STATE state;
        ZeroMemory(&state, sizeof(XINPUT_STATE));
        bool exit = false;

        if (XInputGetState(0, &state) == ERROR_SUCCESS){
          if (codePos < PAD_LTRIGGER){
            switch (codePos){
            case PAD_DPAD_UP:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_UP);
              break;
            case PAD_DPAD_DOWN:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_DOWN);
              break;
            case PAD_DPAD_LEFT:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_LEFT);
              break;
            case PAD_DPAD_RIGHT:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_RIGHT);
              break;
            case PAD_START:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_START);
              break;
            case PAD_BACK:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_BACK);
              break;
            case PAD_LEFT_THUMB:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_LEFT_THUMB);
              break;
            case PAD_RIGHT_THUMB:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_RIGHT_THUMB);
              break;
            case PAD_LEFT_SHOULDER:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_LEFT_SHOULDER);
              break;
            case PAD_RIGHT_SHOULDER:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_RIGHT_SHOULDER);
              break;
            case PAD_A:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_A);
              break;
            case PAD_B:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_B);
              break;
            case PAD_X:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_X);
              break;
            case PAD_Y:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_Y);
              break;
            }
            if (exit){
              itrAxis->second.prevValue = itrAxis->second.value;
              if (itrAxis->second.value < MAX_ABS)
                itrAxis->second.value = itrAxis->second.prevValue + STEP;
              break;
            }
          }
        }
      }
      if ((i == itrAxis->second.inputCodesNeg.Size() - 1) && itrAxis->second.value > 0){
        itrAxis->second.value = itrAxis->second.value - STEP;
        if (itrAxis->second.value < 0)
          itrAxis->second.value = 0.;
      }

      //negative
      eInputCode codeNeg = itrAxis->second.inputCodesNeg[i];
      if (ValueInRange(codeNeg, KEYBOARD_OFFSET, MOUSE_OFFSET - 1)){
        if (glfwGetKey(codeNeg) == GLFW_PRESS){
          itrAxis->second.prevValue = itrAxis->second.value;
          if (itrAxis->second.value > -MAX_ABS)
            itrAxis->second.value = itrAxis->second.prevValue - STEP;
          break;
        }
      }
      else if (ValueInRange(codeNeg, MOUSE_OFFSET, PAD_OFFSET - 1)){

      }
      else{ // codeNeg >= PAD_OFFSET
        XINPUT_STATE state;
        ZeroMemory(&state, sizeof(XINPUT_STATE));
        bool exit = false;

        if (XInputGetState(0, &state) == ERROR_SUCCESS){
          if (codeNeg < PAD_LTRIGGER){
            switch (codeNeg){
            case PAD_DPAD_UP:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_UP);
              break;
            case PAD_DPAD_DOWN:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_DOWN);
              break;
            case PAD_DPAD_LEFT:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_LEFT);
              break;
            case PAD_DPAD_RIGHT:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_DPAD_RIGHT);
              break;
            case PAD_START:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_START);
              break;
            case PAD_BACK:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_BACK);
              break;
            case PAD_LEFT_THUMB:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_LEFT_THUMB);
              break;
            case PAD_RIGHT_THUMB:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_RIGHT_THUMB);
              break;
            case PAD_LEFT_SHOULDER:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_LEFT_SHOULDER);
              break;
            case PAD_RIGHT_SHOULDER:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_RIGHT_SHOULDER);
              break;
            case PAD_A:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_A);
              break;
            case PAD_B:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_B);
              break;
            case PAD_X:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_X);
              break;
            case PAD_Y:
              exit = checkPadButtomPressed(state, XINPUT_GAMEPAD_Y);
              break;
            }
            if (exit){
              itrAxis->second.prevValue = itrAxis->second.value;
              if (itrAxis->second.value > -MAX_ABS)
                itrAxis->second.value = itrAxis->second.prevValue - STEP;
              break;
            }
          }
        }
      }
      if ((i == itrAxis->second.inputCodesNeg.Size() - 1) && itrAxis->second.value < 0){
        itrAxis->second.value = itrAxis->second.value + STEP;
        if (itrAxis->second.value > 0)
          itrAxis->second.value = 0.;
      }

      //analógicos
      if (codePos == codeNeg){
        if (ValueInRange(codePos, KEYBOARD_OFFSET, MOUSE_OFFSET - 1)){

        }
        else if (ValueInRange(codePos, MOUSE_OFFSET, PAD_OFFSET - 1)){

        }
        else{ // codePos >= PAD_OFFSET
          XINPUT_STATE state;
          ZeroMemory(&state, sizeof(XINPUT_STATE));
          bool exit = false;

          if (XInputGetState(0, &state) == ERROR_SUCCESS){
            switch (codePos){
            case PAD_LTRIGGER:
              itrAxis->second.value = normalize(state.Gamepad.bLeftTrigger, 255., -255.);
              exit = true;
              break;
            case PAD_RTRIGGER:
              itrAxis->second.value = normalize(state.Gamepad.bRightTrigger, 255., -255.);
              exit = true;
              break;
            case PAD_THUMB_LX:
              itrAxis->second.value = normalize(state.Gamepad.sThumbLX, 32767., -32768.);
              exit = true;
              break;
            case PAD_THUMB_LY:
              itrAxis->second.value = normalize(state.Gamepad.sThumbLY, 32767., -32768.);
              exit = true;
              break;
            case PAD_THUMB_RX:
              itrAxis->second.value = normalize(state.Gamepad.sThumbRX, 32767., -32768.);
              exit = true;
              break;
            case PAD_THUMB_RY:
              itrAxis->second.value = normalize(state.Gamepad.sThumbRY, 32767., -32768.);
              exit = true;
              break;
            }

            if (abs(itrAxis->second.value) < ZONA_MUERTA)
              itrAxis->second.value = 0;

            if (exit)
              break;
          }
        }
      }
    }
  }
}

void InputManager::CreateAction(const String& name, eInputCode button){
  dicAction[name].inputCodes.Add(button);
}

void InputManager::CreateActionAxis(const String& name, eInputCode positiveAxis, eInputCode negativeAxis){
  dicActionAxis[name].inputCodesPos.Add(positiveAxis);
  dicActionAxis[name].inputCodesNeg.Add(negativeAxis);
}

bool InputManager::IsActionPressed(const String& name) const{
  auto it = dicAction.find(name);
  if (it != dicAction.end())
    if (it->second.state == pressed)
      return true;
  return false;
}

bool InputManager::IsActionDown(const String& name) const{
  auto it = dicAction.find(name);
  if (it != dicAction.end())
    if (it->second.state == down)
      return true;
  return false;
}

bool InputManager::IsActionUp(const String& name) const{
  auto it = dicAction.find(name);
  if (it != dicAction.end())
    if (it->second.state == up)
      return true;
  return false;
}

float InputManager::GetActionAxis(const String& name) const{
  auto it = dicActionAxis.find(name);
  if (it != dicActionAxis.end())
      return it->second.value;
  return 0.;
}

void InputManager::setVibration(eRotor rotor, uint32 value){
  if (rotor == rLeft)
    powerLeft = value;
  if (rotor == rRight)
    powerRight = value;

  XINPUT_VIBRATION vibration;

  vibration.wLeftMotorSpeed = (WORD)powerLeft;
  vibration.wRightMotorSpeed = (WORD)powerRight;

  XInputSetState(0, &vibration);
}

uint32 InputManager::getVibration(eRotor rotor){
  if (rotor == rLeft)
    return powerLeft;
  if (rotor == rRight)
    return powerRight;
  return 0;
}

float InputManager::normalize(float value, float max, float min){
  return ((value - min) / (max - min)) * 2 - 1;
}