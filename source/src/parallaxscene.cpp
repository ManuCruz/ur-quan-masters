#include "../include/parallaxscene.h"
#include "../include/renderer.h"

ParallaxScene::ParallaxScene(Image* imageBack, Image* imageFront) : Scene(NULL){
  backLayer = imageBack;
  frontLayer = imageFront;
  backX = backY = 0.;
  frontX = frontY = 0.;
  relBackSpeedX = relBackSpeedY = 0.;
  relFrontSpeedX = relFrontSpeedY = 0.;
  autoBackSpeedX = autoBackSpeedY = 0.;
  autoFrontSpeedX = autoFrontSpeedY = 0.;
}

ParallaxScene::~ParallaxScene(){
}

void ParallaxScene::SetRelativeBackSpeed(double x, double y){
  relBackSpeedX = x;
  relBackSpeedY = y;
}

void ParallaxScene::SetRelativeFrontSpeed(double x, double y){
  relFrontSpeedX = x;
  relFrontSpeedY = y;
}

void ParallaxScene::SetAutoBackSpeed(double x, double y){
  autoBackSpeedX = x;
  autoBackSpeedY = y;
}

void ParallaxScene::SetAutoFrontSpeed(double x, double y){
  autoFrontSpeedX = x;
  autoFrontSpeedY = y;
}

void ParallaxScene::Update(double elapsed, Map* map){
  double oldX = GetCamera().GetX();
  double oldY = GetCamera().GetY();

  Scene::Update(elapsed, map);

  double newX = GetCamera().GetX();
  double newY = GetCamera().GetY();

  backX += autoBackSpeedX * elapsed - relBackSpeedX * (newX - oldX);
  backY += autoBackSpeedY * elapsed - relBackSpeedY * (newY - oldY);
  frontX += autoFrontSpeedX * elapsed - relFrontSpeedX * (newX - oldX);
  frontY += autoFrontSpeedY * elapsed - relFrontSpeedY * (newY - oldY);
}

void ParallaxScene::RenderBackground() const{
  if (backLayer)
    Renderer::Instance().DrawTiledImage(backLayer, 0, 0, Screen::Instance().GetWidth(), Screen::Instance().GetHeight(), -backX, -backY);
  if (frontLayer)
    Renderer::Instance().DrawTiledImage(frontLayer, 0, 0, Screen::Instance().GetWidth(), Screen::Instance().GetHeight(), -frontX, -frontY);
}
