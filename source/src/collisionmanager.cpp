#include "../include/collisionmanager.h"
#include "../include/collisionpixeldata.h"
#include "../include/math.h"
#include "../include/types.h"
#include <stdio.h>
#include <math.h>

CollisionManager* CollisionManager::manager = NULL;

bool CollisionManager::CircleToCircle(double x1, double y1, double r1, double x2, double y2, double r2) const{
  return Distance(x1, y1, x2, y2) <= (r1 + r2);
}

bool CollisionManager::CircleToPixels(double cx, double cy, double cr, const CollisionPixelData* pixels, double px, double py) const{
  if (RectsOverlap(px, py, pixels->GetWidth(), pixels->GetHeight(), cx - cr, cy - cr, cx + cr, cy + cr)){ //si hay solapamiento con el rect�ngulo que engloba al c�rculo
    double x, y, w, h;
    OverlappingRect(px, py, pixels->GetWidth(), pixels->GetHeight(), cx - cr, cy - cr, cx + cr, cy + cr, &x, &y, &w, &h); //se calcula el rect�ngulo solapado

    uint32 xiu = static_cast<uint32>(x - px);
    uint32 yiu = static_cast<uint32>(y - py);
    uint32 toph = static_cast<uint32>(h)+yiu;
    uint32 topw = static_cast<uint32>(w)+xiu;

    for (uint32 i = yiu; i < toph; i++)
      for (uint32 j = xiu; j < topw; j++)
        if (pixels->GetData(j, i) && (Distance(j + px,  i + py, cx, cy) <= cr)) //si un punto del rect�ngulo solapado es opaco y su distancia al centro del c�rculo es menor que el radio, hay colisi�n
          return true;
  }
  return false;
}

bool CollisionManager::CircleToRect(double cx, double cy, double cr, double rx, double ry, double rw, double rh) const{
  double outx, outy;
  ClosestPointToRect(cx, cy, rx, ry, rw, rh, &outx, &outy);
  return Distance(cx, cy, outx, outy) <= cr;
}

bool CollisionManager::PixelsToPixels(const CollisionPixelData* p1, double x1, double y1, const CollisionPixelData* p2, double x2, double y2) const{
  if (RectsOverlap(x1, y1, p1->GetWidth(), p1->GetHeight(), x2, y2, p2->GetWidth(), p2->GetHeight())){
    double x, y, w, h;
    OverlappingRect(x1, y1, p1->GetWidth(), p1->GetHeight(), x2, y2, p2->GetWidth(), p2->GetHeight(), &x, &y, &w, &h);

    uint32 xi1u = static_cast<uint32>(x - x1);
    uint32 yi1u = static_cast<uint32>(y - y1);
    uint32 xi2u = static_cast<uint32>(x - x2);
    uint32 yi2u = static_cast<uint32>(y - y2);
    uint32 hu = static_cast<uint32>(h);
    uint32 wu = static_cast<uint32>(w);

    for (uint32 i = 0; i < hu; i++)
      for (uint32 j = 0; j < wu; j++)
        if (p1->GetData(xi1u + j, yi1u + i) && p2->GetData(xi2u + j, yi2u + i))
          return true;
  }
  return false;
}

bool CollisionManager::PixelsToRect(const CollisionPixelData* pixels, double px, double py, double rx, double ry, double rw, double rh) const{
  if (RectsOverlap(px, py, pixels->GetWidth(), pixels->GetHeight(), rx, ry, rw, rh)){
    double x, y, w, h;
    OverlappingRect(px, py, pixels->GetWidth(), pixels->GetHeight(), rx, ry, rw, rh, &x, &y, &w, &h);

    uint32 xiu = static_cast<uint32>(x - px);
    uint32 yiu = static_cast<uint32>(y - py);
    uint32 toph = static_cast<uint32>(h) + yiu;
    uint32 topw = static_cast<uint32>(w) + xiu;

    for (uint32 i = yiu; i < toph; i++)
      for (uint32 j = xiu; j < topw; j++)
        if (pixels->GetData(j, i))
          return true;
  }
  return false;
}

bool CollisionManager::RectToRect(double x1, double y1, double w1, double h1, double x2, double y2, double w2, double h2) const{
  return RectsOverlap(x1, y1, w1, h1, x2, y2, w2, h2);
}

//https://luisrey.wordpress.com/2008/07/06/distancia-punto-1/
bool CollisionManager::CircleToLine(double cx, double cy, double cr, double x0, double y0, double angle, double width, double height) const{
  double initialX = x0;
  double initialY = y0 + (int32)(height / 2);
  double finalX = initialX + DegCos(angle) * width;
  double finalY = initialY + DegSin(angle) * width;
  
  //distancia del centro del c�rculo a la recta
  double dist = ((finalX - initialX)*(cy - initialY) - (finalY - initialY)*(cx - initialX)) / sqrt(pow(finalX - initialX, 2) + pow(finalY - initialY, 2));

  //comprobar si el punto m�s cercano est� dentro del segmento. Si u est� entre 0 y 1, el punto est� en el segmento
  double u = ((cx - initialX)*(finalX - initialX) + (cy - initialY)*(finalY - initialY)) / (pow(finalX - initialX, 2) + pow(finalY - initialY, 2));

  //o si el punto inicial est� a menos de un radio de distancia del centro
  double di = sqrt(pow(cx - initialX, 2) + pow(cy - initialY, 2));

  //o si el punto final est� a menos de un radio de distancia del centro
  double df = sqrt(pow(cx - finalX, 2) + pow(cy - finalY, 2));

  if ((u >= 0 && u <= 1 && abs(dist) <= cr) || (abs(di) <= cr) || (abs(df) <= cr))
    return true;
  return false;
}

bool CollisionManager::RectToLine(double rx, double ry, double rw, double rh, double x0, double y0, double angle, double width, double height) const{
  //TO DO
  return false;
}
bool CollisionManager::PixelsToLine(const CollisionPixelData* pixels, double px, double py, double x0, double y0, double angle, double width, double height) const{
  //TO DO
  return false;
}
bool CollisionManager::LineToLine(double x0, double y0, double angle, double width, double height, double x02, double y02, double angle2, double width2, double height2) const{
  //TO DO
  return false;
}
