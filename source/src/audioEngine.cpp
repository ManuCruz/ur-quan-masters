#include "../include/audioEngine.h"
#include "../lib/AL/alc.h"
#include "../lib/AL/al.h"

AudioEngine* AudioEngine::engine = NULL;

AudioEngine& AudioEngine::Instance() {
  if (!engine)
    engine = new AudioEngine();
  return *engine;
}

void AudioEngine::Init(){
  if (!device)
    device = alcOpenDevice(NULL);
  if (device && !context)
    context = alcCreateContext((ALCdevice *)device, NULL);
  if (context)
    alcMakeContextCurrent((ALCcontext *)context);
}

void AudioEngine::Finish(){
  if (context)
    alcDestroyContext((ALCcontext *)context);
  if (device)
    alcCloseDevice((ALCdevice *)device);
}

void AudioEngine::SetDopplerFactor(float factor) {
  alDopplerFactor(factor);
}

AudioEngine::~AudioEngine(){
  Finish();
}