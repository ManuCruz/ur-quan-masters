#include "../include/screen.h"
#include "../include/glinclude.h"

Screen* Screen::screen = NULL;

int8 keyboardState[GLFW_KEY_LAST + 1];
int8 mouseState[GLFW_MOUSE_BUTTON_8 + 1];

int32 GLFWCALL Screen::CloseCallback() {
  Screen::Instance().opened = false;
  return GL_TRUE;
}

Screen::Screen() {
  glfwInit();

  opened = false;
}

Screen::~Screen() {
  glfwTerminate();
}

Screen& Screen::Instance() {
  if ( !screen )
    screen = new Screen();
  return *screen;
}

void Screen::Open(uint16 width, uint16 height, bool fullscreen) {
  // Abrimos la ventana
  glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
  glfwOpenWindow(int(width), int(height), 8, 8, 8, 8, 0, 0, fullscreen ? GLFW_FULLSCREEN : GLFW_WINDOW );
  if ( !fullscreen )
    glfwSetWindowPos((GetDesktopWidth()-width)/2, (GetDesktopHeight()-height)/2);
  glfwSetWindowCloseCallback(GLFWwindowclosefun(CloseCallback));
  glfwSetCharCallback(&onKey);
  glfwSwapInterval(1);
  SetTitle("");
  opened = true;

  // Inicializamos OpenGL
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);

  // Configuramos viewport
  this->width = width;
  this->height = height;
  glViewport(0, 0, width, height);

  // Configuramos matriz de proyeccion
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, width, height, 0, 0, 1000);

  // Configuramos matriz de modelado
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  // Inicializamos temporizador
  lastTime = glfwGetTime();
  elapsed = 0;
}

void Screen::Close() {
  glfwCloseWindow();
}

void Screen::Resize(uint32 width, uint32 height){
  glfwSetWindowSize(width, height);
  glfwSetWindowPos((GetDesktopWidth() - width) / 3, (GetDesktopHeight() - height) / 3);

  // Configuramos viewport
  this->width = (uint16)width;
  this->height = (uint16)height;
  glViewport(0, 0, width, height);

  // Configuramos matriz de proyeccion
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, width, height, 0, 0, 1000);

  // Configuramos matriz de modelado
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void Screen::Zoom(uint32 width, uint32 height){
  this->width = (uint16)width;
  this->height = (uint16)height;

  // Configuramos matriz de proyeccion
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, width, height, 0, 0, 1000);

  // Configuramos matriz de modelado
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

void Screen::SetTitle(const String &title) {
  glfwSetWindowTitle(title.ToCString());
}

void Screen::Refresh() {
  glfwSwapBuffers();
  glfwGetMousePos(&mousex, &mousey);
  elapsed = glfwGetTime() - lastTime;
  lastTime = glfwGetTime();
}

uint16 Screen::GetDesktopWidth() const {
  GLFWvidmode mode;
  glfwGetDesktopMode(&mode);
  return uint16(mode.Width);
}

uint16 Screen::GetDesktopHeight() const {
  GLFWvidmode mode;
  glfwGetDesktopMode(&mode);
  return uint16(mode.Height);
}

bool Screen::MouseButtonPressed(int button) const {
  return glfwGetMouseButton(button) == GLFW_PRESS;
}

bool Screen::KeyPressed(int key) const {
  return glfwGetKey(key) == GLFW_PRESS;
}

bool Screen::KeyOnce(int key) const {
  if (glfwGetKey(key)) {
    if (!keyboardState[key]) {
      keyboardState[key] = true;
      return true;
    }
  }
  else
    keyboardState[key] = false;
  return false;
}

bool Screen::MouseOnce(int button) const {
  if (glfwGetMouseButton(button)) {
    if (!mouseState[button]) {
      mouseState[button] = true;
      return true;
    }
  }
  else
    mouseState[button] = false;
  return false;
}

String Screen::text = "";

void GLFWCALL Screen::onKey(int32 key, int32 action){
  if (action == GLFW_PRESS)
    text += (int8)key;
}