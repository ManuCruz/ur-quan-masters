#include "../include/sprite.h"
#include "../include/rectcollision.h"
#include "../include/image.h"
#include "../include/map.h"
#include "../include/math.h"
#include "../include/pixelcollision.h"
#include "../include/renderer.h"
#include "../include/circlecollision.h"
#include "../include/linecollision.h"
#include <math.h>

Sprite::Sprite(Image* image) {
  this->image = image;
  x = y = angle = 0.;
  scalex = scaley = 1.;
  blendMode = Renderer::ALPHA;
  r = g = b = a = 255;
  userData = NULL;

  rotating = false;
  toAngle = 0;
  rotatingSpeed = anglesToRotate = 0.;
  moving = false;
  toX = toY = movingSpeedX = movingSpeedY = distX = distY = 0.;

  animFPS = firstFrame = lastFrame = 0;
  currentFrame = 0.;

  colx = coly = colwidth = colheight = radius = 0.;
  collision = NULL;
  colPixelData = NULL;
  colSprite = NULL;
  collided = false;
}

Sprite::~Sprite() {
  delete collision;
}

void Sprite::SetCollision(CollisionMode mode) {
  delete collision;

  switch (mode) {
  case Sprite::COLLISION_CIRCLE:
    collision = new CircleCollision(&x, &y, &radius);
    break;
  case Sprite::COLLISION_PIXEL:
    collision = new PixelCollision(&colx, &coly, &colwidth, &colheight, colPixelData);
    break;
  case Sprite::COLLISION_RECT:
    collision = new RectCollision(&colx, &coly, &colwidth, &colheight);
    break;
  case Sprite::COLLISION_LINE:
    collision = new LineCollision(&colx, &coly, &angle, &colwidth, &colheight);
    break;
  case Sprite::COLLISION_NONE:
  default:
    collision = NULL;
    break;
  }
}

bool Sprite::CheckCollision(Sprite* sprite) {
  if (collision && sprite->GetCollision())
    if (collision->DoesCollide(sprite->GetCollision())){
      colSprite = sprite;
      collided = true;
      sprite->colSprite = this;
      sprite->collided = true;
      return true;
    }
  return false;
}

bool Sprite::CheckCollision(const Map* map) {
  if (map && collision){
    collided = map->CheckCollision(collision);
    return collided;
  }
  return false;
}

void Sprite::RotateTo(int32 angle, double speed) {
  toAngle = static_cast<uint16>(WrapValue(angle, 360.));
  if (abs(this->angle - toAngle) == 0.)  //sin rotacion
    rotating = false;
  else if ((WrapValue(toAngle - this->angle, 360) <= WrapValue(this->angle - toAngle, 360.))){  //rotacion positiva
    rotating = true;
    rotatingSpeed = speed;
    anglesToRotate = WrapValue(toAngle - this->angle, 360.);
  }
  else{  //rotacion negativa
    rotating = true;
    rotatingSpeed = -speed;
    anglesToRotate = WrapValue(this->angle - toAngle, 360.);
  }
}

void Sprite::MoveTo(double x, double y, double speedX, double speedY) {
  if (abs(this->x - x) == 0. && abs(this->y - y) == 0.)  //sin movimiento
    moving = false;
  else if (speedY == 0.){  //movimiento uniforme
    moving = true;
    toX = x;
    toY = y;
    distX = abs(toX - this->x);
    distY = abs(toY - this->y);

    double distan = Distance(this->x, this->y, toX, toY);

    movingSpeedX = (distX == 0.) ? 0. : speedX * (toX - this->x) / distan;
    movingSpeedY = (distY == 0.) ? 0. : speedX * (toY - this->y) / distan;
  }
  else{  //movimiento con dos velocidades
    moving = true;
    toX = x;
    toY = y;
    distX = abs(toX - this->x);
    distY = abs(toY - this->y);
    movingSpeedX = speedX;
    movingSpeedY = speedY;
    if (this->x > toX)
      movingSpeedX *= -1;
    else if (distX == 0.)
      movingSpeedX = 0.;
    if (this->y > toY)
      movingSpeedY *= -1;
    else if (distY == 0.)
      movingSpeedY = 0.;
  }
}

void Sprite::Update(double elapsed, const Map* map) {
  // Informacion inicial de colision
  colSprite = NULL;
  collided = false;

  // Actualizar animacion
  if (firstFrame != lastFrame){
    currentFrame += animFPS * elapsed;
    if (currentFrame <= firstFrame)
      currentFrame = lastFrame + 0.999999999999;
    if (currentFrame >= (lastFrame + 1))
      currentFrame = firstFrame;
  }

  // Actualizar rotacion animada
  if (rotating){
    if (anglesToRotate == 0.)
      rotating = false;
    else{
      anglesToRotate -= abs(rotatingSpeed) * elapsed;
      angle = (anglesToRotate < 0.) ? toAngle : WrapValue(angle + rotatingSpeed * elapsed, 360.);
    }
  }

  // Actualizar movimiento animado
  if (moving){
    if (distX == 0. && distY == 0.)
      moving = false;
    else{
      bool moveX = true;
      bool moveY = true;

      if (distX > 0.){
        distX -= abs(movingSpeedX) * elapsed;
        double prevX = x;
        
        x = (distX < 0.) ? toX : x + movingSpeedX * elapsed;

        UpdateCollisionBox();

        if (CheckCollision(map)){
          x = prevX;
          moveX = false;
        }
      }

      if (distY > 0.){
        distY -= abs(movingSpeedY) * elapsed;
        double prevY = y;

        y = (distY < 0.) ? toY : y + movingSpeedY * elapsed;

        UpdateCollisionBox();

        if (CheckCollision(map)){
          y = prevY;
          moveY = false;
        }
      }

      if (!moveX && !moveY)
        moving = false;
    }
  }

  // Informacion final de colision
  UpdateCollisionBox();
}

void Sprite::Render() const {
  Renderer::Instance().SetBlendMode(blendMode);
  Renderer::Instance().SetColor(r, g, b, a);
  Renderer::Instance().DrawImage(image, GetScreenX(), GetScreenY(), static_cast<uint32>(currentFrame), image->GetWidth() * scalex, image->GetHeight() * scaley, angle);
  Renderer::Instance().SetColor(255, 255, 255, 255);
}

void Sprite::UpdateCollisionBox() {
  double cx = x - image->GetHandleX() * abs(scalex);
  double cy = y - image->GetHandleY() * abs(scaley);
  double cw = image->GetWidth() * abs(scalex);
  double ch = image->GetHeight() * abs(scaley);
  UpdateCollisionBox(cx, cy, cw, ch);
}

void Sprite::UpdateCollisionBox(double x, double y, double w, double h) {
  colx = x;
  coly = y;
  colwidth = w;
  colheight = h;
}

double Sprite::GetUpX() const{
  return DegCos(angle);
}

double Sprite::GetUpY() const{
  return DegSin(angle);
}