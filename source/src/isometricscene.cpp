#include "../include/isometricscene.h"
#include "../include/isometricmap.h"
#include "../include/map.h"
#include "../include/sprite.h"
#include "../include/image.h"
#include "../include/array.h"

IsometricScene::IsometricScene(IsometricMap* map, Image* imageBack, Image* imageFront) : MapScene(static_cast<Map*>(map), imageBack, imageFront){
  map->GenerateLayerSprites(this);
}

IsometricSprite* IsometricScene::CreateSprite(Image* image, Layer layer){
  IsometricSprite* spr = new IsometricSprite(image);
  AddSprite(spr, layer);
  return spr;
}

void IsometricScene::Update(double elapsed){
  MapScene::Update(elapsed); 
  for (uint32 i = 0; i < LAYER_COUNT; i++) 
    GetSprites(static_cast<Layer>(i)).Sort(&CompareSprites);
}

bool IsometricScene::CompareSprites(Sprite*& first, Sprite*& second){
  return (first->GetScreenY() < second->GetScreenY()) ? true : (first->GetScreenY() == second->GetScreenY() && first->GetScreenX() < second->GetScreenX()) ? true : false;
}

