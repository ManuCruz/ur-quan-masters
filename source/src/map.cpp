#include "../include/map.h"
#include "../include/rectcollision.h"
#include "../include/collision.h"
#include "../include/image.h"
#include "../include/renderer.h"
#include "../include/resourcemanager.h"
#include "../include/base64.h"
#include "../include/filemanager.h"
#include "../lib/rapidxml.hpp"
#include <limits.h>

using namespace rapidxml;

Map::Map(const String &filename, uint16 firstColId) {
  this->filename = filename;
  this->firstColId = firstColId;
  valid = false;

  String file = FileManager::Instance().LoadString(filename);
  
  if (file != ""){
    xml_document<> doc;
    doc.parse<0>(const_cast<char *>(file.ToCString()));

    xml_node<>* nMap = doc.first_node("map");
    width = static_cast<uint16>(String(nMap->first_attribute("width")->value()).ToInt());
    height = static_cast<uint16>(String(nMap->first_attribute("height")->value()).ToInt());
    tileWidth = static_cast<uint16>(String(nMap->first_attribute("tilewidth")->value()).ToInt());
    tileHeight = static_cast<uint16>(String(nMap->first_attribute("tileheight")->value()).ToInt());

    xml_node<>* nTileset = nMap->first_node("tileset");
    int32 firstgid = String(nTileset->first_attribute("firstgid")->value()).ToInt();
    int32 tWidth = String(nTileset->first_attribute("tilewidth")->value()).ToInt();
    int32 tHeight = String(nTileset->first_attribute("tileheight")->value()).ToInt();
    int32 tSpacing = 0; //
    if (nTileset->first_attribute("spacing")) //
      tSpacing = String(nTileset->first_attribute("spacing")->value()).ToInt(); //

    xml_node<>* nTileoffset = nTileset->first_node("tileoffset");
    int32 offx = 0, offy = 0;
    if (nTileoffset){
      offx = String(nTileoffset->first_attribute("x")->value()).ToInt();
      offy = String(nTileoffset->first_attribute("y")->value()).ToInt();
    }

    xml_node<>* nImage = nTileset->first_node("image");
    imageFile = String(nImage->first_attribute("source")->value()).StripDir();
    int32 imageWidth = String(nImage->first_attribute("width")->value()).ToInt();
    int32 imageHeight = String(nImage->first_attribute("height")->value()).ToInt();

    xml_node<>* nData = nMap->first_node("layer")->first_node("data");
    //si no hay atributos “encoding” o “compression”,
    if (!nData->first_attribute("encoding") && !nData->first_attribute("compression")){
      xml_node<>* nTile = nData->first_node("tile");
      while (nTile){
        tileIds.Add(String(nTile->first_attribute("gid")->value()).ToInt() - firstgid);
        nTile = nTile->next_sibling("tile");
      }

      image = ResourceManager::Instance().LoadImage(imageFile, static_cast<uint16>(imageWidth / (tWidth + tSpacing)), static_cast<uint16>(imageHeight / (tHeight + tSpacing)));
      if (image){
        image->SetHandle(offx, offy);
        valid = true;
      }
    }
    else if (String(nData->first_attribute("encoding")->value()) == "csv"){
      String cad = nData->value();
      cad = cad.Replace("\n", "");
      cad = cad.Trim();
      Array<String> aCad = cad.Split(",");
      for (uint32 i = 0; i < aCad.Size(); i++)
        tileIds.Add(aCad[i].ToInt() - firstgid);

      image = ResourceManager::Instance().LoadImage(imageFile, static_cast<uint16>(imageWidth / (tWidth + tSpacing)), static_cast<uint16>(imageHeight / (tHeight + tSpacing)));
      if (image){
        image->SetHandle(offx, offy);
        valid = true;
      }
    }
    else if (String(nData->first_attribute("encoding")->value()) == "base64" && !nData->first_attribute("compression")){
      String cad = nData->value();
      cad = cad.Replace("\n", "");
      cad = cad.Trim();
      cad = base64_decode(cad);

      uint32 value;
      for (int32 i = 0; i < cad.Length(); i += 4){
        value = (cad[i + 3] - 0x30);
        value = value << 8;
        value = (cad[i + 2] - 0x30);
        value = value << 8;
        value = (cad[i + 1] - 0x30);
        value = value << 8;
        value = (cad[i] - 0x30);

        tileIds.Add(value - firstgid);
      }

      image = ResourceManager::Instance().LoadImage(imageFile, static_cast<uint16>(imageWidth / (tWidth + tSpacing)), static_cast<uint16>(imageHeight / (tHeight + tSpacing)));
      if (image){
        image->SetHandle(offx, offy);
        valid = true;
      }
    }
  }
}

void Map::Render() const {
  for (uint16 y = 0; y < GetRows(); y++)
    for (uint16 x = 0; x < GetColumns(); x++)
      if (GetTileId(x, y) >= 0)
        Renderer::Instance().DrawImage(image, x*GetTileWidth(), y*GetTileHeight(), GetTileId(x, y));
}

bool Map::CheckCollision(const Collision* collision) const {
  // Creamos caja de colisiones
  double boxX = 0, boxY = 0, boxWidth = GetTileWidth(), boxHeight = GetTileHeight();
  RectCollision boxCol(&boxX, &boxY, &boxWidth, &boxHeight);

  // Comprobamos colision con cada tile
  for (uint16 y = 0; y < GetRows(); y++) {
    for (uint16 x = 0; x < GetColumns(); x++) {
      if (GetTileId(x, y) >= firstColId) {
        boxX = x * GetTileWidth();
        boxY = y * GetTileHeight();

        if (collision->DoesCollide(&boxCol))
          return true;
      }
    }
  }

  return false;
}

double Map::GetGroundY(double x, double y) const {
  double groundY = UINT_MAX;

  if (x < 0 || x >= width*tileWidth || y >= height*tileHeight) return groundY;
  if (y < 0) y = 0;

  // Buscamos el primer tile en esa columna
  for (int tiley = int(y / tileHeight); tiley < height; tiley++)
    if (tileIds[tiley*width + int(x / tileWidth)] >= 0) {
      groundY = tiley*tileHeight;
      break;
    }
  return groundY;
}
