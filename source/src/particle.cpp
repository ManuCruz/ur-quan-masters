#include "../include/particle.h"
#include "../include/image.h"

Particle::Particle():Sprite(NULL){
  velocityx = velocityy = 0.;
  angularVelocity = 0.;
  lifetime = initialLifetime = 0.;
  autofade = false;
  affected = false;
  initialAlpha = 0;
}

Particle::Particle(Image* image, double velx, double vely, double angularVel, double lifetime, bool autofade):Sprite(image){
  velocityx = velx;
  velocityy = vely;
  angularVelocity = angularVel;
  this->lifetime = initialLifetime = lifetime;
  this->autofade = autofade;
  affected = false;
  initialAlpha = GetAlpha();
}

void Particle::Update(double elapsed){
  SetX(GetX() + velocityx*elapsed);
  SetY(GetY() + velocityy*elapsed);

  SetAngle(GetAngle() + angularVelocity*elapsed);

  lifetime -= elapsed;
  if (lifetime < 0.)
    lifetime = 0.0;

  if (autofade){
    SetColor(GetRed(), GetGreen(), GetBlue(), static_cast<uint8>(initialAlpha * lifetime / initialLifetime));
  }
}